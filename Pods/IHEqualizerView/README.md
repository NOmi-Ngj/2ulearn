# IHEqualizerView

[![Version](https://img.shields.io/cocoapods/v/IHEqualizerView.svg?style=flat)](http://cocoapods.org/pods/IHEqualizerView)
[![License](https://img.shields.io/cocoapods/l/IHEqualizerView.svg?style=flat)](http://cocoapods.org/pods/IHEqualizerView)
[![Platform](https://img.shields.io/cocoapods/p/IHEqualizerView.svg?style=flat)](http://cocoapods.org/pods/IHEqualizerView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IHEqualizerView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IHEqualizerView'
```

## Author

Md Ibrahim Hassan, mdibrahimhassan@gmail.com

## License

IHEqualizerView is available under the MIT license. See the LICENSE file for more info.
