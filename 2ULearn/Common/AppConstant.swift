//
//  AppConstant.swift
//  SmartServe
//
//  Created by MacBook on 6/16/17.
//  Copyright © 2017 DigitonicsLabs. All rights reserved.
//

import Foundation
import UIKit

enum UserStatus {
    case isFriend
    case isPending
    case isCancel
    case isUnFriend
    case isUnFollow
    case isAccepted
    case isInvited
    case isRejected
    case isSendingFromUser
    case none
}

enum CreatePostTypes
{
    case isTypeTxt
    case isTypeImgAndVideo
    case isTypeLocation
    case none
}

enum FeedBackType:Int {
    case isTypeVideo
    case isTypeImage
    case isTypeTxt
    case isTypeLocation
    case none
}

let appDelet = UIApplication.shared.delegate as! AppDelegate
let appDeletForBadee = UIApplication.shared

// Base URL
//let kBaseURL = "http://demo-appocta.com/appocta/contrav/"
let kBaseURL = "http://dev-cmolds.com/appocta/contrav/"

let kImgmarkerURL = kBaseURL+"upload/dropicon.ico|"

// MARK: - API Functions
let kGetResturantJSON = "getRestaurantByCode.json"

let kGETNewsFeedsJSON = "postlist.json"
let kUnFollowJSON = "followingaction.json"
let kPostLIKEJSON = "postlikeaction.json"
let kCompanyJSON = "companies.json"
let kSearchRestultJSON = "search.json"
let kMyPostJSON = "mypost.json"
let kCreatePostJSON = "postadd.json"
let kCommentListJSON = "commentlist.json"
let kAddCommentJSON = "addcomment.json"
let kEditCommentJSON = "editcomment.json"
let kNotifictionPostDetailJSON = "postview.json"
let kDeleteCommentJSON = "deletecomment.json"
let kPostUpdateJSON = "postupdate.json"
let kUpdateDeviceTokenJSON = "updatedevice.json"
let kUserStatusOnlineJSON = "userstatus.json"
let kPostShareJSON = "postshare.json"
let kLogoutJSON = "logout.json"
let kLikeListJSON = "likelist.json"

let kSuccessResponse = "1"
let kFailureResponse = "0"
let kSuccessRegister = "5"
let kInvalidRequest = "4"
let kSessionExpire = "2"

let kEmptyStringJugar = "  " 

// MARK: - Fonts
let kFontRegular = "Oswald-Regular"

// MARK: - Segue Identifers
let kSegueLoginViewContorller = "CTLoginViewController"
let kSegueForgetPassViewController = "CTForgetPassViewController"
let kSegueFeedViewController = "FeedViewController"
let kSeugeFeedViewDetailViewController = "FeedViewDetailViewController"
let kSegueCTMessageViewController = "CTMessageViewController"
let kSegueDashboardViewController = "DashboardViewController"
let kSegueMainDashboardStorybarod = "MainDashboardStorybarod"
let kSegueCreatePostViewController = "CreatePostViewController"
let kSegueSearchViewController = "SearchViewController"
let kSegueFollowViewController = "FollowViewController"
let kSegueSearchResultDetailViewController = "SearchResultDetailViewController"
let kSegueNearByMeListViewController = "NearByMeListViewController"
let kSegueMyConnectionViewController = "MyConnectionViewController"
let kSegueTermsAndConditionViewController = "TermsAndConditionViewController"
let kSegueFaqsViewController = "FaqsViewController"
let kSegueMyProfileViewController = "MyProfileViewController"
let kSegueMessagesListViewController = "MessagesListViewController"
let kSegueAddTripsViewController = "AddTripsViewController"
let kSegueProfileViewController = "ProfileViewController"
let kSegueRecommedationConnectionViewController = "RecommedationConnectionViewController"
let kSegueConnectViewController = "ConnectViewController"
let kSegueCalenderViewController = "CalenderViewController"
let kSegueCreateEventViewController = "CreateEventViewController"
let kSegueNotificationViewController = "NotificationViewController"
let kSegueNotificationDetailViewController = "NotificationDetailViewController"
let kSeguePhotosViewController = "PhotosViewController"
let kSegueEditProfileViewController = "EditProfileViewController"
let kSegueFeedbackViewController = "FeedbackViewController"
let kSegueSubscriptionViewController = "SubscriptionViewController"
let kSegueTaglistViewController = "TaglistViewController"
let kSegueCreateMessageViewController = "CreateMessageViewController"
let kSegueUpdateAccountInfoViewController = "UpdateAccountInfoViewController"
let kSegueEdiitProfileViewController = "EdiitProfileViewController"
let kSegueShowProfileViewsController = "ShowProfileViewsController"
let kSegueFollowAndUnFollowViewController = "FollowAndUnFollowViewController"
let kSegueUserFeedsViewController = "UserFeedsViewController"
let kSegueSendRequestViewController = "SendRequestViewController"
let kSegueCelederDetailViewController = "CelederDetailViewController"
let kSegueWebviewContoller = "WebviewContoller"
let kSegueLikeListViewController = "LikeListViewController"
let kSegueSRCountryPickerController = "SRCountryPickerController"

let kuserSideMenu = "containSideMenu"
let kSideMenu = "mainhomeScreenss"

// MARK: - Storyboards
let kMainStoryboard = "Main"
let kMainFeedViewStoryboard = "MainFeedViewStoryboard"
let kMessagesStoryboard = "MainMessagesViewStoryboard"
let kCreatepostStoryboard = "createpostStorybarod"
let kSearchStoryboard = "searchUserStoryboard"
let kNearStorybarod = "NearByMeStoryboard"
let kMyConnectinoStoryboad = "MyConnectionStoryboard"
let kTermsAndFaqsStoryboard = "TermsAndFaqsStoryboard"
let kMyProfileStoryboard = "MyProfileStoryboard"
let kOtherProfileStoryboard = "profileStoryboard"
let kRecommendedConnectionStoryboard = "RecommendedConnectionStoryboard"
let kconnectStoryboard = "connectStoryboard"
let kcreateEventstoryboard = "createeventStoryboard"
let kNotificationStorybarod = "NotificationStorybarod"
let kPhotosStoryboard = "PhotosStoryboard"
let kFeedbackStoryboard = "MainFeedBackStoryboard"
let kUserFriendStoryboard = "UserFriendStoryboard"
let kWebViewStoryboard = "WebViewSBoard"
let kListStobryboard = "LikeLikeStoryboard"
let kCountryStoryboard = "CountryPickerStoryboard"

// MARK: - API
let kLoginJSON = "login.json"
let kSocialJSON = "socialsignup.json"
let kResigterJSON = "register.json"
let kForgetJSON = "forgot.json"
let kNearByUserJSON = "nearbyusers.json"
let kMyNetworkJSON = "network.json"
let kRecomentListJSON = "recommendedlist.json"
let kMyProfileJSON = "profile.json"
let kCountryJSON = "coutrieslist.json"
let kCityJSON = "citieslist.json"
let kStateJSON = "stateslist.json"
let kAddPassportJSON = "addpassport.json"
let kUpdateProfileJSON = "update.json"
let kConnectionJSON = "connection.json"
let kFollowingJSON = "following.json"
let kFollowdJSON = "followed.json"
let kUserPhots = "userphotos.json"
let kEventJSON = "eventlist.json"
let kAddEventJSON = "eventadd.json"
let kMemberActionJSON = "memberaction.json"
let kMemberActionUpdateJSON = "memberaction.json"
let kDeleteEventJSON = "eventdelete.json"
let kTermsJSON = "termsandconditions.json"
let kPolictyJSON = "privacyandpolicy.json"
let kFAQSJSON = "faqs.json"
let kSubmitFeedbackJSON = "feedback.json"
let kNotificationListJSON = "notificationslist.json"
let kDeletePOSTJSON = "postdelete.json"
let kUPloadFileJSON = "updatefiles.json"
let kDeactiveJSON = "profilestatus.json"
let kUploadPhotosOfUser = "addphoto.json"
let kFriendRequestToPendingJSON = "addconnection.json"
let kRjectedStatusJSON = "requestcancel.json"
let kAceptReqeustJSON = "requestaction.json"
let kDeletePhotosJSON = "deletephoto.json"
let kUnFriendJSON = "removeconnection.json"
let kEventDetailJSON = "eventview.json"
let kEventAcepted = "invitationaction.json"
let kEventReject = "invitecancel.json"
let kDeleteNotifcatn = "deletenotification.json"
let kPeopoleYouKnowJSON = "people.json"
let kUserListJSON = "userslist.json"
let kCheckAccountJSON = "checkacount.json"
let kPagesJSON = "pages.json"

// MARK: - Side Menu Items Title
let kHomeDashboardTitle = "Home"
let kLogoutDashboardTitle = "LOG OUT"

let kGoogleMapsServicesAPIKey = "AIzaSyAW4dxBPnPO_PHVQnRG4mg4t8-KxKkO4PM"
let kGooglePlacesServicesAPIKey = kGoogleMapsServicesAPIKey

// MARK: - Twitter API
let kTwitterConsumerKey = "FxRaYJn2KDEJe8JzGUarYlbyb"
let kTwitterConsumerSecretKey = "BC0K3qtv36QHwjnxlUILNe3a64YHcAScBdgIfmD1aQdCIZLbif"

// MARK: - Stripe API
let kStripeAPIKey = "pk_test_2keWRAETlPScDdpG97rf0oA5"
let kAPPLEPayKey = ""

// MARK: - BrainTree Paypal API
let kBraintreePaypal = "com.gsgroup.smartserveios.payments"

// MARK: - Instragram API
var KAUTHURL = "https://api.instagram.com/oauth/authorize/"
var kAPIURl = "https://api.instagram.com/v1/users/"
var KCLIENTID = "e0bb4cfa7705482287688620db0a9235"
var KCLIENTSERCRET = "07396ad542d5440085c557d810fde27d"
var kREDIRECTURI = "http://www.google.com"

let kFCM_Message_token = "AIzaSyDRoMNMFgKYF27RGf_KEQfga3NByL1MugA"

// MARK: - Email Validation
func checkValidEmail(emailTxt: String) -> Bool{
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailTxt)
}

// MARK: - User Default Function

func setDefaultValue(keyValue: String, valueIs: String)
{
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    usrdefault.set(valueIs, forKey: keyValue)
    usrdefault.synchronize()
}

func removeobjectforkey(keyValue: String)
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    defaults.removeObject(forKey: keyValue)
    defaults.synchronize()
}

func setArrayDefaultValue(keyValue: String, valueIs: NSMutableArray)
{
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    usrdefault.set(valueIs, forKey: keyValue)
    usrdefault.synchronize()
}

func getArrayofStrings(keyValue: String) -> NSMutableArray
{
    var objectsAre = NSMutableArray()
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    
    if usrdefault.value(forKey: keyValue) == nil
    {
        
    }
    else
    {
        objectsAre = usrdefault.value(forKey: keyValue) as! NSMutableArray
    }
    
    return objectsAre
}



func getValueOfString(keyValue: String) -> String
{
    var valueOfThatKey = ""
    
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    
    if usrdefault.value(forKey: keyValue) == nil
    {
        valueOfThatKey = ""
    }
    else
    {
        valueOfThatKey = usrdefault.value(forKey: keyValue) as! String
    }
    
    
    return valueOfThatKey
}




// MARK: - Alert Controller
func showAlert(controller: UIViewController, messageStr: String, titleStr: String)
{
//    let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
//    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    //controller.present(alert, animated: true, completion: nil)
   
//    AJAlertController.initialization().showAlertWithOkButton(aStrMessage: messageStr) { (index, title) in
//        print(index,title)
//    }

}



// MARK: - Constant Strings

let kNewsFeedTitle = "News Feed"

let kdatePickrConstant = "Select Date and Time for booking"

let kSplashIntro = "smartServeSplashIntro"

let kNotificationCounter = ""

// Dashboard Titles
let kScanQRCode = "Scan QR Code"
let kOrderHistory = "Order History"
let kGSGReward = "GSG Reward"
let kReviews = "Reviews"
let kLoyalty = "Loyalty"
let kOrderTakway = "Order Takeway"

let kShowTutorials = "tutorials"
let kIsTutorialsOn = "1"
let kIsTutorialsOff = "0"

let CheckBackButton = "checkback"
let onbackButton = "1"
let offbackButton = "0"

let kLoadDashboard = "dashboardtile"
let kLoadOrdrHistory = "takeOrder"
let kLoadNormal = "loadNormal"

let kUserId = "usrId"
let kIsUserLogin = "1"
let kIsUserLogout = "1"
let ksUserFullName = "user_name"
let kUsrName = "usrFullname"
let kUserObject = "usrObject"

let kUserNotifictionCoutner = "notifiitonCounter"

let kSmartCallTitle = "SmartCall!"
let kSmartCallMessage = "Choose an option to continue"

let kSmartCallFeatureCall = "Call Waiter"
let kSmartCallRequestBill = "Request Bill"
let kSmartCallSendMessage = "Send Message"
let kSmartCallMesagePlaceHolder = "Kindly send some message"

let kCancelFeature = "Cancel"
let kSendFeature = "Send"
let kOkBtn = "Ok"

let kError = "Error"
let kShowErrorMsgofCall = "Your Phone Doesn't contain the call feature"
let kShowErrorMsgofCamera = "Your Phone Doesn't contain the camera feature"

let kShowErrorScan = "Scanning not supported"
let kScanQrCodeError = "Your device does not support scanning a code from an item. Please use a device with a camera."

let kStripeSDKText = "Stripe"
let kPaypalSDKText = "Paypal"
let kCreditCardSDKText = "Credit Card"
let kCashSDKText = "Cash on delivery"

let loginEmailNotvalid = "Please enter email address"
let loginPaswordNotvalid = "Please enter password"
let loginEmailIsNotValid = "Please enter valid email address"
let loginPhoneNo = "Please enter phone number"
let signupNot6 = "Password should in between 6 - 12 characters"
let signupNot12 = signupNot6

let loginNametvalid = "Please enter name"
let loginPasswordvalid = "Please enter password"

let kRevrPerson = "Please enter number of person"
let kRevrDateTime = "Please select date and time"

let kdeviceToken = "deviceToken"
let kTYpEIOS = "ios"

let kNotifictionCounter = "NotificationIdentifier"
let kSmartCallNotifictnCounter = "SmartCall"
let kFoodBoxNotifictnCounter = "Foodboxcountr"

let kShareTxtOnSocialMedia = ""

let KReferalFullName = "Please enter full Name"
let KReferalEmailAdd = "Please enter email address"
let KReferalValidEmailAdd = "Please enter valid email address"
let KReferalCompanyName = "Please enter company name"
let KReferalCompanyEmailAdd = "Please enter company email address"
let KReferalCompanyValidEmailAdd = "Please enter valid company email address"
let KReferalCompanyDirector = "Please enter Director name"
let KReferalPhoneNo = "Please enter phone number"


let kReviewStyleSad = "sad"
let kReviewStyleSmile = "smile"
let kReviewStyleNormal = "normal"

let kTwitterLoginMsg = "You have cancel login thorugh twitter, in order to login please click on twitter button"
let kTwitterNoAccountSetupMsg = "No Account Setup in your twitter app, kindly add account in order to continue"

let kVATValueKey = "vatvalue"

let kUserOrderOtherResutantPart1 = "Your cart already contain an items from "
let kUserOrderOtherResutantPart2 = " Would you like to clear the cart and this item from "

let kUserScanQrCode = "scanQrCode"
let kUserScanQrCodeYes = "userscanQrCodeyes"
let kUserScanQrCodeNo = "userscanQrCodeno"

let kUserScanType = "scanTypes"

let kCartCounter = "cartcounter"

let kNotifictionLoadSmartCall = "showCallCall"

let kEmailReceipt = "Receipt is send to your email address"

let kRewardChanges = "You can't change any quantity in reward items"

let kMyRewards = "My GSG Rewards"
let kMyCart = "My Order"
let kSelectPaymentType = "Please select any payment Type"
let kFoodItems = "Please add some items in Food box in order to place order"

let kLoyalityPrefValue = "loyalityValue"

let kEmptyString = ""

let kGSGRewardMsg = "Right now no GSG Rewards Avaliable"

let kOrderStatusType = "Order Type"
let kTypeOfOrder = ""

let kSingltnObjectQrCode = "qrcode"
let kSingltnObjectCollection = "collection"
let kSingltnObjectTakeAway = "takeaway"

let kUserPasword = "pasword"

let kSetTableId = "tableId"

let kSmartCallMsg = "Kindly send message to Waiter"

let kLoginFirstTime = "firstTime"

let kNoRevewAval = "No Resturant Reviews Avaliable"
let kGSGRewardTitle = "No GSG Reward avaliable right now"
let kLoyalityTitle = "No Loyality Points avaliable"
let kOrderHisotyrTitle = "No Order History Found"

let kBillPaid = "Your Bill has send to your email adderss"

let kBillRequestNotFound = "You didn't order dinning food"

let kShowGoogleMaps = "Do you want to see this restaurant in google maps?"

let kShowFeedbackFormMsg = "Please enter some comments to enter feeback"
let promocdeCodeTxt = "Please enter some code in order to avail promocode"

let kPreviousOrder = "order"
let kPrevousOrderScan = "Scan"
let kPrevousOrderTakeAway = "takeawat"

// navigation Titles

let kConnectTitle = "Connect"
let kFeedObjects = "feedObjects"

let kSaveUDID = "saveUDID"

let kAPPName = "Contrav!"
let kNoResturntFound = "No Record found"

let kPassword = "pasword"
let kAccessToken = "accessToken"
let kNoDataFound = "No Data Found.. "
let kNoPicFound = "No Picture found with respect to this user..."

let ktitleFeedback = "Contarv!"

let kphotosImg = "photosimgss"

// MARK: - Event
let kInternetNotAvalible = "No internet available"

let kEvntTitle = "Please enter first name"
let kEvntDescription = "Please enter event description"
let kEvntStartTime = "Please enter Start Time"
let kEvntStartDate = "Please enter Start Date"
let kEvntEndTime = "Please enter End Time"
let kEvntEndDate = "Please enter End Date"
let kEvntLocation = "Please enter Location"

let kPostNotifcaion = "PostNotification"
