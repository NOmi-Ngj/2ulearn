//
//  Helper.swift
//  HopTheLine
//
//  Created by APP on 7/17/17.
//  Copyright © 2017 APP. All rights reserved.
//

import Foundation
import UIKit
import KUIPopupController
import AVFoundation
import AudioToolbox

class Helper {
    static var audioPlayer: AVAudioPlayer?
}

extension Helper {
    
    //download sound file
    static func playFileFromURL(url: String , selfdelegate:UIViewController){
        let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(encodedUrl)
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: URL(string: encodedUrl ?? "")!, completionHandler: { (URL, response, error) -> Void in
            if let url = URL {
                Helper.playSound(url)
                if selfdelegate is ChatConversationVC{
                self.audioPlayer?.delegate = selfdelegate as! AVAudioPlayerDelegate
                }
                
            }
        })
        downloadTask.resume()
    }
    
    //play sound
    static func playSound(_ soundName: URL) {
        //vibrate phone first
        //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundName)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("audioPlayer error \(err.localizedDescription)")
            return
        } else {
            audioPlayer!.prepareToPlay()
        }
        
        //negative number means loop infinity
        audioPlayer!.numberOfLoops = 0
        audioPlayer!.play()
    }
    
 
    //user Methods
    static func getUser() -> User?  {

        if let user = UserDefaults.standard.rm_customObject(forKey: "user") {
            return DataHandler.sharedInstance.mapUserResponse(response: user as AnyObject)
        }
        return nil
    }
    
    static func showQuestionTypeViewController(type:String, navigation:UINavigationController){
        if type == "picture" {
            let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonWithImagesVC.rawValue) as! LessonWithImagesVC
            navigation.pushViewController(vc, animated: true)
        }else if type == "single"{
            let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonWithWordVC.rawValue) as! LessonWithWordVC
            navigation.pushViewController(vc, animated: true)
            
        }else if type == "multiple"{
            let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonWithGrammarVC.rawValue) as! LessonWithGrammarVC
            navigation.pushViewController(vc, animated: true)
        }else if type == "word"{
            let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonWithSentancesVC.rawValue) as! LessonWithSentancesVC
            navigation.pushViewController(vc, animated: true)
        }else if type == "result"{
            let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonsResultsVC.rawValue) as! LessonsResultsVC
            navigation.pushViewController(vc, animated: true)
        }
    }
    
    static func attributedStringWithLineSpacing(string:String , lineSpacing:Int , stralign:String) -> NSMutableAttributedString{
        
        let attributedString = NSMutableAttributedString(string: string)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = CGFloat(lineSpacing) // Whatever line spacing you want in points
        paragraphStyle.alignment = stralign == "center" ? .center:.left
        // *** Apply attribute to string ***
       attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        return attributedString
        
    }
    static func saveUser(user : AnyObject) {
        UserDefaults.standard.rm_setCustomObject(user, forKey: "user")
    }
    
    static func removeUser() {
        UserDefaults.standard.removeObject(forKey: "user")
    }
    
   static func setTextFieldsFont(textField:UITextField , size:CGFloat, isBold:Bool){
        textField.font = isBold == true ? UIFont.appBoldFonts(size: size):UIFont.appRegularFonts(size: size)
    }
    
    static func setButtonsFont(button:UIButton , size:CGFloat, isBold:Bool){
        button.titleLabel?.font = isBold == true ? UIFont.appBoldFonts(size: size):UIFont.appRegularFonts(size: size)
        
    }
    static func setLablesFont(lable:UILabel, size:CGFloat, isBold:Bool){
        lable.font = isBold == true ? UIFont.appBoldFonts(size: size):UIFont.appRegularFonts(size: size)
    }
    
    static let REGEX_EMAIL = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    static let REGEX_PASSWORD_LIMIT =  "^.{8,20}$"
    static let REGEX_PASSWORD  = "[[a-zA-Z0-9]]{8,20}$"
    static let REGEX_USER_NAME = "[[._-a-zA-Z0-9 ]]{3,20}$" /*"[A-Za-z0-9\\_.- ]{3,20}"*/ /* "/^[a-zA-Z0-9 _.-\\ ]{3,20}$/" */
    static let REGEX_NAME  = "[[a-zA-Z]]{3,20}$"
    static let REGEX_USER_NAME_LIMIT  = "^.{3,15}$"
    
    static func validateEmailString (_ strEmail: String) -> Bool {
        let emailText = NSPredicate(format:"SELF MATCHES [c]%@",REGEX_EMAIL)
        return (emailText.evaluate(with: strEmail))
    }
//    static func validatePasswordAllowCharacters (_ strEmail: String) -> Bool {
//        let passwordText = NSPredicate(format:"SELF MATCHES [c]%@",REGEX_PASSWORD)
//        return (passwordText.evaluate(with: strEmail))
//    }
//    static func validatePasswordLimitString (_ strEmail: String) -> Bool {
//        let passwordText = NSPredicate(format:"SELF MATCHES [c]%@",REGEX_PASSWORD_LIMIT)
//        return (passwordText.evaluate(with: strEmail))
//    }
//    static func validateNameString(_ strUserName: String) -> Bool {
//        let usernameText = NSPredicate(format:"SELF MATCHES [c]%@", REGEX_USER_NAME)
//        if (strUserName.trimmingCharacters(in: .whitespaces).isEmpty == false && usernameText.evaluate(with: strUserName) == true){
//            return true
//        }
//        return false
//    }
//    static func validateFirstLastName(_ strEmail: String) -> Bool {
//        let nameText = NSPredicate(format:"SELF MATCHES [c]%@",REGEX_NAME)
//        return (nameText.evaluate(with: strEmail))
//    }
//
//    static func setSelectedState(button : UIButton) {
//        button.backgroundColor = UIColor.appGreenColor()
//        button.layer.borderColor = UIColor.white.cgColor
//        button.setTitleColor(.white, for: .normal)
//        button.layer.borderWidth = 0.0
//    }
//
//    static func setUnselectedState(button : UIButton){
//        button.backgroundColor = .white
//        button.setTitleColor(.darkGray, for: .normal)
//        button.layer.borderColor = UIColor.darkGray.cgColor
//        button.layer.borderWidth = 1.0
//    }
    
//    static func setRoundedCorners(view : Any, width : CGFloat) {
//        (view as AnyObject).layer.borderColor = UIColor.lightGray.cgColor
//        (view as AnyObject).layer.borderWidth = width
//    }
//
//    static func localConversion(date:Date)-> Date {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        //  let defaultTimeZoneStr = formatter.string(from: date as Date)
//        formatter.timeZone = NSTimeZone.local
//        let utcTimeZoneStr = formatter.string(from: date as Date)
//        let convertedDate = formatter.date(from: utcTimeZoneStr)
//        return convertedDate!
//    }
//
    static func getDeviceType() -> ScreenType{
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 2436:
            return .iPhoneX
        default:
            return .Unknown
        }
    }
}
enum ScreenType: String {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case iPhoneX
    case Unknown
}

//class FromTopTranslationAnimator: KUIPopupContentViewAnimator {
//    func animate(_ parameter: KUIPopupContentViewAnimatorStateParameter, completion: @escaping (Bool) -> Void) {
//        let isShow = parameter.isShow
//        let containerView = parameter.containerView
//        let containerViewY = parameter.containerViewCenterY
//        let screenHeight = UIScreen.main.bounds.height
//
//        containerViewY.constant = isShow ? -screenHeight : 0.0
//        containerView.superview?.layoutIfNeeded()
//
//        containerViewY.constant = isShow ? 0.0 : -screenHeight
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.1, options: .allowUserInteraction, animations: {
//            containerView.superview?.layoutIfNeeded()
//
//            if !isShow {
//                containerView.superview?.alpha = 0.0
//            }
//        }, completion: completion)
//    }
//}

//extension String {
//    subscript (bounds: CountableClosedRange<Int>) -> String {
//        let start = index(startIndex, offsetBy: bounds.lowerBound)
//        let end = index(startIndex, offsetBy: bounds.upperBound)
//        return String(self[start...end])
//    }
//
//    subscript (bounds: CountableRange<Int>) -> String {
//        let start = index(startIndex, offsetBy: bounds.lowerBound)
//        let end = index(startIndex, offsetBy: bounds.upperBound)
//        return String(self[start..<end])
//    }
//
//    public func index(of char: Character) -> Int? {
//        if let idx = characters.index(of: char) {
//            return characters.distance(from: startIndex, to: idx)
//        }
//        return nil
//    }
//}


class FromTopTranslationAnimator: KUIPopupContentViewAnimator {
    func animate(_ parameter: KUIPopupContentViewAnimatorStateParameter, completion: @escaping (Bool) -> Void) {
        let isShow = parameter.isShow
        let containerView = parameter.containerView
        let containerViewY = parameter.containerViewCenterY
        let screenHeight = UIScreen.main.bounds.height
        
        containerViewY.constant = isShow ? -screenHeight : 0.0
        containerView.superview?.layoutIfNeeded()
        
        containerViewY.constant = isShow ? 0.0 : -screenHeight
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.1, options: .allowUserInteraction, animations: {
            containerView.superview?.layoutIfNeeded()
            
            if !isShow {
                containerView.superview?.alpha = 0.0
            }
        }, completion: completion)
    }
}

