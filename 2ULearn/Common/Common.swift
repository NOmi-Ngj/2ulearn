//
//  Common.swift
//  Show Pro
//
//  Created by waqar jamsheed on 13/09/2017.
//  Copyright © 2017 waqar jamsheed. All rights reserved.
//

import UIKit
import Foundation

open class Common {
    static func getDate(format: String, date: Date) -> String {
        let timeFormat = DateFormatter()
        timeFormat.locale = Locale(identifier:"en_US")
        timeFormat.dateFormat = format
        return timeFormat.string(from: date)
    }
    
    static func dateFormat(date:String?, dateformat:String,desiredFormat:String)-> String {
        guard date == nil || date == "00:00" else {
            let formatter = DateFormatter()
            formatter.dateFormat = dateformat
            formatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let utcDate: Date = formatter.date(from: date!)!
            
            let formatter2 = DateFormatter()
            formatter2.dateFormat = desiredFormat
            formatter2.locale = Locale(identifier: "en_US_POSIX")
            
            let dateString = formatter2.string(from: utcDate)
            
            return dateString
        }
        return ""
    }
    
    static func isTesting() -> Bool{
        let userDefaults = UserDefaults.standard
        if let isTesting = userDefaults.value(forKey: "isTesting") as? Bool {
            return false
        }else{
            return false
        }
    }
    static func getValueForKey(keyValue: String) -> String
    {
        let defaults = UserDefaults.standard
        defaults.synchronize()
        UserDefaults.standard.synchronize()
        
        let valueForStr = defaults.value(forKey: keyValue)
        if(valueForStr != nil)
        {
            return valueForStr as! String
        }
        else{
            return ""
        }
        
    }
    static func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        }
        else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        }
        else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
}
 
class StaticAppData{
    
    
    static let sliderText:[String] = ["Lorem Ipsum 1","Lorem Ipsum 2","Lorem Ipsum 3","Lorem Ipsum 4","Lorem Ipsum 5","Lorem Ipsum 6"]
    static let sliderLogo:[UIImage] = [#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo"),#imageLiteral(resourceName: "logo")] // 6 items    
    static let strLoremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries."
    
    
}

class Constants {
    
    static let App_HeaderTitleFontSize = CGFloat.init(21)
    static let APP_ALERT_TITLE = ""
    static let APP_Name = "Empowered"
    static let APP_Info = "Information"
    static let APP_Error = "Error"
    static let APP_Success = "Success"
    static let fcmToken = "fcmToken"
    static let badgeNotification = "badgeNotification"
    static let badgeMessage = "badgeMessage"
    static let LoginSuccesfull = "didSentfcmToken"
//    static let notificationBackground = Notification.Name("notificationBackground")
//    static let notificationForeground = Notification.Name("notificationForeground")
//    static let notificationForegroundCountHandler = Notification.Name("notificationForegroundCountHandler")
//    static let notificationForegroundHome = Notification.Name("notificationForegroundHome")
//    static let notificationRemoveSpinner = Notification.Name("removeSpinner")
//    static let notificationAddSpinner = Notification.Name("addSpinner")
    
    
}


