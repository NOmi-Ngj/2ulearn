//
//  BooksShelfTVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

protocol seeAllBooksDelegates: class {
    func onSeeAllTapped(sender:UIButton)
}

class BooksShelfTVC: UITableViewCell , NibLoadableView, ReusableView {
    

    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemDecriptionLable: UILabel!
    @IBOutlet weak var itemSeeAllLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var bookImage1: UIImageView!
    @IBOutlet weak var bookImage2: UIImageView!
    @IBOutlet weak var bookImage3: UIImageView!
    weak var delegate : seeAllBooksDelegates!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(title:String, details:String, img1:String,img2:String,img3:String){
 
        itemTitleLabel.text = title
        itemDecriptionLable.text = details 
        setImage(image: bookImage1, imgUrl: img1)
        setImage(image: bookImage2, imgUrl: img2)
        setImage(image: bookImage3, imgUrl: img3)
        
        itemTitleLabel.font = UIFont.appMontFonts(size: 19)
        itemDecriptionLable.font = UIFont.appMontFonts(size: 13)
        itemSeeAllLabel.font = UIFont.appMontFonts(size: 15)
    }
    func setImage(image:UIImageView, imgUrl:String){
        if imgUrl == "" {
            image.image = #imageLiteral(resourceName: "graybg")
        }else{
            image.yy_setImage(with: URL(string: imgUrl), placeholder:#imageLiteral(resourceName: "graybg"))
        }
    }
    
    @IBAction func seeAllTapped(_ sender: UIButton) {
        delegate.onSeeAllTapped(sender: sender)
    }
}
