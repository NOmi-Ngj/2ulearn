//
//  BooksListCVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class BooksListCVC: UICollectionViewCell , NibLoadableView, ReusableView {
    

    @IBOutlet weak var bookTitleName: UILabel!
    @IBOutlet weak var booksImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(title:String, imgUrl:String){
        bookTitleName.text = title.capitalized
        booksImage.yy_setImage(with: URL(string: imgUrl), placeholder:#imageLiteral(resourceName: "graybg"))
        bookTitleName.font = UIFont.appMontFonts(size: 15)
    }
}
