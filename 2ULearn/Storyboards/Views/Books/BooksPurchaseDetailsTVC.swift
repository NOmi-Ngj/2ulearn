//
//  BooksPurchaseDetailsTVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import YYWebImage
class BooksPurchaseDetailsTVC: UITableViewCell , NibLoadableView, ReusableView {
    
    @IBOutlet weak var bookTitle: UILabel!
    @IBOutlet weak var bookDetails: UILabel!
    @IBOutlet weak var bookPrice: UILabel!
    @IBOutlet weak var btnRemoveFromList: UIButton!
    @IBOutlet weak var imgRemoveFromList: UIImageView!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var deleteImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(title:String,details:String,price:String,imageUrl:String){
        if imageUrl != "" {
            DispatchQueue.main.async {
                self.bookImage.yy_setImage(with: URL(string: imageUrl), placeholder:#imageLiteral(resourceName: "graybg"))
            }
        }
        bookTitle.text = title
        bookPrice.text = price
        bookDetails.attributedText = Helper.attributedStringWithLineSpacing(string: details, lineSpacing: 5, stralign: "left")
        setFonts()
    }
    
    func setViewForCarts(){
        btnRemoveFromList.isHidden = false
        imgRemoveFromList.isHidden = false
    }
    func setFonts(){
        bookTitle.font = UIFont.appMontFonts(size: 17)
        bookDetails.font = UIFont.appMontFonts(size: 14)
        bookPrice.font = UIFont.appMontFonts(size: 17)
    }
    
}
