//
//  LoadingAndErrorView.swift
//  Konnect
//
//  Created by APP on 12/27/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit

class LoadingAndErrorView: UIView, NibLoadableView {
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var retryButton: UIButton!
    
    var retryHandler: ((_ sender: UIButton) -> Void)?
    
    func setMessage(attributedText: NSAttributedString, canRetry: Bool) {
        retryButton.isEnabled = canRetry
        textLabel.attributedText = attributedText
    }
    
    //My Methods
    
    func showLoading() {
        retryButton.isEnabled = false
        textLabel.isHidden = true
        
        activityIndicator.startAnimating()
//        CustomSpinner.sharedInstance.objAJProgressView.show()
    }
    
    func showErrorView(canRetry: Bool) {
        activityIndicator.stopAnimating()
//        CustomSpinner.sharedInstance.objAJProgressView.hide()
        
        retryButton.isEnabled = canRetry
        textLabel.isHidden = false
    }
    
    func showNothing() {
        activityIndicator.stopAnimating()
//        CustomSpinner.sharedInstance.objAJProgressView.hide()
        retryButton.isHidden = true
        textLabel.isHidden = true
    }
    
    //MARK: My IBActions
    
    @IBAction func retryButtonTapped(sender: UIButton) {
        retryHandler?(sender)
    }
}

