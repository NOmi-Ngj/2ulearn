//
//  CollectionViewLoadMoreView.swift
//  Tree
//
//  Created by Mac OS X on 07/03/2017.
//  Copyright © 2017 abc. All rights reserved.
//

import UIKit

class CollectionViewLoadMoreView: UICollectionReusableView, ReusableView, NibLoadableView {

    var loadingView: LoadingAndErrorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        loadingView = Bundle.main.loadNibNamed(LoadingAndErrorView.NibName, owner: nil, options: nil)?.first as! LoadingAndErrorView
        loadingView.backgroundColor = .clear
        addSubview(loadingView)
        
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        loadingView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        loadingView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        loadingView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        loadingView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    func setUpFeedLoadMoreView(loadMoreModel: FeedLoadMoreModel) {
        if loadMoreModel.isLoading {
            loadingView.showLoading()
        } else if let error = loadMoreModel.error {
            let errorAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white,
                                   NSAttributedStringKey.font : UIFont(name: "CircularStd-Medium", size: 17)]
            let errorMessage = error.localizedDescription
            let attributedError = NSMutableAttributedString(string: errorMessage, attributes: errorAttributes)
            
            loadingView.setMessage(attributedText: attributedError, canRetry: true)
            loadingView.showErrorView(canRetry: true)
            loadingView.retryButton.isHidden = false
        } else {
            loadingView.showNothing()
        }
    }
}

class FeedLoadMoreModel {
    var isLoading = false
    var canLoadMore = false
    var error: NSError?
    
    convenience init(isLoading: Bool, canLoadMore: Bool, error: NSError?) {
        self.init()
        
        self.isLoading = isLoading
        self.canLoadMore = canLoadMore
        self.error = error
    }
}

