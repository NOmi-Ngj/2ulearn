//
//  audioListingTVC.swift
//  2ULearn
//
//  Created by NoumanGul on 5/6/19.
//  Copyright © 2019 CMolds. All rights reserved.
//

import UIKit
import YYWebImage

protocol heartButtonTappedDelegates{
    func heartTappedWithId(MediaID:String,isSelected:Bool,index:Int)
}

func color(_ rgbColor: Int) -> UIColor{
    return UIColor(
        red:   CGFloat((rgbColor & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbColor & 0x00FF00) >> 8 ) / 255.0,
        blue:  CGFloat((rgbColor & 0x0000FF) >> 0 ) / 255.0,
        alpha: CGFloat(1.0)
    )
}
class audioListingTVC: UITableViewCell, NibLoadableView, ReusableView {
    
    var heartDelegate : heartButtonTappedDelegates!
    var imageViewPopUp : fullImageView?
    var media:mMedia?
    let colors = [
        DotColors(first: color(0x7DC2F4), second: color(0xE2264D)),
        DotColors(first: color(0xF8CC61), second: color(0x9BDFBA)),
        DotColors(first: color(0xAF90F4), second: color(0x90D1F9)),
        DotColors(first: color(0xE9A966), second: color(0xF8C852)),
        DotColors(first: color(0xF68FA7), second: color(0xF6A2B8))
    ]
    @IBOutlet weak var heartButton: FaveButton!
    
    @IBOutlet weak var ch_title: UILabel!
    @IBOutlet weak var en_title: UILabel!
    @IBOutlet weak var imageMedia: UIImageView!
    @IBOutlet weak var play_icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.heartButton.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(dataMedia:mMedia){
        self.media = dataMedia
        let isSelected = self.media?.is_favourite == 1 ? true:false
        self.heartButton?.setSelected(selected: isSelected, animated: false)
        self.ch_title.text = self.media?.title ?? ""
        self.en_title.text = self.media?.description ?? ""
        
    }
    func setImageFromUrl(){
        if self.media?.type == "audio"{
            imageMedia.image = #imageLiteral(resourceName: "audio_icon")
        }else{
            imageMedia.yy_setImage(with: URL(string: self.media?.cover_image?.scaledImageURL() ?? ""), placeholder:#imageLiteral(resourceName: "goal4"))
        }
        
    }
    @IBAction func heartbuttonTapped(){
        heartButton.isUserInteractionEnabled = false
        
    }
    
    @IBAction func imageShowbuttonTapped(){
        let view = showImagePopUpForMediaViews()
        
        if self.media?.type == "audio"{
            return
        }else{
            view.backGroundImage.yy_setImage(with: URL(string: self.media?.cover_image?.scaledImageURL() ?? ""), placeholder:#imageLiteral(resourceName: "goal4"))
        }
        view.show()
    }
    
    func showImagePopUpForMediaViews() -> fullImageView {
        imageViewPopUp = fullImageView.instanceFromNib()
        imageViewPopUp?.animator = FromTopTranslationAnimator()
        return imageViewPopUp!
    }
}

extension audioListingTVC:FaveButtonDelegate{
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        heartButton.isUserInteractionEnabled = true
        heartDelegate.heartTappedWithId(MediaID: self.media?.id ?? "", isSelected: selected, index: faveButton.tag)
    }
    
    func faveButtonDotColors(_ faveButton: FaveButton) -> [DotColors]?{
        if( faveButton === heartButton){
            return colors
        }
        return nil
    }
    
}

