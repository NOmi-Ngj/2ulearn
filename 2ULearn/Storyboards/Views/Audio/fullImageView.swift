//
//  fullImageView.swift
//  2ULearn
//
//  Created by NoumanGul on 5/17/19.
//  Copyright © 2019 CMolds. All rights reserved.
//

import UIKit
import KUIPopupController

class fullImageView: UIView, NibLoadableView, KUIPopupContentViewProtocol {
    var animator: KUIPopupContentViewAnimator?
    weak var delegate : customAlertViewDelegate!
    @IBOutlet weak var backGroundImage: UIImageView!
    

    class func instanceFromNib() -> fullImageView? {
        let view = Bundle.main.loadNibNamed(fullImageView.NibName, owner: nil, options: nil)?.first as? fullImageView
        return view
        
    }
    
    @IBAction func okBtnTapped(_ sender: Any) {
        dismiss(true)
    }
}
