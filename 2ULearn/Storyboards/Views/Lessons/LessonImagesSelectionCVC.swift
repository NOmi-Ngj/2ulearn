//
//  LessonImagesSelectionCVC.swift
//  2ULearn
//
//  Created by mac on 6/14/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import KUIPopupController

protocol LessonImagesSelectionCVCDelegates: class {
    func onPressNext()
}
class LessonImagesSelectionCVC: UIView, NibLoadableView, KUIPopupContentViewProtocol {

    
    var animator: KUIPopupContentViewAnimator?
    weak var delegate : LessonImagesSelectionCVCDelegates!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var wrongRightLabel: UILabel!
    @IBOutlet weak var lessonLabel: UILabel!
    
    class func instanceFromNib() -> LessonImagesSelectionCVC? {
        let view = Bundle.main.loadNibNamed(LessonImagesSelectionCVC.NibName, owner: nil, options: nil)?.first as? LessonImagesSelectionCVC
        return view
        
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
        delegate.onPressNext()
    }

}
