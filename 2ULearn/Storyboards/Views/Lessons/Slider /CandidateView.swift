//
//  HomeJobSeekerView.swift
//  GoodJob
//
//  Created by Macbook on 4/17/18.
//  Copyright © 2018 Waqar Apps. All rights reserved.
//

import UIKit
import FSPagerView


protocol startLearningSelectionCVCDelegates: class {
    func onPressStartLearning(index:Int)
}

class CandidateView: FSPagerViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var lessonNo: UILabel!
    @IBOutlet weak var startLearning: UIButton!
    @IBOutlet weak var lessonTitle: UILabel!
    @IBOutlet weak var lessonDetail: UILabel!
    @IBOutlet weak var lessonImage: UIImageView!
    weak var delegate : startLearningSelectionCVCDelegates!
    
    override func layoutSubviews() {
       
//        DispatchQueue.main.async {
//            self.layer.cornerRadius = 10
//            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10)
//            self.layer.masksToBounds = false
//            self.layer.shadowColor =  UIColor.black.cgColor
//            self.layer.shadowOffset = CGSize(width: 1 , height: 1);
//            self.layer.shadowOpacity = 0.2
//            self.layer.shadowPath = shadowPath.cgPath
//        }
        applicationFontsSetting()
    }
    
    
    func applicationFontsSetting(){
        lessonNo.font = UIFont.appMontFonts(size: 17)
        lessonTitle.font = UIFont.appRegularFonts(size: 22)
        lessonDetail.font = UIFont.appMontFonts(size: 15)
        startLearning.titleLabel?.font = UIFont.appMontFonts(size: 17)
    }
    @IBAction func onPress_StartLearnings(_ sender: UIButton) {
        
        delegate.onPressStartLearning(index: sender.tag)
    }
    
    
}
