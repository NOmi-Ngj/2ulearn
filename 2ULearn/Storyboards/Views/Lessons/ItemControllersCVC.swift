//
//  ItemControllersCVC.swift
//  2ULearn
//
//  Created by mac on 7/2/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class ItemControllersCVC: UICollectionViewCell , NibLoadableView, ReusableView {


    @IBOutlet weak var itemImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(image:String){
        itemImage.image = UIImage.init(named: image)
    }
}
