//
//  LessonsImagesView.swift
//  2ULearn
//
//  Created by mac on 6/14/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class LessonsImagesView: UICollectionViewCell, NibLoadableView, ReusableView {
    

    @IBOutlet weak var titleEnglishLabel: UILabel!
    @IBOutlet weak var titleChineeseLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var selectionIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setTitles(english:String,Chineese:String){
        self.titleEnglishLabel.text = english
        self.titleChineeseLabel.text = Chineese
        
        self.titleChineeseLabel.font = UIFont.appRegularFonts(size: 16)
        self.titleEnglishLabel.font = UIFont.appRegularFonts(size: 16)
    }
    func setImage(image:UIImage){
        self.image.image = image
    }

}
