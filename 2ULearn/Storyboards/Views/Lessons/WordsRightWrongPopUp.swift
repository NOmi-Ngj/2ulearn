//
//  WordsRightWrongPopUp.swift
//  2ULearn
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import KUIPopupController

class WordsRightWrongPopUp: UIView, NibLoadableView, KUIPopupContentViewProtocol {
    
    
    var animator: KUIPopupContentViewAnimator?
    weak var delegate : LessonImagesSelectionCVCDelegates!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var wrongRightLabel: UILabel!
    @IBOutlet weak var lessonLabel: UILabel!
    
    class func instanceFromNib() -> WordsRightWrongPopUp? {
        let view = Bundle.main.loadNibNamed(WordsRightWrongPopUp.NibName, owner: nil, options: nil)?.first as? WordsRightWrongPopUp
        return view
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
//        dismiss(true)
        delegate.onPressNext()
    }
}
