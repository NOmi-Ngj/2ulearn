//
//  ValidationAlertView.swift
//  Empowered
//
//  Created by Nouman Gul Junejo on 5/15/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit
import KUIPopupController

/// A custom delgate method to access the location events.
@objc protocol ButtonOKPressedDelegates {
    
    @objc optional func okButtonPressed()
}

class ValidationAlertView: UIView, NibLoadableView, KUIPopupContentViewProtocol {
    
    var animator: KUIPopupContentViewAnimator?
    weak var delegate : customAlertViewDelegate!
    weak var okDelegate : ButtonOKPressedDelegates!
    @IBOutlet weak var backGroundView: UIView!
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var alertTitleLbl: UILabel!
    @IBOutlet weak var OkButton: UIButton!
    var okButtonDelegates = false
    
    class func instanceFromNib() -> ValidationAlertView? {
        let view = Bundle.main.loadNibNamed(ValidationAlertView.NibName, owner: nil, options: nil)?.first as? ValidationAlertView
        return view
        
    }
    @IBAction func okBtnTapped(_ sender: Any) {
        
        if okButtonDelegates{
            okDelegate.okButtonPressed?()
        }
        
        dismiss(true)
    }
}


