//
//  CustomPopUpView.swift
//  Empowered
//
//  Created by Nouman Gul Junejo on 5/9/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit
import KUIPopupController

protocol customAlertViewDelegate: class {
    func okButtonPressed()
}
class CustomPopUpView: UIView, NibLoadableView, KUIPopupContentViewProtocol {

    var animator: KUIPopupContentViewAnimator?
    weak var delegate : customAlertViewDelegate!
    @IBOutlet weak var backGroundView: UIView!
    
    @IBOutlet weak var messageLbl: UILabel!
    
    class func instanceFromNib() -> CustomPopUpView? {
        let view = Bundle.main.loadNibNamed(CustomPopUpView.NibName, owner: nil, options: nil)?.first as? CustomPopUpView
        return view
        
    }
    @IBAction func okBtnTapped(_ sender: Any) {
        dismiss(true)
    } 
}
