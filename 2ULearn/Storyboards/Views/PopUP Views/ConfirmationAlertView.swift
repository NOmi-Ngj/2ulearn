//
//  ConfirmationAlertView.swift
//  Empowered
//
//  Created by Nouman Gul Junejo on 5/9/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit
import KUIPopupController

protocol customConfirmationAlertViewDelegate: class {
    func okButtonPressed(index:Int)
}

class ConfirmationAlertView: UIView, NibLoadableView, KUIPopupContentViewProtocol {
    
    var animator: KUIPopupContentViewAnimator?
    weak var delegate : customConfirmationAlertViewDelegate!
    @IBOutlet weak var backGroundView: UIView!
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var YesBtn: UIButton!
    
    class func instanceFromNib() -> ConfirmationAlertView? {
        let view = Bundle.main.loadNibNamed(ConfirmationAlertView.NibName, owner: nil, options: nil)?.first as? ConfirmationAlertView
        return view
        
    }
    @IBAction func cancelBtnTapped(_ sender: Any) {
        dismiss(true)
    }
    @IBAction func YesBtnTapped(_ sender: UIButton) {
        delegate.okButtonPressed(index: sender.tag)
    }
}
