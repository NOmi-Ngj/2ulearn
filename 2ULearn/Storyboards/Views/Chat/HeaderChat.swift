//
//  HeaderChat.swift
//  2ULearn
//
//  Created by Nouman Gul Junejo on 8/16/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class HeaderChat: UIView, NibLoadableView{
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var NavigationHeader: UIView!
    @IBOutlet weak var XcrossIcon: UIImageView!
    
    class func instanceFromNib() -> HeaderChat? {
        let view = Bundle.main.loadNibNamed(HeaderChat.NibName, owner: nil, options: nil)?.first as? HeaderChat
        let templateImage = view?.XcrossIcon.image?.withRenderingMode(.alwaysTemplate)
        view?.XcrossIcon.image = templateImage
        view?.XcrossIcon.tintColor = UIColor.white
        
        return view
        
    }

}
