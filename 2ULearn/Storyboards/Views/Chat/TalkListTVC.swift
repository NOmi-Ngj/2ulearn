//
//  TalkListTVC.swift
//  2ULearn
//
//  Created by mac on 6/27/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class TalkListTVC: UITableViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var imgTrophies: UIImageView!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var itemDetails: UILabel!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemTitleImagePlaceHolder: UILabel!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var imgRightArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(title:String, image1:UIImage,detailText:String){
        
        itemTitle.text = title
        itemDetails.text = detailText
        imgUserProfile.image = image1
        
        let index2 = title.index(title.startIndex, offsetBy: 0) //will call succ 2 times
        let lastChar: Character = title[index2] //now we can index!
        let string = "\(lastChar)"
        itemTitleImagePlaceHolder.text = string.capitalized
        setFonts()
    }
    func setData(title:String, imageURL:String,detailText:String){
        self.imgUserProfile.yy_setImage(with: URL(string: imageURL), placeholder:#imageLiteral(resourceName: "graybg"))
        
        DispatchQueue.main.async {
            self.imgUserProfile.cornerRadiuss = self.imgUserProfile.frame.size.height/2
        }
        let index2 = title.index(title.startIndex, offsetBy: 0) //will call succ 2 times
        let lastChar: Character = title[index2] //now we can index!
        let string = "\(lastChar)"
        itemTitleImagePlaceHolder.text = string.capitalized
        
        itemTitle.text = title
        itemDetails.text = detailText
        setFonts()
    }
    func setFonts(){
        itemTitle.font = UIFont.appMontFonts(size: 17)
        itemTitleImagePlaceHolder.font = UIFont.appMontFonts(size: 23)
        itemDetails.font = UIFont.appMontFonts(size: 14)
    }
}
