//
//  ConversationTVC.swift
//  2ULearn
//
//  Created by mac on 6/28/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class ConversationTVC: UITableViewCell , NibLoadableView, ReusableView {

    
    @IBOutlet weak var itemTitleEnglish: UILabel!
    @IBOutlet weak var itemTitleMandarin: UILabel!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var otherUserView: UIView!
    @IBOutlet weak var userTimeStamp: UILabel!
    @IBOutlet weak var otherUserTimeStamp: UILabel!
    
    @IBOutlet weak var userSoundPlayButton: UIButton!
    @IBOutlet weak var otherUserSoundPlayButton: UIButton!
    @IBOutlet weak var imgUserSoundPlayButton: UIImageView!
    @IBOutlet weak var imgOtherUserSoundPlayButton: UIImageView!
    @IBOutlet weak var itemTitleEnglishOtherUser: UILabel!
    @IBOutlet weak var itemTitleMandarinOtherUser: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    func setData(english:String,mandarin:String , isMyMessage:Bool){
//        userView.isHidden = true
//        otherUserView.isHidden = true
//        if isMyMessage{
//            userView.isHidden = false
//            itemTitleMandarin.text = mandarin
//            itemTitleEnglish.text = english
//        }else{
//            otherUserView.isHidden = false
//            itemTitleEnglishOtherUser.text = english
//            itemTitleMandarinOtherUser.text = mandarin
//        }
//
//        //setFonts()
//    }
    func setData(english:String,mandarin:String , isMyMessage:Bool, timeStamp:Int){
        
        debugPrint(timeStamp)
        let messageDate = Date.init(milliseconds: timeStamp)
        debugPrint(messageDate)
        let date = Common.timeAgoSinceDate(messageDate, currentDate: Date(), numericDates: true)
 
        userView.isHidden = true
        otherUserView.isHidden = true
        if isMyMessage{
            userView.isHidden = false
            itemTitleMandarin.text = mandarin
            itemTitleEnglish.text = english
            itemTitleEnglishOtherUser.text = ""
            itemTitleMandarinOtherUser.text = ""
            userTimeStamp.text = date
        }else{
            otherUserView.isHidden = false
            itemTitleEnglishOtherUser.text = english
            itemTitleMandarinOtherUser.text = mandarin
            itemTitleMandarin.text = ""
            itemTitleEnglish.text = ""
            otherUserTimeStamp.text = date
        }
        
        
        //setFonts()
    }
    func setFonts(){
        itemTitleMandarin.font = UIFont.appMontFonts(size: 16)
        itemTitleEnglish.font = UIFont.appMontFonts(size: 16)
        itemTitleMandarinOtherUser.font = UIFont.appMontFonts(size: 16)
        itemTitleEnglishOtherUser.font = UIFont.appMontFonts(size: 16)
    }
}

