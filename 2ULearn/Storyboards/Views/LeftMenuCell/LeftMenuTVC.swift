//
//  LeftMenuTVC.swift
//  Empowered
//
//  Created by Nouman Gul Junejo on 5/10/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit

class LeftMenuTVC: UITableViewCell , NibLoadableView, ReusableView {

    @IBOutlet weak var menuItemsLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(text:String){
        menuItemsLbl.text = text
    }
    
}
