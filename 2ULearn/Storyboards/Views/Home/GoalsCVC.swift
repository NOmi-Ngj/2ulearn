//
//  GoalsCVC.swift
//  2ULearn
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import YYWebImage
class GoalsCVC: UICollectionViewCell , NibLoadableView, ReusableView {

    
    @IBOutlet weak var lessonsImages: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setImage(viewImageURL : String ){
        self.lessonsImages.yy_setImage(with: URL(string: viewImageURL), placeholder:#imageLiteral(resourceName: "graybg"))
    }
}
