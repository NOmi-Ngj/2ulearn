//
//  HomeFeedCVC.swift
//  Free Agents
//
//  Created by Nouman Gul Junejo on 1/24/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit
import YYWebImage
class HomeFeedCVC: UICollectionViewCell , NibLoadableView, ReusableView {
    
    
     @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var detailedLable: UILabel!
    
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func setImage(viewImageURL : String, title:String, Details:String){
        self.image.yy_setImage(with: URL(string: viewImageURL), placeholder:#imageLiteral(resourceName: "graybg"))
        DispatchQueue.main.async {
            self.image.cornerRadiuss = self.image.frame.size.height/2
        }
        titleLable.text = title
        detailedLable.text = Details
        
        titleLable.font = UIFont.appRegularFonts(size: 22)
        detailedLable.font = UIFont.appMontFonts(size: 15)
    }
   
  
}
