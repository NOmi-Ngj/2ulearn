//
//  GoalsTVC.swift
//  2ULearn
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

protocol lessonSelectionDelegate: class {
    func didSelectLessons(lessons:[QuestionsList]? ,goalIndex:Int, index:Int)
}

class GoalsTVC: UITableViewCell , NibLoadableView, ReusableView {


    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDetails: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    weak var lessonDelegates:lessonSelectionDelegate!
    var lessonsA = [LessonsList]()
    var GoalIndex:Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.emptyDataSetSource = self
        self.collectionView.emptyDataSetDelegate = self
        self.collectionView.register(UINib(nibName: GoalsCVC.NibName, bundle: nil), forCellWithReuseIdentifier: GoalsCVC.reuseIdentifier)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(title:String, details:String, lessons:[LessonsList]?){
        lessonsA = lessons ?? []
        itemTitle.text = title
        itemDetails.text = details
        collectionView.reloadData()
        setFonts()
        
    }
    func setGoalIndex(index:Int){
        GoalIndex = index
    }
    
    func setFonts(){
        itemTitle.font = UIFont.appMontFonts(size: 24)
        itemDetails.font = UIFont.appMontFonts(size: 16)
    }
    
}

extension GoalsTVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lessonsA.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GoalsCVC.reuseIdentifier, for: indexPath) as! GoalsCVC
        cell.setImage(viewImageURL: lessonsA[indexPath.row].image_url?.scaledImageURL() ?? "")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        lessonDelegates.didSelectLessons(lessons: lessonsA[indexPath.row].questions ?? [], goalIndex: self.GoalIndex, index: indexPath.row)
    }
}
extension GoalsTVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.size.width)/3-10
        let height = (collectionView.frame.size.height)
        return CGSize(width:  width , height: height)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension GoalsTVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Lessons Not Found."
        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
        return attributedString
    }
}
