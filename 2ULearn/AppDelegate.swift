//
//  AppDelegate.swift
//  2ULearn
//
//  Created by mac on 6/12/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
//import CRNotifications
import AVFoundation
import AudioToolbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    var audioPlayer: AVAudioPlayer?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Thread.sleep(forTimeInterval: 3.0)
        UIApplication.shared.statusBarStyle = .lightContent
        DataHandler.sharedInstance.getPoints { (points) in
            debugPrint(points)
        }
        //IQKeyboard manager setup
        IQKeyboardManager.sharedManager().enable = true
        var isTesting = false
        if isSimulator {
            isTesting = true
        }
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(isTesting, forKey: "isTesting")
        userDefaults.synchronize()
        
        FirebaseApp.configure()
        promptUserToRegisterPushNotifications()
         self.navigateViewOnStart()
        UIApplication.shared.applicationIconBadgeNumber = 0
        return true
    }
    
    // MARK: PUSH
    func promptUserToRegisterPushNotifications() {
        // Register for Push Notifications
        
        let application: UIApplication = UIApplication.shared
        
        Messaging.messaging().delegate = self
        //        Messaging.messaging().shouldEstablishDirectChannel = true
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    func navigateViewOnStart(){
        if Helper.getUser() != nil{
            let user = Helper.getUser()
            if (user?.is_active?.isEqual("2"))!{
                
                let vc = UIStoryboard.homeStoryboard().instantiateViewController(withIdentifier: HomeControllers.MainVC.rawValue) as! MainVC
                self.window?.rootViewController = vc
                
            }else{
//                ChatUser.logOutUser(completion: { (status) in
//                    if status == true {
//                        print("logout successfully")
//                    }
//                })
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("****************************************")
        print("****************************************")
        print("APNs device token: \(deviceTokenString)")
        print("****************************************")
        print("****************************************")
        
        
        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        print("Recived: \(userInfo)")
    }
    

    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        
        print("********************************")
        print("********************************")
        print("Recived: \(userInfo)")
        print("********************************")
        print("********************************")
 
    }
    
    func pushNotifiction(info: NSDictionary?)
    {
        if Helper.getUser() != nil{
            
            let aps = info!["aps"] as? NSDictionary
            let data = info!["gcm.notification.data"] as? String
            
            let alrtObject = aps!["alert"] as! NSDictionary
            let tileofMSg = alrtObject.object(forKey: "title") as! String
            var msgOfBody = alrtObject.object(forKey: "body") as! String
            
            msgOfBody = msgOfBody.decodeUrl()!
            msgOfBody = msgOfBody.replacingOccurrences(of: "+", with: " ", options: .literal, range: nil)
            
            var counterOfNoticin = 1
            if(Common.getValueForKey(keyValue: kNotificationCounter) == kEmptyString)
            {
                counterOfNoticin = 1
            }
            else
            {
                let countr = Int(Common.getValueForKey(keyValue: kNotificationCounter))
                counterOfNoticin = countr!
                counterOfNoticin+=1
            }
            
            appDeletForBadee.applicationIconBadgeNumber = counterOfNoticin
            setDefaultValue(keyValue: kNotificationCounter, valueIs: String(counterOfNoticin))
            
            NotificationCenter.default.post(name: Notification.Name(kPostNotifcaion), object: nil)
            
            let objectValue = data?.split(separator: ",")
            
            let vc =  ((self.window?.rootViewController as? MainVC)?.rootViewController as? UINavigationController)?.viewControllers.last
            
            if vc is ChatConversationVC{
                let chatVC = vc as! ChatConversationVC
                if chatVC.conversationId != String(objectValue![1]){
                    playSound("sound")
                    let postNotificaiton = CRNotifications()
                    postNotificaiton.data = data!
                    postNotificaiton.showNotification(type: .success, title: tileofMSg, message: msgOfBody, dismissDelay: 3,completion: { isTap in
                        if isTap{
                            self.moveToChatScreen(data: postNotificaiton.data)
                        }
                    })
                }
            }else{
                playSound("sound")
                let postNotificaiton = CRNotifications()
                postNotificaiton.data = data!
                
                
                postNotificaiton.showNotification(type: .success, title: tileofMSg, message: msgOfBody, dismissDelay: 3,completion: { isTap in
                    if isTap{
                        self.moveToChatScreen(data: postNotificaiton.data)
                    }
                })
            }
        }
    }
}

// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        setDefaultValue(keyValue: kdeviceToken, valueIs: fcmToken)
         
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        
    }
    // [END ios_10_data_message]
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    func playSound(_ soundName: String) {
        //vibrate phone first
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: soundName, ofType: "wav")!)
        
        var error: NSError?
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("audioPlayer error \(err.localizedDescription)")
            return
        } else {
            audioPlayer!.delegate = self
            audioPlayer!.prepareToPlay()
        }
        
        //negative number means loop infinity
        audioPlayer!.numberOfLoops = 0
        audioPlayer!.play()
    }
    
    
    
    func moveToChatScreen(data:String) {
     
        let objectValue = data.split(separator: ",")
        
        let chatConversationVC = UIStoryboard.chatStoryboard().instantiateViewController(withIdentifier: ChatControllers.ChatConversationVC.rawValue) as! ChatConversationVC
        
        let vc =  ((self.window?.rootViewController as? MainVC)?.rootViewController as? UINavigationController)?.viewControllers.last
        
        if vc is ChatConversationVC{
            let chatVC = vc as! ChatConversationVC
            if chatVC.conversationId != String(objectValue[1]){
                chatConversationVC.otherUserName = String(objectValue[0])
                chatConversationVC.chatConversationId = String(objectValue[1])
                chatConversationVC.curentUserid = DataHandler.sharedInstance.user?.id ?? ""
                chatConversationVC.otherUserId = String(objectValue[2])
                chatConversationVC.conversationId = String(objectValue[1])
                chatConversationVC.userPhoto = "" //objectValue.user.profilePic
                chatConversationVC.uid = String(objectValue[2])
                chatConversationVC.totalCount = Int(String(objectValue[3])) ?? 0
                chatConversationVC.isDataFound = true
                chatConversationVC.userName =  String(objectValue[0])
                vc?.navigationController?.pushViewController(chatConversationVC, animated: true)
            } 
        }else{
            chatConversationVC.otherUserName = String(objectValue[0])
            chatConversationVC.chatConversationId = String(objectValue[1])
            chatConversationVC.curentUserid = DataHandler.sharedInstance.user?.id ?? ""
            chatConversationVC.otherUserId = String(objectValue[2])
            chatConversationVC.conversationId = String(objectValue[1])
            chatConversationVC.userPhoto = "" //objectValue.user.profilePic
            chatConversationVC.uid = String(objectValue[2])
            chatConversationVC.totalCount = Int(String(objectValue[3])) ?? 0
            chatConversationVC.isDataFound = true
            chatConversationVC.userName =  String(objectValue[0])
            vc?.navigationController?.pushViewController(chatConversationVC, animated: true)
        }
        
        
    }
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.pushNotifiction(info:notification.request.content.userInfo as? NSDictionary)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let userInfo = response.notification.request.content.userInfo
        if (userInfo["custom"] as? String) != nil{
       
        }
        self.moveToChatScreen(data:  userInfo["gcm.notification.data"] as? String ?? "")
        
    }
}
extension AppDelegate : AVAudioPlayerDelegate{
    
    //AVAudioPlayerDelegate protocol
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        // player.stop()
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        
    }
}

