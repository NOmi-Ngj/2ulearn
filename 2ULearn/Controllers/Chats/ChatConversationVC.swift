//
//  ChatConversationVC.swift
//  2ULearn
//
//  Created by mac on 6/28/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import AVFoundation
//import CRNotifications
import IQKeyboardManagerSwift

class ChatConversationVC: UIViewController,ShowsAlert {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var NavigationHeader: UIView!

    @IBOutlet weak var XcrossIcon: UIImageView!
    @IBOutlet weak var conversationText: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnLanguageConversino: UIButton!
    
    
    
    var isSelectedImage = false
    var selectedIndex = 0
//     : [(textTitle:String, textDetails:String, image:UIImage)]
    var itemsArray:[(english:String,chineese:String, isMyMessage:Bool)] = [("Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.","很多西东西很多 东西东西 东西东 很多西多西东西 东西东很多 东西 东西东很多西东西 东西东很多西东西 东西东多西东西 东西东很多很多西东西 东西东很 多西东西 东西东很多 多西东西 东西东很多西东西开车",true),("how are you?","东西东",true),("message here","很多",true),("Demo text message","慢慢地 ",true),("Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.","很多西东西很多 东西东西 东西东 很多西多西东西 东西东很多 东西 东西东很多西东西 东西东很多西东西 东西东多西东西 东西东很多很多西东西 东西东很 多西东西 东西东很多 多西东西 东西东很多西东西开车",false),("how are you?","东西东",false),("message here","很多",false),("Demo text message","慢慢地",false)]
    var nextLessonPopUp = WordsRightWrongPopUp()
    
    // Chat Helper
    var curentUserid = ""
    var otherUserId = ""
    var otherUserName = ""
    var conversationId = ""
    var chatConversationId = ""
    var userName = ""
    var uid = ""
    var userPhoto = ""
    var items = [Message]()
    var totalCount = 0
    var indexPlaying = 0
    
    var isDataFound = false
    var isEditingEanble = false
    
    var deletedObjects = [String]()
    
    var arrayOfObjects = [ConversationMessagesObject]()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.subviews
//        let view = UIApplication.shared.window.
        setbtnIcon(isInitial: true)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
//        shouldShowTextFieldPlaceholder = false
//        IQKeyboardManager.sharedManager().shouldHidePreviousNext = false
        
        conversationText.delegate = self
        conversationText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//        conversationText.addTarget(self, action: "textFieldDidChange:", for: UIControlEvents.editingChanged)
        titleText.text = otherUserName
        applicationFontsSetting()
        let templateImage = XcrossIcon.image?.withRenderingMode(.alwaysTemplate)
        XcrossIcon.image = templateImage
        XcrossIcon.tintColor = UIColor.white
//        XcrossIcon.tintColor = .white
        
        
        debugPrint(self.conversationId)
        debugPrint(self.chatConversationId)
        debugPrint(isDataFound)
        
//        var headerView: UIView = UIView.init(frame: CGRectMake(1, 50, 276, 30));
//        NavigationHeader.backgroundColor = UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1.0)
        
//        var labelView: UILabel = UILabel.init(frame: CGRectMake(4, 5, 276, 24))
//        labelView.text = "hello"
        
//        headerView.addSubview(labelView)
//        let view = HeaderChat.instanceFromNib()
//        view?.frame = CGRect(x: 1 , y: 50, width: 276, height: 30)
//        self.tableView.tableHeaderView = view
        
        tableViewRegisterDelegateandDataSource()
        conversationId != "" ? fetchData():nil
        
        guard let window = UIApplication.shared.keyWindow else {
            print("Failed to show CRNotification. No keywindow available.")
            return
        }
        if let view = window.viewWithTag(1010) as? CRNotification{
            view.dismissNotification()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: ConversationTVC.NibName , bundle: nil), forCellReuseIdentifier: ConversationTVC.reuseIdentifier)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
    }
    @IBAction func onPressed_LanguageConversation(_ sender: UIButton) {
        setbtnIcon(isInitial: false)
        
    }
    func setbtnIcon(isInitial:Bool){
        
        
        let userDefaults = UserDefaults.standard
        if let isTesting = userDefaults.value(forKey: "isLanguage") as? String {
            if !isInitial{
                let language = isTesting == "Eng" ? "Chi":"Eng"
                let image = isTesting == "Chi" ? #imageLiteral(resourceName: "icon_eng"):#imageLiteral(resourceName: "icon_chi")
                btnLanguageConversino.setImage(image, for: .normal)
                userDefaults.setValue(language, forKey: "isLanguage")
                self.showAlertAndDisappear(title: "", message: "\(image == #imageLiteral(resourceName: "icon_eng") ? "English selected":"Chinese selected")") { (isTruee) in
                }
            }else{
                let image = isTesting == "Eng" ? #imageLiteral(resourceName: "icon_eng"):#imageLiteral(resourceName: "icon_chi")
                btnLanguageConversino.setImage(image, for: .normal)
            }
        }else{
            btnLanguageConversino.setImage(#imageLiteral(resourceName: "icon_eng"), for: .normal)
            userDefaults.setValue("Eng", forKey: "isLanguage")
        }
        userDefaults.synchronize()
    }
    
    @IBAction func onPress_Send(_ sender: UIButton) {
        
        
        if conversationText.text != ""{
            self.composeMessage(type: .text, content: conversationText.text ?? "", sender: sender)
        }
        
    }
    func goBack(){
        Helper.audioPlayer?.stop()
        if navigationController?.viewControllers[(navigationController?.viewControllers.count)! - 2] is UsersListVC{
            navigationController?.viewControllers.forEach({
                if $0 is TalksListVC{
                    _ = navigationController?.popToViewController($0, animated: true)
                }
            })
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
//    http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=test&tl=zh
 
    func currentTimeInMiliseconds() -> Int {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        let infalValue = Int(since1970 * 1000)
        print(infalValue)
        return infalValue
    }
    
    func composeMessage(type: MessageType, content: Any , sender:UIButton)  {
        guard conversationText.text?.trimmingCharacters(in: .whitespaces).isEmpty == false else {
            return
        }
        btnSend.isUserInteractionEnabled = false
        btnSend.isEnabled = false
        let language = conversationText.textInputMode?.primaryLanguage
        if language == nil {
            return
        }
        let encodedContent = (content as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if (language?.contains("dictation"))!{
        ApiHelper.getRequest(nil, kPath: "https://translation.googleapis.com/language/translate/v2/detect/?q=\(encodedContent ?? "")&key=AIzaSyC-WwF_dgNRipH_Kk76_t9oMjjVwoIjJ1s", completion: { response,error in
            debugPrint(response)
            let language = ((((response as? NSDictionary)?.value(forKey: "detections") as! NSArray)[0] as! NSArray)[0] as! NSDictionary).value(forKey: "language") as! String
            self.sendMessageWithContent(language: language, type: type, content: content)
        })
        }else{
            self.sendMessageWithContent(language: language, type: type, content: content)
        }
    }
    func sendMessageWithContent(language:String?, type: MessageType, content: Any){
        let encodedContent = (content as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if (language?.contains("en"))!{
            
            ApiHelper.getRequest(nil, kPath: "https://translation.googleapis.com/language/translate/v2/?q=\(encodedContent ?? "")&source=en&target=zh&key=AIzaSyC-WwF_dgNRipH_Kk76_t9oMjjVwoIjJ1s", completion: { response,error in
                
                let translatedText = ((response as? NSDictionary)?.value(forKeyPath: "translations.translatedText") as? [String])?.first ?? ""
                let message =  Message.init(type: type, content: content, contentChi: translatedText, owner: .sender, timestamp: Int(self.currentTimeInMiliseconds()), isRead: false, isBlocked: false, toID: false, fromID: false, chatId:  self.conversationId, timeAgo: "\(Int(self.currentTimeInMiliseconds()))", uid: self.uid, totalCount: 0, userid: self.curentUserid, lastConversationId: self.conversationId)
                
                Message.send(isDataFound: self.isDataFound, message: message, toID: self.otherUserId, currentUserID: self.curentUserid, conversationId: self.conversationId ,isBlocked: false, udi: self.uid, chatConversationId: self.chatConversationId ,completion: {(count, chatID, profileImg, userImage) in
                    self.conversationText.text = ""
                    self.btnSend.isUserInteractionEnabled = true
                    self.btnSend.isEnabled = true
                    if message.type == .text{
                        ChatUser.infoUserIdOnline(forUserID: self.otherUserId, completion: { (isLogin, UserUdid)  in
                            if isLogin == 1 ||  isLogin == 2{
                            let dataObject = ""
                                self.sendPushs(fcmTOken: UserUdid, message: "\(Helper.getUser()?.name ?? "") : \(message.content as! String)",messagObject:message,isLogin:isLogin)
                            }
                        })
                        
                    }
                    
                    if(chatID != "-1")
                    {
                        self.conversationId = chatID
                        self.chatConversationId = chatID
                        self.isDataFound = true
                        self.fetchData()
                    }
                    self.totalCount = count
                })
            })
        }else if (language?.contains("zh"))!{
            
            ApiHelper.getRequest(nil, kPath: "https://translation.googleapis.com/language/translate/v2/?q=\(encodedContent ?? "")&source=zh&target=en&key=AIzaSyC-WwF_dgNRipH_Kk76_t9oMjjVwoIjJ1s", completion: {response,error in
                
                let translatedText = ((response as? NSDictionary)?.value(forKeyPath: "translations.translatedText") as? [String])?.first ?? ""
                
                let message = Message.init(type: type, content: translatedText, contentChi: content , owner: .sender, timestamp: Int(self.currentTimeInMiliseconds()), isRead: false, isBlocked: false, toID: false, fromID: false, chatId:  self.conversationId, timeAgo: "\(Int(self.currentTimeInMiliseconds()))", uid: self.uid, totalCount: 0, userid: self.curentUserid, lastConversationId: self.conversationId)
                
                Message.send(isDataFound: self.isDataFound, message: message, toID: self.otherUserId, currentUserID: self.curentUserid, conversationId: self.conversationId ,isBlocked: false, udi: self.uid, chatConversationId: self.chatConversationId ,completion: {(count, chatID, profileImg, userImage) in
                    self.conversationText.text = ""
                    self.btnSend.isUserInteractionEnabled = true
                    self.btnSend.isEnabled = true
                    if message.type == .text{
                        ChatUser.infoUserIdOnline(forUserID: self.otherUserId, completion: { (isLogin, UserUdid) in
                            if isLogin == 1 ||  isLogin == 2 {
                                
                                self.sendPushs(fcmTOken: UserUdid, message: "\(Helper.getUser()?.name ?? "") : \(message.content as! String)",messagObject:message,isLogin:isLogin)
                            }
                        })
                        
                    }
                    
                    if(chatID != "-1")
                    {
                        self.conversationId = chatID
                        self.chatConversationId = chatID
                        self.isDataFound = true
                        self.fetchData()
                    }
                    self.totalCount = count
                })
            })
        }else{
            btnSend.isUserInteractionEnabled = true
            self.btnSend.isEnabled = true
            self.showCustomAlertView(messageText: "Only English and Chinese language supported.")
        }

    }
    
    func sendPushs(fcmTOken: String, message: String,messagObject:Message,isLogin:Int)
    {
        let params = ["notification-type":"message", "data":"\(Helper.getUser()?.name ?? ""),\(conversationId),\(Helper.getUser()?.id ?? ""),\(totalCount)", "title": "Message", "text": message, "sound": "default", "badge": 0] as [String : AnyObject]
        
        let dataParams = ["conversationId" : self.conversationId,"mMessage":messagObject.content as? String ?? "","mMessage_ch": messagObject.content_ch as? String ?? "", "time":Int(self.currentTimeInMiliseconds()),"toName":self.otherUserName,"fromName":Helper.getUser()?.name ?? "","fromUid":self.curentUserid,"fromImages":"","toUid":self.otherUserId,"toImages":"","isBlock":false,"fromId":self.curentUserid,"toId":self.otherUserId,"searchName":"","lastMsgCreatorId":self.curentUserid,"totalMessageCount":0,"isRead":false,"lastConversationId":self.conversationId,self.otherUserId:false , self.curentUserid:false] as [String : AnyObject]
        let convesationObject = ["conversation":dataParams] as [String : AnyObject]
        ApiHelper.sendPush(fcmToken: fcmTOken, params: params,converstaionObject: convesationObject,isLogin:isLogin) { (response, error) in
            
        }
    }
    
    //Downloads messages
    func fetchData() {

        // Show loading on api call
//        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        Message.downloadAllMessages(forUserID: self.otherUserId, currentUserId: self.curentUserid, conversionId: self.conversationId, completion: {[weak weakSelf = self] (message) in
            
            weakSelf?.items.removeAll()
            
            weakSelf?.items = message
            DispatchQueue.main.async {
                self.hideLoaderView()
            }
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            DispatchQueue.main.async
                {
                    self.hideLoaderView()
                    if let state = weakSelf?.items.isEmpty, state == false
                    {
                        self.hideLoaderView()
                        weakSelf?.tableView?.reloadData()
                        weakSelf?.tableView?.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    }
            }
        })
        // Message.markMessagesRead(forUserID: self.otherUserId, currentUserID: self.curentUserid)
//        Message.markMessagesRead(forUserID: self.otherUserId, currentUserID: self.curentUserid, childId: self.chatConversationId)
    }
    
    @objc func playConversationText(_ sender: UIButton){
        
        let indexPath = IndexPath(row: indexPlaying, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as? ConversationTVC
        
        let index = sender.tag
        indexPlaying = index
        let message = items[index]
        if Helper.audioPlayer?.isPlaying ?? false {
            if cell?.imgOtherUserSoundPlayButton.image == #imageLiteral(resourceName: "icon_pause")   && index == indexPlaying {
                DispatchQueue.main.async {
                    cell?.imgOtherUserSoundPlayButton.image = #imageLiteral(resourceName: "icon_play")
                }
            }
            if cell?.imgUserSoundPlayButton.image == #imageLiteral(resourceName: "icon_pause")  && index == indexPlaying{
                DispatchQueue.main.async {
                    cell?.imgUserSoundPlayButton.image = #imageLiteral(resourceName: "icon_play")
                }
            }
            Helper.audioPlayer?.stop()
            return
        }
        cell?.imgUserSoundPlayButton.image = #imageLiteral(resourceName: "icon_play")
        cell?.imgOtherUserSoundPlayButton.image = #imageLiteral(resourceName: "icon_play")
        

//        let language = conversationText.textInputMode?.primaryLanguage
        isPlayingSound(isPlaying: true, Index: indexPlaying)
        let userDefaults = UserDefaults.standard
        if let isTesting = userDefaults.value(forKey: "isLanguage") as? String {
            if isTesting == "Eng"{
                Helper.playFileFromURL(url: "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=\( message.content as? String  ?? "")&tl=en", selfdelegate: self)
            }else{
                Helper.playFileFromURL(url: "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=\( message.content_ch  as? String ?? "")&tl=zh", selfdelegate: self)
            }
        }else{
            Helper.playFileFromURL(url: "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=\( message.content as? String  ?? "")&tl=en", selfdelegate: self)
        }
        
//        if (language?.contains("en"))!{
//
//        }else{
//
//        }
    }
    
    func isPlayingSound(isPlaying:Bool,Index:Int){
        
        let indexPath = IndexPath(row: Index, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as? ConversationTVC
        cell?.imgUserSoundPlayButton.image = isPlaying == true ? #imageLiteral(resourceName: "icon_pause"):#imageLiteral(resourceName: "icon_play")
        cell?.imgOtherUserSoundPlayButton.image = isPlaying == true ? #imageLiteral(resourceName: "icon_pause"):#imageLiteral(resourceName: "icon_play")
        
    }
    
    deinit {
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = true
        
        debugPrint("chat deinit")
    }
}


//MARK:- tableView Datasource and Delegate methods
extension ChatConversationVC : UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConversationTVC.reuseIdentifier, for: indexPath) as! ConversationTVC
        cell.selectionStyle = .none
        let english = items[indexPath.row].content as? String
        let chinese = items[indexPath.row].content_ch as? String
        let timestamp = items[indexPath.row].timestamp
        
        let isMyUser = items[indexPath.row].userid == curentUserid ? true:false
        if isMyUser == true{
            cell.userSoundPlayButton.tag = indexPath.row
        }else{
            cell.otherUserSoundPlayButton.tag = indexPath.row
        }
        cell.userSoundPlayButton.addTarget(self, action: #selector(playConversationText), for: .touchUpInside)
        cell.otherUserSoundPlayButton.addTarget(self, action: #selector(playConversationText), for: .touchUpInside)
//        let isMy =  items[indexPath.row].owner.hashValue
//        cell.setData(english: english ?? "", mandarin: chinese ?? "",isMyMessage: items[indexPath.row].userid
// == curentUserid ? true:false )
        cell.setData(english: english ?? "", mandarin: chinese ?? "",isMyMessage: items[indexPath.row].userid
         == curentUserid ? true:false, timeStamp: timestamp)
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//    }
}

//MARK:- Audio Player And Recording delegates methods
extension ChatConversationVC : AVAudioPlayerDelegate{
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("Audio Play Finish playing")
        isPlayingSound(isPlaying: false, Index: indexPlaying)
//        audioPlayer?.stop()
//        musicPlayerPopup?.dismiss(true)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error")
        isPlayingSound(isPlaying: false, Index: indexPlaying)
    }
  
    
}
extension ChatConversationVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let language = textField.textInputMode?.primaryLanguage
        if (language == nil){
            return false
        }
        
        if (textField.text?.count ?? 0) > 160 {
            self.showAlertAndDisappear(title: "", message: "characters limit exceed") { (isDisappeared) in
                
            }
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        btnSend.isUserInteractionEnabled = true
        self.btnSend.isEnabled = true
        
//            conversationText.text = ""
//            textField.resignFirstResponder()
//            self.showCustomAlertView(messageText: "English and Chinese language should be typed only, emoji's is not supported")
//        
//            conversationText.text = textField.text
//        }
    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        let language = textField.textInputMode?.primaryLanguage
//        if (language?.contains("dictation"))!{
//
//        }
//        if ((language?.contains("en"))! || (language?.contains("zh"))!){
//            return true
//        }
//
//        return false
//    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let language = textField.textInputMode?.primaryLanguage
//
//        if ((language?.contains("en"))! || (language?.contains("zh"))!){
//            return true
//        }
//        if (language?.contains("dictation"))!{
//            return false
//        }
//        return false
//    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension ChatConversationVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
//    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
//        let text = "Chat Not Found."
//        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
//        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
//        return attributedString
//    }
//    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
//        return UIImage(named: "Placeholders_messages")
//    }
}
