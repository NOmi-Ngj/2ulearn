//
//  UsersListVC.swift
//  2ULearn
//
//  Created by Nouman Gul Junejo on 7/23/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class UsersListVC: UIViewController , ShowsAlert{
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var chatUser = [ChatUser]()
    
    var items = [MessagesObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        tableViewRegisterDelegateandDataSource()
        
        self.showLoaderView(isCenter: true, UserInterfaceEnable: true)
        let usrId = DataHandler.sharedInstance.user?.id ?? ""
        
        ChatUser.downloadAllUsers(exceptID: usrId) {[weak self] (users) in
            if users != nil && self?.chatUser != nil{
                
                if !(self?.chatUser.contains(users!))!{
                self?.chatUser.append(users!)
                }
                
                DispatchQueue.main.async {
                    self?.hideLoaderView()
                    self?.tableView.reloadData()
                }
            }
            
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.showLoaderView(isCenter: true, UserInterfaceEnable: true)
//        
//        let usrId = DataHandler.sharedInstance.user?.id ?? ""
//        if Common.isTesting() {
//            usrId = "asdfasdfa"
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: TalkListTVC.NibName , bundle: nil), forCellReuseIdentifier: TalkListTVC.reuseIdentifier)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    }
    
}
extension UsersListVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatUser.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TalkListTVC.reuseIdentifier, for: indexPath) as! TalkListTVC
        cell.selectionStyle = .none
        cell.itemTitle.text = chatUser[indexPath.row].name
        cell.itemDetails.text = chatUser[indexPath.row].email
        cell.imgUserProfile.yy_setImage(with: URL(string: chatUser[indexPath.row].userImgURL), placeholder:#imageLiteral(resourceName: "graybg"))
        DispatchQueue.main.async {
            cell.imgUserProfile.cornerRadiuss = cell.imgUserProfile.frame.size.height/2
        }
        let title = chatUser[indexPath.row].name
        let index2 = title.index(title.startIndex, offsetBy: 0) //will call succ 2 times
        let lastChar: Character = title[index2] //now we can index!
        let string = "\(lastChar)"
        cell.itemTitleImagePlaceHolder.text = string.capitalized
        //cell.imgTrophies.image = indexPath.row < 3 ? tropyImages[indexPath.row]:tropyImages[3]
        //cell.imgSelection.image = indexPath.row == 0 ? #imageLiteral(resourceName: "cellBoxSelected"):#imageLiteral(resourceName: "cellBox")
        //cell.itemTitle.textColor = indexPath.row == 0 ? .white:.black
        //let points = "\(leaderBoard[indexPath.row].total_score ?? 0) Points"
        //cell.setData(title: leaderBoard[indexPath.row].name ?? "", imageURL: leaderBoard[indexPath.row].image_url?.scaledImageURL() ?? "", detailText: points)
        //cell.imgTrophies.isHidden = false
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatConversationVC = UIStoryboard.chatStoryboard().instantiateViewController(withIdentifier: ChatControllers.ChatConversationVC.rawValue) as! ChatConversationVC
        _ = ChatUser()
//        objectValue = self.chatUser[indexPath.row]
//        let chatId = objectValue.conversation[0].conversationId
//        chatConversationVC.isDataFound = objectValue.isFound
//        if objectValue.conversationId == "" {
//        }
//        MessagesObject.checkIfConversationFound(currentUserId: DataHandler.sharedInstance.user?.id ?? "") { (isFound) in
//            if  isFound {
//
//            }
//        }
//        chatConversationVC.chatConversationId = chatId
//        chatConversationVC.conversationId = objectValue.conversationId
//        chatConversationVC.otherUserId = objectValue.id
//        chatConversationVC.uid = objectValue.uid
//        chatConversationVC.curentUserid = DataHandler.sharedInstance.user?.id ?? ""
        
        var objectValue = MessagesObject()

        
        _ = chatUser[indexPath.row].conversation.forEach({element in
            if element.user.id == self.chatUser[indexPath.row].id{
                objectValue = element
            }
        })
        
        if objectValue.conversationId == ""{
             chatConversationVC.isDataFound = false
//            chatConversationVC.userName = objectValue.user.name
            
        }else{
                chatConversationVC.isDataFound = self.chatUser[indexPath.row].isFound
//                chatConversationVC.userName = self.chatUser[indexPath.row].name
        }
        let otherUserId = objectValue.user.id != "" ? objectValue.user.id:self.chatUser[indexPath.row].id
        chatConversationVC.otherUserName = self.chatUser[indexPath.row].name
        chatConversationVC.chatConversationId = objectValue.conversationId
        chatConversationVC.curentUserid = DataHandler.sharedInstance.user?.id ?? ""
        chatConversationVC.otherUserId = otherUserId
        chatConversationVC.conversationId = objectValue.user.conversationId
        chatConversationVC.userPhoto = objectValue.user.userImgURL //objectValue.user.profilePic
        chatConversationVC.uid = self.chatUser[indexPath.row].uid
        chatConversationVC.totalCount = objectValue.totalCount
        
        
        self.navigationController?.pushViewController(chatConversationVC, animated: true)
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension UsersListVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
//    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
//        let text = "Users Not Found."
//        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
//        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
//        return attributedString
//    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_user")
    }
}
