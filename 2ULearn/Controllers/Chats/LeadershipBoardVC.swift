//
//  LeadershipBoardVC.swift
//  2ULearn
//
//  Created by mac on 6/27/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet
class LeadershipBoardVC: UIViewController ,ShowsAlert{
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var dataTupleSet : [(textTitle:String, textDetails:String, image:UIImage)] = [("Jhony","7890 Points",#imageLiteral(resourceName: "rpic1")),("Deo","7520 Points",#imageLiteral(resourceName: "rpic2")),("Grey","7450 Points",#imageLiteral(resourceName: "rpic3")),("Jhony","7390 Points",#imageLiteral(resourceName: "rpic1")),("Deo","7890 Points",#imageLiteral(resourceName: "rpic2")),("Grey","7890 Points",#imageLiteral(resourceName: "rpic3")),("Jhony","7890 Points",#imageLiteral(resourceName: "rpic1")),("Deo","7890 Points",#imageLiteral(resourceName: "rpic2")),("Grey","7890 Points",#imageLiteral(resourceName: "rpic3"))]
    var tropyImages:[UIImage] = [#imageLiteral(resourceName: "tropy_1"),#imageLiteral(resourceName: "tropy_2"),#imageLiteral(resourceName: "tropy_3"),#imageLiteral(resourceName: "tropy_4")]
    
    var leaderBoard = [LeaderBoard]()
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        tableViewRegisterDelegateandDataSource()
        loadLeadershipListApi()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: TalkListTVC.NibName , bundle: nil), forCellReuseIdentifier: TalkListTVC.reuseIdentifier)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    }
    
    func loadLeadershipListApi(){
        // Parametters for login api
        let params = [ : ] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.topScorers.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            // handle response
            if  response != nil {
                debugPrint(response!)
                self?.leaderBoard = Mapper<LeaderBoard>().mapArray(JSONArray: response![ResponseKey.data.rawValue] as! [[String : AnyObject]]) ?? []
                self?.tableView.reloadData()
            }
        }
    }
}

extension LeadershipBoardVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaderBoard.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TalkListTVC.reuseIdentifier, for: indexPath) as! TalkListTVC
        cell.selectionStyle = .none
        cell.imgTrophies.image = indexPath.row < 3 ? tropyImages[indexPath.row]:tropyImages[3]
        cell.imgSelection.image = indexPath.row == 0 ? #imageLiteral(resourceName: "cellBoxSelected"):#imageLiteral(resourceName: "cellBox")
        cell.itemTitle.textColor = indexPath.row == 0 ? .white:.black
        let points = "\(leaderBoard[indexPath.row].total_score ?? "0") Points"
        cell.setData(title: leaderBoard[indexPath.row].name ?? "", imageURL: leaderBoard[indexPath.row].image_url?.scaledImageURL() ?? "", detailText: points)
        cell.imgTrophies.isHidden = false
        return cell
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension LeadershipBoardVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
