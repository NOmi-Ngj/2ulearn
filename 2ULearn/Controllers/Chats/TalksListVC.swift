//
//  TalksListVC.swift
//  2ULearn
//
//  Created by mac on 6/27/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import NVActivityIndicatorView
import SystemConfiguration

class TalksListVC: UIViewController, ShowsAlert {
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    var validationAlert : ValidationAlertView?
    @IBOutlet weak var tableView: UITableView!
   // var dataTupleSet : [(textTitle:String, textDetails:String, image:UIImage)] = [("Jhony","9:00am",#imageLiteral(resourceName: "rpic1")),("Deo","11:00am",#imageLiteral(resourceName: "rpic2")),("Grey","1:00pm",#imageLiteral(resourceName: "rpic3")),("Jhony","9:00am",#imageLiteral(resourceName: "rpic1")),("Deo","11:00am",#imageLiteral(resourceName: "rpic2")),("Grey","1:00pm",#imageLiteral(resourceName: "rpic3")),("Jhony","9:00am",#imageLiteral(resourceName: "rpic1")),("Deo","11:00am",#imageLiteral(resourceName: "rpic2")),("Grey","1:00pm",#imageLiteral(resourceName: "rpic3"))]
    
    var createConfirmationPopup : ConfirmationAlertView?
    var isFilter = false
    var arrayOfObjects = [MessagesObject]()
    let messageCell = "MessagesTableViewCell"
    var items = [MessagesObject]()
    var filterObjectsArray = [MessagesObject]()
    public var isloadFromNotication = false
    //Refresh Controller
    var refresher:UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refreshStream), for: UIControlEvents.valueChanged)
        refresher = refresh
        tableView!.addSubview(refresher)
       
        applicationFontsSetting()
        tableViewRegisterDelegateandDataSource()
    }
    @objc func refreshStream() {
        fetchData()
        refresher?.endRefreshing()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         fetchData()
    }
    
    //Downloads conversations
    func fetchData() {
        
//        self.showLoaderView(isCenter: false, UserInterfaceEnable: true)
        self.items = []
//        self.tableView.reloadData()
        let  usrId = DataHandler.sharedInstance.user?.id ?? ""
        MessagesObject.showConversations(currentUserId: usrId) { [weak self]( conversations, isFoundData) in
            
            DispatchQueue.main.async {
                self?.hideLoaderView()
            }
            if(isFoundData == false)
            {
                self?.tableView?.reloadData()
                return
            }
            
            self?.items = conversations
            self?.items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            DispatchQueue.main.async {
                
                if ((self?.isViewLoaded != nil) && (self?.view.window != nil)) {
                    // viewController is visible

                    self?.tableView?.reloadData()
                }
                
                var badgeCount = 0
                for conversation in self?.items ?? [] {
                    if conversation.lastMessage.isRead == false && conversation.lastMessage.owner == .sender && conversation.lastMessage.isBlocked == false{
                        badgeCount += 1
                    }
                }
                
                if self?.isloadFromNotication ?? false{
                    if self?.items.count ?? 0 > 0 {
                        self?.items.forEach({ (user) in
                           
                        })
                    }
                }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onPressSelectUser(){
        let usersListVC = UIStoryboard.chatStoryboard().instantiateViewController(withIdentifier: ChatControllers.UsersListVC.rawValue) as! UsersListVC
        usersListVC.items = items
        self.navigationController?.pushViewController(usersListVC, animated: true)
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: TalkListTVC.NibName , bundle: nil), forCellReuseIdentifier: TalkListTVC.reuseIdentifier)
    } 
}


extension TalksListVC : customConfirmationAlertViewDelegate {
    func okButtonPressed(index: Int) {
        
        if connectedToNetwork(){
         
            let delteObject = self.items[index]
            let usrId = Helper.getUser()?.id ?? ""
            var isDeletedChat = false
//            self.showLoaderView(isCenter: true, UserInterfaceEnable: false)
            Message.deleteParticularUserChat(forUserID: usrId, OtherUserId: delteObject.user.id ?? "", conversationId: delteObject.conversationId, chatKey: delteObject.lastConversationId, completion: { (num) in
//                self.hideLoaderView()
                if isDeletedChat == false {
//                    if index >= self.items.count {
                        self.items.remove(at: index)
//                    }
                    self.tableView?.reloadData()
                }
                isDeletedChat = true
                self.createConfirmationPopup?.dismiss(true)
            })
        }else{
            self.createConfirmationPopup?.dismiss(true)

            self.showAlertAndDisappear(title: "", message: "The Internet connection appears to be offline") { (isDeleted) in
                
            }
//            validationAlert = ValidationAlertView.instanceFromNib()
//            validationAlert?.animator = FromTopTranslationAnimator()
//            validationAlert?.messageLbl.text = "Internet Connect Required"
//            validationAlert?.alertTitleLbl.text = "Alert"
//            validationAlert?.OkButton.setTitle("OK", for: .normal)
//            validationAlert?.show()
//            validationAlert?.delegate = self
//            self.validationAlert?.delegate = self as! customAlertViewDelegate
            
        }
        
    }
    
    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}
extension TalkListTVC : customAlertViewDelegate{
    func okButtonPressed() {
        
    }
}
extension TalksListVC : UITableViewDelegate,UITableViewDataSource{
    
//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
//        if tableView.isEditing {
//            return .delete
//        }
//        
//        return .none
//    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            
            createConfirmationPopup = ConfirmationAlertView.instanceFromNib()
            createConfirmationPopup?.YesBtn.tag = indexPath.row
            createConfirmationPopup?.messageLbl.text = "Do you want to delete chat?"
            createConfirmationPopup?.animator = FromTopTranslationAnimator()
            createConfirmationPopup?.delegate = self
            createConfirmationPopup?.show()
           
            
//            deleteChatForUser
//            Message.deleteChatForUser(fromUserid: usrId, conversationID: delteObject.conversationId, completion: { (inata) in
//                self.items.remove(at: indexPath.row)
//
//                self.tableView?.reloadData()
//            })
            
//deltePartiuclarChat
//            Message.deleteChat(toID: usrId, forLocation: delteObject.conversationId, completion: {
//                self.items.remove(at: indexPath.row)
//                self.tableView?.reloadData()
//            })
            
            
//            Message.deleteAllChat(conversationId: delteObject.conversationId, curentUserId: usrId, fromUserid: delteObject.user.id, completion: { (susses) in
//                self.items.remove(at: indexPath.row)
//                self.tableView?.reloadData()
//            })
            
//            Message.deleteAllChat(conversationId: delteObject.conversationId, curentUserId: usrId, fromUserid: usrId, completion: { (susses) in
//                self.items.remove(at: indexPath.row)
//                self.tableView?.reloadData()
//            })
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TalkListTVC.reuseIdentifier, for: indexPath) as! TalkListTVC
        cell.selectionStyle = .none
        cell.itemTitle.text = items[indexPath.row].user.name
        let userDefaults = UserDefaults.standard
        if let isTesting = userDefaults.value(forKey: "isLanguage") as? String {
        
//            if isTesting == "Chi"{
//
//                cell.itemDetails.text = (items[indexPath.row].lastMessage.content_ch as? String)?.decodeUrl()
//            }else{
//                cell.itemDetails.text = (items[indexPath.row].lastMessage.content as? String)?.decodeUrl()
//            }
            
            if isTesting == "Chi"{
                
                
                var userChatChine = (items[indexPath.row].lastMessage.content_ch as? String)?.decodeUrl()
                userChatChine = userChatChine?.replacingOccurrences(of: "+", with: " ", options: .literal, range: nil)
                cell.itemDetails.text = userChatChine
            }else{
                
                var userChat = (items[indexPath.row].lastMessage.content as? String)?.decodeUrl()
                userChat = userChat?.replacingOccurrences(of: "+", with: " ", options: .literal, range: nil)
                
                cell.itemDetails.text = userChat
            }
            
            
        }else{
            cell.itemDetails.text = (items[indexPath.row].lastMessage.content as? String)?.decodeUrl()
        }
        
        
        cell.imgUserProfile.yy_setImage(with: URL(string: items[indexPath.row].user.userImgURL), placeholder:#imageLiteral(resourceName: "graybg"))
        
        DispatchQueue.main.async {
            cell.imgUserProfile.cornerRadiuss = cell.imgUserProfile.frame.size.height/2
        }
        let title = items[indexPath.row].user.name ?? ""
        let index2 = title.index(title.startIndex, offsetBy: 0) //will call succ 2 times
        let lastChar: Character = title[index2] //now we can index!
        let string = "\(lastChar)"
        cell.itemTitleImagePlaceHolder.text = string.capitalized
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.items.count > indexPath.row {
        let chatConversationVC = UIStoryboard.chatStoryboard().instantiateViewController(withIdentifier: ChatControllers.ChatConversationVC.rawValue) as! ChatConversationVC
        var objectValue = MessagesObject()
//        if(self.isFilter == true)
//        {
//            objectValue = self.filterObjectsArray[indexPath.row]
//        }
//        else
//        {
            objectValue = self.items[indexPath.row]
//        }
        //let objectValue = self.items[indexPath.row]
        
        chatConversationVC.chatConversationId = objectValue.conversationId
        chatConversationVC.curentUserid = DataHandler.sharedInstance.user?.id ?? "" 
        chatConversationVC.otherUserId = objectValue.user.id
        chatConversationVC.otherUserName = objectValue.user.name
        chatConversationVC.conversationId = objectValue.user.conversationId
        chatConversationVC.userPhoto = objectValue.user.userImgURL //objectValue.user.profilePic
        chatConversationVC.uid = objectValue.user.uid
        chatConversationVC.totalCount = objectValue.totalCount
        chatConversationVC.isDataFound = true
        chatConversationVC.userName = objectValue.user.name
        self.navigationController?.pushViewController(chatConversationVC, animated: true)
        }
    }
}

//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension TalksListVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
//    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
//        let text = "Data Not Found."
//        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
//        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
//        return attributedString
//    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_messages")
    }
}
