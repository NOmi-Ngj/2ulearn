//
//  HomeVC.swift
//  2ULearn
//
//  Created by mac on 6/13/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class HomeVC: UIViewController, ShowsAlert {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var MandarinLanguage: UILabel!
    
    @IBOutlet weak var userPoints: UILabel!
    
    @IBOutlet weak var logoBird: UIImageView!
    //MARK:- Helper Variables
    var lessonsArr = [LessonsModel]()
    
    
    
    //MARK:- Bird Animation Constraints
    @IBOutlet weak var baCenterVerticalConstraints: NSLayoutConstraint!
    @IBOutlet weak var baCenterHorizontal: NSLayoutConstraint!
    
    @IBOutlet weak var baNewTextLeftConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var baOldTextLeftConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var baOldCenterVerticalConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var baOldCenterHorizontalConstraints: NSLayoutConstraint!
    
    
    func showBirdAnimation()  {
 
        self.view.removeConstraint(self.baOldTextLeftConstraints)
        self.view.removeConstraint(self.baOldCenterVerticalConstraints)
        self.view.removeConstraint(self.baOldCenterHorizontalConstraints)
        
        
        self.view.addConstraint(self.baCenterVerticalConstraints)
        self.view.addConstraint(self.baNewTextLeftConstraints)
        self.view.addConstraint(self.baCenterHorizontal)
    
    }
    
    func hideBirdAnimation()  {
        
        self.view.removeConstraint(self.baCenterVerticalConstraints)
        self.view.removeConstraint(self.baNewTextLeftConstraints)
        self.view.removeConstraint(self.baCenterHorizontal)
        
        
        self.view.addConstraint(self.baOldTextLeftConstraints)
        self.view.addConstraint(self.baOldCenterVerticalConstraints)
        self.view.addConstraint(self.baOldCenterHorizontalConstraints)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationLocalizationStringSetup()
        collectionviewRegisterDelegateandDataSource()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            //self.hideBirdAnimation()
           // self.showBirdAnimation()
           // self.view.setNeedsLayout()
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DataHandler.sharedInstance.getPoints { (points) in
            self.userPoints.text = "\(points)"
        }
        
        loadHomeApi()
        
    }
    
    override func viewDidLayoutSubviews() {
       // showBirdAnimation()
        
       // self.view.setNeedsLayout()
        
        
       
    }
    
    func applicationLocalizationStringSetup(){
        MandarinLanguage.text = NSLocalizedString("2ULearn Language", comment: "")
        let string = Helper.attributedStringWithLineSpacing(string: MandarinLanguage.text ?? "", lineSpacing: 5, stralign: "left")
        MandarinLanguage.attributedText = string
        MandarinLanguage.font = UIFont.appBoldFonts(size: 30)
    }
    func collectionviewRegisterDelegateandDataSource(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self
        collectionView.register(UINib(nibName: HomeFeedCVC.NibName, bundle: nil), forCellWithReuseIdentifier: HomeFeedCVC.reuseIdentifier)
    }

    func loadHomeApi(){
        // Parametters for login api
        let params =   [//            "type":"home"
            :
            ] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.home.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            DispatchQueue.main.async {
                self?.hideLoaderView()
            }
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            if  response != nil {
                debugPrint(response!)
                self?.lessonsArr = Mapper<LessonsModel>().mapArray(JSONArray: response![ResponseKey.data.rawValue] as? [[String : AnyObject]] ?? [])
                self?.collectionView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.lessonsArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeFeedCVC.reuseIdentifier, for: indexPath) as! HomeFeedCVC
        cell.setImage(viewImageURL: (lessonsArr[indexPath.row].image_url?.scaledImageURL())!,title: lessonsArr[indexPath.row].name ?? "",Details: lessonsArr[indexPath.row].descriptions ?? "")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonVC.rawValue) as! LessonVC
        vc.catLessontitle = lessonsArr[indexPath.row].name ?? ""
        vc.catLessonDetails = lessonsArr[indexPath.row].descriptions ?? ""
        vc.catID = lessonsArr[indexPath.row].id ?? ""
        vc.lessonsArr = lessonsArr[indexPath.row].lessons ?? []
        if Common.isTesting() == true{
//        vc.catID = "BvOGyAG8WY"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
extension HomeVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            let width = (collectionView.frame.size.width)/2-0.5
            let height = (collectionView.frame.size.height)/3-0.3
            return CGSize(width:  width , height: height)
    
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        //top, left, bottom, right
//        return UIEdgeInsets(top:1, left:1, bottom:1, right:1)
//    }
  
}

//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension HomeVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
