//
//  LeftMenuVC.swift
//  2ULearn
//
//  Created by Nouman Gul Junejo on 6/13/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit
import LGSideMenuController

class LeftMenuVC: UIViewController, ShowsAlert {
 
    @IBOutlet weak var tableView: UITableView!
    var createConfirmationPopup : ConfirmationAlertView?
    //MARK:- Helper Variables
    static let itemHome = "Home"
    static let itemResults = "Results"
    static let itemGoals = "Goals"
    static let itemAbout = "About"
    static let itemTextbooks = "Textbooks"
    static let itemAudio = "Audio"
    static let itemVideo = "Video"
    static let itemEbook = "Ebook"
    static let itemFav = "Favorites"
    static let itemTalks = "Talks"
    static let itemPurchased = "Purchased"
    static let itemHelp = "Help"
    static let itemLogout = "Logout"
    let menuItems:[String] = [itemHome,itemResults,itemGoals,itemAbout,itemTextbooks,itemAudio,itemVideo,itemEbook,itemFav, itemTalks,itemPurchased,itemHelp,itemLogout]
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
       
 
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: LeftMenuTVC.NibName , bundle: nil), forCellReuseIdentifier: LeftMenuTVC.reuseIdentifier)
    }
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-  Deinit
    deinit {
        debugPrint("LeftMenu Deinit Called")
    }
}
//MARK:- tableView Datasource and Delegate methods
extension LeftMenuVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LeftMenuTVC.reuseIdentifier, for: indexPath) as! LeftMenuTVC
        cell.selectionStyle = .none
        cell.setData(text: menuItems[indexPath.row].capitalized)
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let naviController = getNavigationController()
        
        Helper.audioPlayer?.stop()
        
        var hideMenu = false
        if menuItems[indexPath.row] == LeftMenuVC.itemHome {
            moveToHome(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemResults {
            moveToResultVC(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemGoals {
            moveToGoals(navController: naviController)
            hideMenu = true
        } 
        if menuItems[indexPath.row] == LeftMenuVC.itemAbout {
            moveToAboutUs(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemTextbooks {
            moveToTextBookVC(navController: naviController)
            hideMenu = true
        }
        
        if menuItems[indexPath.row] == LeftMenuVC.itemAudio{
            moveToAudioVC(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemVideo{
            moveToVideoVC(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemEbook{
            moveToEbookVC(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemFav{
            moveToFavVC(navController: naviController)
            hideMenu = true
        }
        
        
        
        if menuItems[indexPath.row] == LeftMenuVC.itemTalks {
            moveToTalks(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemPurchased {
            moveToPurchasedVC(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemHelp {
            moveToHelp(navController: naviController)
            hideMenu = true
        }
        if menuItems[indexPath.row] == LeftMenuVC.itemLogout {
            
            createConfirmationPopup = ConfirmationAlertView.instanceFromNib()
            createConfirmationPopup?.YesBtn.tag = 1
            createConfirmationPopup?.messageLbl.text = "Do you want to logout?"
            createConfirmationPopup?.animator = FromTopTranslationAnimator()
            createConfirmationPopup?.delegate = self
            createConfirmationPopup?.show()
            
        }
        
        if hideMenu{
            sideMenuController?.hideLeftView(animated: true, completionHandler: {
            })
        }
        
    }
    
    
    func moveToAboutUs(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is AboutUsVC})
        guard vc.count != 0 else {
            let aboutUsVC = UIStoryboard.extraStoryboard().instantiateViewController(withIdentifier: ExtraControllers.AboutUsVC.rawValue) as! AboutUsVC
            navController.pushViewController(aboutUsVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    func moveToHelp(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is HelpVC})
        guard vc.count != 0 else {
             let helpVC = UIStoryboard.extraStoryboard().instantiateViewController(withIdentifier: ExtraControllers.HelpVC.rawValue) as! HelpVC
            navController.pushViewController(helpVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    func getNavigationController() -> UINavigationController{
        return sideMenuController?.rootViewController as! UINavigationController
    }
    
    func moveToTalks(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is TalksListVC})
        guard vc.count != 0 else {
            let talksListVC = UIStoryboard.chatStoryboard().instantiateViewController(withIdentifier: ChatControllers.TalksListVC.rawValue) as! TalksListVC
            navController.pushViewController(talksListVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    func moveToHome(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is HomeVC})
        guard vc.count != 0 else {
            let homeVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.HomeVC.rawValue) as! HomeVC
            navController.pushViewController(homeVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    func moveToGoals(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is GoalsVC})
        guard vc.count != 0 else {
            let homeVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.GoalsVC.rawValue) as! GoalsVC
            navController.pushViewController(homeVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    func moveToResultVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is ResultsVC})
        guard vc.count != 0 else {
            let resultsVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.ResultsVC.rawValue) as! ResultsVC
            navController.pushViewController(resultsVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
   
    func moveToAudioVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is AudioVC})
        guard vc.count != 0 else {
            let textBooksVC = UIStoryboard.mediaStoryboard().instantiateViewController(withIdentifier: MediaController.AudioVC.rawValue) as! AudioVC
            navController.pushViewController(textBooksVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    func moveToVideoVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is VideoVC})
        guard vc.count != 0 else {
            let textBooksVC = UIStoryboard.mediaStoryboard().instantiateViewController(withIdentifier: MediaController.VideoVC.rawValue) as! VideoVC
            navController.pushViewController(textBooksVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    func moveToEbookVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is EbookVC})
        guard vc.count != 0 else {
            let textBooksVC = UIStoryboard.mediaStoryboard().instantiateViewController(withIdentifier: MediaController.EbookVC.rawValue) as! EbookVC
            navController.pushViewController(textBooksVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    func moveToFavVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is FavoriteVC})
        guard vc.count != 0 else {
            let textBooksVC = UIStoryboard.mediaStoryboard().instantiateViewController(withIdentifier: MediaController.FavoriteVC.rawValue) as! FavoriteVC
            navController.pushViewController(textBooksVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    
    
    func moveToTextBookVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is TextBooksVC})
        guard vc.count != 0 else {
            let textBooksVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.TextBooksVC.rawValue) as! TextBooksVC
            navController.pushViewController(textBooksVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
    
    func moveToPurchasedVC(navController:UINavigationController){
        let vc = navController.viewControllers.filter({$0 is PurchasedListVC})
        guard vc.count != 0 else {
            let purchasedListVC = UIStoryboard.purchasesStoryboard().instantiateViewController(withIdentifier: PurchasesControllers.PurchasedListVC.rawValue) as! PurchasedListVC
            navController.pushViewController(purchasedListVC, animated: true)
            return
        }
        navController.popToViewController((vc.first)!, animated: true)
    }
}
extension LeftMenuVC : customConfirmationAlertViewDelegate {
    func okButtonPressed(index: Int) {
        
        UserDefaults.standard.setValue("Eng", forKey: "isLanguage")
        UserDefaults.standard.synchronize()
        DataHandler.sharedInstance.removeUserAndLogout()
        createConfirmationPopup?.dismiss(true)
    }
}
 
