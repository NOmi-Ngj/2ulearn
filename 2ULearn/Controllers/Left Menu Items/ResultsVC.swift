//
//  ResultsVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import UICircularProgressRing
import ObjectMapper
class ResultsVC: UIViewController, ShowsAlert {

    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var totalvocabulary: UILabel!
    @IBOutlet weak var totalgrammar: UILabel!
    @IBOutlet weak var totalword: UILabel!
    @IBOutlet weak var totalcharacters: UILabel!
   
    @IBOutlet weak var cirlceBlue: UICircularProgressRingView!
    @IBOutlet weak var circleOrange: UICircularProgressRingView!
    @IBOutlet weak var circleYellow: UICircularProgressRingView!
    @IBOutlet weak var circleGreen: UICircularProgressRingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        self.cirlceBlue.value = 0.0
        self.circleYellow.value = 0.0
        self.circleGreen.value = 0.0
        self.circleOrange.value = 0.0
        loadResultsListApi()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    @IBAction func leadershipIconTapped(_ sender: UIButton) {
        let leadershipBoardVC = UIStoryboard.chatStoryboard().instantiateViewController(withIdentifier: ChatControllers.LeadershipBoardVC.rawValue) as! LeadershipBoardVC
        self.navigationController?.pushViewController(leadershipBoardVC, animated: true)
      
    }
    
    func loadResultsListApi(){
        // Parametters for login api
        let params = [:] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.allResults.rawValue) { [weak self] (responseData, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            // handle response
            if  responseData != nil {
                debugPrint(responseData!)

                let data = (responseData as! [String:AnyObject])["data"]
                let resultsArr = Mapper<Results>().map(JSON: data as! [String:AnyObject])
                // % formula amount*100/totalAmount
                let wordsPercentage = resultsArr?.word != 0 ? self?.getPercentageFromValues(amount: resultsArr?.word ?? 0, totalAmount: resultsArr?.totalword ?? 1):0
                let grammarPercentage = resultsArr?.grammar != 0 ? self?.getPercentageFromValues(amount: resultsArr?.grammar ?? 0, totalAmount: resultsArr?.totalgrammar ?? 1):0
                let charactersPercentage = resultsArr?.characters != 0 ?  self?.getPercentageFromValues(amount: resultsArr?.characters ?? 0, totalAmount: resultsArr?.totalcharacters ?? 1):0
                let vocabularyPercentage =  resultsArr?.vocabulary != 0 ? self?.getPercentageFromValues(amount: resultsArr?.vocabulary ?? 0, totalAmount: resultsArr?.totalvocabulary ?? 1):0
                self?.cirlceBlue.value = CGFloat(vocabularyPercentage ?? 0.0)
                self?.circleYellow.value = CGFloat(grammarPercentage ?? 0.0)
                self?.circleGreen.value = CGFloat(wordsPercentage ?? 0.0)
                self?.circleOrange.value = CGFloat(charactersPercentage ?? 0.0)
                self?.totalword.text = "\(resultsArr?.word ?? 0)/\(resultsArr?.totalword ?? 0)"
                self?.totalgrammar.text = "\(resultsArr?.grammar ?? 0)/\(resultsArr?.totalgrammar ?? 0)"
                self?.totalcharacters.text = "\(resultsArr?.characters ?? 0)/\(resultsArr?.totalcharacters ?? 0)"
                self?.totalvocabulary.text = "\(resultsArr?.vocabulary ?? 0)/\(resultsArr?.totalvocabulary ?? 0)"
            }
        }
    }
    
    func getPercentageFromValues(amount: Int,totalAmount: Int) -> Double {
        let newamount = amount == 0 ? 10:amount
        let newtotalAmount = totalAmount == 0 ? 20:totalAmount
        let amountPercent = newamount*100
        var result: Double = Double(amountPercent / newtotalAmount)
        result = amount == 0 ? 0 : result
        return result
    }

}
