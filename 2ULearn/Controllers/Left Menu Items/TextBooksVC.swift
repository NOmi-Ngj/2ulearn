//
//  TextBooksVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class TextBooksVC: UIViewController,ShowsAlert{
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
 
    @IBOutlet weak var userPoints: UILabel!
     
    //MARK:- Helper Variables
    var BooksArr = [LessonsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        tableViewRegisterDelegateandDataSource()
        loadBooksApi()
        DataHandler.sharedInstance.getPoints { (points) in
            self.userPoints.text = "\(points)"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: BooksShelfTVC.NibName , bundle: nil), forCellReuseIdentifier: BooksShelfTVC.reuseIdentifier)
    }
    
    @IBAction func onPress_textBook(segue:UIStoryboardSegue) {
        
    }
    
    func loadBooksApi(){
        // Parametters for login api
        let params =   [:] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.allBooks.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            if  response != nil {
                debugPrint(response!)
                self?.BooksArr = Mapper<LessonsModel>().mapArray(JSONArray: response![ResponseKey.data.rawValue] as! [[String : AnyObject]]) ?? []
                self?.tableView.reloadData()
            }
        }
    }

}
extension TextBooksVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BooksArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BooksShelfTVC.reuseIdentifier, for: indexPath) as! BooksShelfTVC
        cell.selectionStyle = .none
        
        var image1:String,image2:String,image3:String = ""
        image1 = BooksArr[indexPath.row].books?.count ?? 0 > 0 ? BooksArr[indexPath.row].books![0].image_url?.scaledImageURL() ?? "":""
        image2 = BooksArr[indexPath.row].books?.count ?? 0 > 1 ? BooksArr[indexPath.row].books![1].image_url?.scaledImageURL() ?? "":""
        image3 = BooksArr[indexPath.row].books?.count ?? 0 > 2 ? BooksArr[indexPath.row].books![2].image_url?.scaledImageURL() ?? "":""
        cell.setData(title: BooksArr[indexPath.row].name ?? "", details: BooksArr[indexPath.row].descriptions ?? "",  img1: image1, img2: image2, img3: image3) 
        cell.delegate = self
        cell.seeAllButton.tag = indexPath.row
        return cell
    }
}
extension TextBooksVC : seeAllBooksDelegates{
    func onSeeAllTapped(sender: UIButton) {
        debugPrint(sender.tag)
        let textBookListVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.TextBookListVC.rawValue) as! TextBookListVC
        textBookListVC.booksList = BooksArr[sender.tag].books ?? []
        textBookListVC.strTitleText = BooksArr[sender.tag].name ?? ""
        self.navigationController?.pushViewController(textBookListVC, animated: true)
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension TextBooksVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
