//
//  GoalsVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class GoalsVC: UIViewController ,ShowsAlert{

    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userPoints: UILabel!
     
    //MARK:- Helper Variables
    var GoalsArr = [LessonsModel]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        tableViewRegisterDelegateandDataSource()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadGoalsApi()
        DataHandler.sharedInstance.getPoints { (points) in
            self.userPoints.text = "\(points)"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: GoalsTVC.NibName , bundle: nil), forCellReuseIdentifier: GoalsTVC.reuseIdentifier)
    }

    @IBAction func onPress_1NextLesson(segue:UIStoryboardSegue) {
         
    }
    @IBAction func onPress_1RetryLesson(segue:UIStoryboardSegue) {
        
    }
    @IBAction func onPress_1Xclose(segue:UIStoryboardSegue) {
    }
    
    func loadGoalsApi(){
        // Parametters for login api
        let params =   [//            "type":"home"
            :
            ] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.goals.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            if  response != nil {
                debugPrint(response!)
                self?.GoalsArr = Mapper<LessonsModel>().mapArray(JSONArray: response![ResponseKey.data.rawValue] as! [[String : AnyObject]]) ?? []
                self?.tableView.reloadData()
            }
        }
    }
}
extension GoalsVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GoalsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GoalsTVC.reuseIdentifier, for: indexPath) as! GoalsTVC
        cell.selectionStyle = .none
        cell.setGoalIndex(index: indexPath.row)
        cell.setData(title: GoalsArr[indexPath.row].name ?? "", details: GoalsArr[indexPath.row].descriptions ?? "", lessons: GoalsArr[indexPath.row].lessons ?? [])
        cell.lessonDelegates = self
        return cell
    }
}

extension GoalsVC:lessonSelectionDelegate{
   
    func didSelectLessons(lessons: [QuestionsList]?, goalIndex: Int, index: Int) {
        let is_attempt = index == 0 ? self.GoalsArr[goalIndex].lessons![index].is_attempt:self.GoalsArr[goalIndex].lessons![index-1].is_attempt
//        let lessonAttempt = index == 0 ? lessons![index]:lessons![index-1]
        if index == 0 || is_attempt == "true"{
            if  lessons?.count ?? 0 != 0 {
                DataHandler.sharedInstance.lessonsPagerItemsController = []
                DataHandler.sharedInstance.lessonsPagerItemsId = []
                DataHandler.sharedInstance.lessonsPagerIndex = 0
                DataHandler.sharedInstance.lessonsSelectionType = 1
                DataHandler.sharedInstance.lessonsPagerCount = lessons?.count ?? 0
                DataHandler.sharedInstance.questionsList = lessons ?? []
                
                DataHandler.sharedInstance.lessonsName = GoalsArr[goalIndex].lessons?[index].name ?? ""
                DataHandler.sharedInstance.lessonsImageUrl = GoalsArr[goalIndex].lessons?[index].image_url?.scaledImageURL() ?? ""
                DataHandler.sharedInstance.lessonsDetails = GoalsArr[goalIndex].lessons?[index].descriptions ?? ""
                DataHandler.sharedInstance.lessonType = []
                Helper.showQuestionTypeViewController(type: DataHandler.sharedInstance.questionsList[0].type ?? "", navigation: self.navigationController!)
            }
        }
    }
     
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension GoalsVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
