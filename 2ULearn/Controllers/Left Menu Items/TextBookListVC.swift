//
//  TextBookListVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class TextBookListVC: UIViewController {

    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var strTitleText = ""
    var booksList = [BooksList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        collectionviewRegisterDelegateandDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
       titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
        titleText.text = strTitleText
    }
    
    
    func collectionviewRegisterDelegateandDataSource(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self
        collectionView.register(UINib(nibName: BooksListCVC.NibName, bundle: nil), forCellWithReuseIdentifier: BooksListCVC.reuseIdentifier)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    } 
}
extension TextBookListVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return booksList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BooksListCVC.reuseIdentifier, for: indexPath) as! BooksListCVC
        cell.setData(title: booksList[indexPath.row].name ?? "", imgUrl:booksList[indexPath.row].image_url?.scaledImageURL() ?? "")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let textBookDetailsVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.TextBookDetailsVC.rawValue) as! TextBookDetailsVC
       textBookDetailsVC.str_titleText = booksList[indexPath.row].name ?? ""
        textBookDetailsVC.bookDetail = booksList[indexPath.row]
        self.navigationController?.pushViewController(textBookDetailsVC, animated: true)
    }
    
}
extension TextBookListVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.size.width)/3-10
        let height = (collectionView.frame.size.height)/3-10
        return CGSize(width:  width , height: height)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension TextBookListVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
