//
//  TextBookDetailsVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import YYWebImage
class TextBookDetailsVC: UIViewController ,ShowsAlert{

    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var bookTitle: UILabel!
    
    @IBOutlet weak var cartIconBtn: UIButton!
    @IBOutlet weak var cartIconImageView: UIImageView!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var addToCartImageView: UIImageView!
    @IBOutlet weak var cartCountLable: UILabel!
    @IBOutlet weak var viewCartCount: UIView!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookDetails: UITextView!
    @IBOutlet weak var bookPrice: UILabel!
    var isFromPurchases = false
    
    var str_titleText:String = ""
    var bookDetail:BooksList?
    override func viewDidLoad() {
        super.viewDidLoad()
        bookDetails.delegate = self
        applicationFontsSetting()
        bookDetails.attributedText = Helper.attributedStringWithLineSpacing(string: bookDetail?.descriptions ?? "", lineSpacing: 5, stralign: "center")
        bookPrice.text = "$\(bookDetail?.price ?? 0)"
        titleText.text = bookDetail?.name ?? ""
        bookTitle.text = bookDetail?.name?.capitalized ?? ""
        bookImage.yy_setImage(with: URL(string: bookDetail?.image_url?.scaledImageURL() ?? ""), placeholder:#imageLiteral(resourceName: "graybg"))
        
        DispatchQueue.main.async {
            self.viewCartCount.cornerRadiuss = self.viewCartCount.frame.size.height/2
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cartIconSetting()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cartIconSetting(){
        if isFromPurchases == false{
            self.viewCartCount.isHidden = DataHandler.sharedInstance.cartList.count > 0 ? false:true
            self.cartCountLable.text = "\(DataHandler.sharedInstance.cartList.count)"
        }else{
            self.viewCartCount.isHidden = true
            self.addToCartImageView.isHidden = true
            self.addToCartBtn.isHidden = true
            self.cartIconBtn.isHidden = true
            self.cartIconImageView.isHidden = true
        }
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
        bookTitle.font = UIFont.appMontFonts(size: 21)
        bookPrice.font = UIFont.appMontFonts(size: 21)
        bookDetails.font = UIFont.appMontFonts(size: 15)
        bookDetails.textColor = UIColor.appLightGrayColor()
    }
    @IBAction func onPress_AddToCart(_ sender: Any) {
       
        
        DataHandler.sharedInstance.cartList.append(bookDetail!)
        self.cartIconSetting()
        self.validationAlertView(title: "2ULearn", message: "Added To Cart", okButtonText: "OK")
    }
    @IBAction func onPress_Back(_ sender: Any) { 
        goBack()
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func onPress_Checkout(_ sender: Any) {
        let thankYouVC = UIStoryboard.purchasesStoryboard().instantiateViewController(withIdentifier: PurchasesControllers.MyCartsVC.rawValue) as! MyCartsVC
        thankYouVC.isFromPurchased = false
        self.navigationController?.pushViewController(thankYouVC, animated: true)
        
    }
}

extension TextBookDetailsVC:UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }
}
