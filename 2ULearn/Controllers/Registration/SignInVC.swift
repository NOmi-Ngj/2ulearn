//
//  SignInVC.swift
//  2ULearn
//
//  Created by mac on 6/12/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import TextFieldEffects
import SwiftValidator
import ObjectMapper
import FirebaseDatabase
    
class SignInVC: UIViewController ,ShowsAlert{

    //MARK:- My Outblets
    @IBOutlet weak var nameTextField: HoshiTextField!
    @IBOutlet weak var passwordTextField: HoshiTextField!
    @IBOutlet weak var nameErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    
    @IBOutlet weak var forgotLine: UIView!
    
    @IBOutlet weak var welcomeToMandarinLanguageLabel: UILabel!
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    @IBOutlet weak var signinButton: UIButton!
    
    //MARK:- Helper Variables
    let validator = Validator()
    
    
    //MARK:- Didload
    override func viewDidLoad() {
        super.viewDidLoad()
        validationSetupForTextFields()
        applicationFontsSetup()
        if Common.isTesting() == true{
            nameTextField.text = "royal01@mailinator.com"
            passwordTextField.text = "123123123"
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.forgotLine.alpha = 0.2
    }
   
    func applicationFontsSetup(){

        welcomeToMandarinLanguageLabel.text = NSLocalizedString("Welcome To 2ULearn Language", comment: "")
    
        let string = Helper.attributedStringWithLineSpacing(string: welcomeToMandarinLanguageLabel.text ?? "", lineSpacing: 10, stralign: "center")
        welcomeToMandarinLanguageLabel.attributedText = string
        
        nameErrorLabel.font = UIFont.appMontFonts(size: 11)
        passwordErrorLabel.font = UIFont.appMontFonts(size: 11) 
        welcomeToMandarinLanguageLabel.font = UIFont.appBoldFonts(size: 23)
        forgotPasswordLabel.font = UIFont.appMontFonts(size: 15) 
        
        nameTextField.font = UIFont.appRegularFonts(size: 17)
        passwordTextField.font = UIFont.appRegularFonts(size: 17)
        signinButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onPressed_ForgotPassword(_ sender: Any) {
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 1.0, animations: {
        self.forgotLine.alpha = 1
        }) { (complete) in
            self.view.isUserInteractionEnabled = true
            let vc = UIStoryboard.registrationStoryboard().instantiateViewController(withIdentifier: RegistrationControllers.ForgotPasswordVC.rawValue) as! ForgotPasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onPressed_SignIn(_ sender: UIButton) {
        validator.validate(delegate: self)
    }

    func validationSetupForTextFields(){
        nameTextField.delegate = self
        passwordTextField.delegate = self
        validator.registerField(textField: nameTextField, errorLabel: nameErrorLabel, rules: [RequiredRule(), MinLengthRule(length: 2), EmailRule()])
        validator.registerField(textField: passwordTextField, errorLabel: passwordErrorLabel, rules: [RequiredRule(), MinLengthRule(length: 2)])
    }
    
    //MARK:- Api Call's
    func hitLoginApi(){
        // Parametters for login api
        let params =   [
            "email":"\(self.nameTextField.text!)",
            "password":"\(self.passwordTextField.text!)",
            "login_type":"normal"
            ] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.postRequest(params, kPath: Users.login.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
           
            
            // handle error
            if  error != nil {
                self?.hideLoaderView()
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            if  response != nil {
                debugPrint(response!)
                
                let user = Mapper<User>().map(JSON: response as! [String : AnyObject])
                DataHandler.sharedInstance.user = user
                Helper.removeUser()
                Helper.saveUser(user: response!)
                let _ = Helper.getUser()
                var email = user?.email ?? ""
                var Uid = user?.id ?? ""
                var imageUrl = user?.image_url?.threeX ?? ""
//                if Common.isTesting() {
//                    email = "royal01asdfa@mailinator.com"
//                    Uid = "asdfasdfa"
//                }
                
                let fcmToken = Common.getValueForKey(keyValue: kdeviceToken)
                
                ChatUser.loginUser(withEmail: email, password: "123456789", completion: { (response, error) in
                    if  error != nil {
                        //Register user incase user not exist. error?.code = 17011
                        if error?.code == 17011 {
                            ChatUser.registerUser(full_name: user?.name ?? "", first_name: user?.name ?? "", last_name: user?.name ?? "", email: email, password: "123456789", profilePicUrl: imageUrl, isLoggedIn: 1, udid: fcmToken, UserAppId: Uid, completion: {  (responseData, error) in
                                
                                self?.hideLoaderView()
                                if error != nil {
                                    debugPrint(error?.localizedDescription ?? "")
                                    self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                                }
                                if responseData != nil {
                                    print("response in success")
                                    print(responseData ?? "sucess")
                                    let vc = UIStoryboard.homeStoryboard().instantiateViewController(withIdentifier: HomeControllers.MainVC.rawValue) as! MainVC
                                    UIApplication.shared.keyWindow?.rootViewController = vc
                                }
                            })
                        }else{
                            self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                        }
                    }
                    if response != nil {
                        self?.hideLoaderView()
                        
                        let values = ["first_name": user?.name ?? "", "last_name": user?.name ?? "", "user_name": user?.name ?? "" , "email" : email, "id" : Uid, "isLoggedIn" : 1, "udid" : fcmToken, "full_name" : user?.name ?? "","profilePicUrl":imageUrl] as! [String : Any]
//                        ,"profilePicUrl":user?.image_url?.threeX ?? ""
//                        let values = ["first_name": first_name, "last_name": last_name, "email" : email, "id" : UserAppId, "isLoggedIn" : 1, "udid" :udid , "full_name" : full_name,"profilePicUrl":profilePicUrl] as [String : Any]
                        
                        Database.database().reference().child("users").child(Uid).updateChildValues(values)
                        let vc = UIStoryboard.homeStoryboard().instantiateViewController(withIdentifier: HomeControllers.MainVC.rawValue) as! MainVC
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                })
                

                
            }
            
        }
        
    }
    
    func checkIfUserFound(uid: String, user_name: String, email: String, dictionary: NSDictionary)
    {
        let db = Database.database().reference()
        print("Before attaching observer");
        db.child("users").child(uid).observe(.value, with: { (snapshot) in
            print("Data has loaded");
            print(snapshot)
            
            let usrName = user_name
            
            if snapshot.value is NSNull {
                print("NUll")
               // self.createObjectInFirebase(uid: uid, profilePic: (self.jugarImg?.image)!, withName: usrName, email: email, dictionary: dictionary)
            }
            else
            {
                
            }
        })
        print("After attaching observer");
        
    }
    
    // MARK: - Firebase create
    func createObjectInFirebase(uid: String, profilePic: UIImage, withName: String, email: String, dictionary: NSDictionary)
    {
        
        let first_name = dictionary.object(forKey: "first_name") as! String
        let last_name = dictionary.object(forKey: "last_name") as! String
        let fullname = first_name + " " + last_name
        let user_name = dictionary.object(forKey: "user_name") as! String
        
        var uUID = UIDevice.current.identifierForVendor!.uuidString
//
//        if(getValueForKey(keyValue: kSaveUDID) == kEmptyString)
//        {
//            saveUsrDefault(keyValue: uUID, valueIs: kSaveUDID)
//        }
//        else
//        {
//            uUID = getValueForKey(keyValue: kSaveUDID)
//        }
        
        let images = dictionary.object(forKey: "images") as! NSDictionary
        var firstObject = ""
        var secondObject = ""
        var thirdObject = ""
        
        if var firstObject = images.object(forKey: "1x") as? String{
            firstObject = images.object(forKey: "1x") as! String
            secondObject = images.object(forKey: "2x") as! String
            thirdObject = images.object(forKey: "3x") as! String
        }
        
        let values = ["first_name": first_name, "last_name": last_name, "user_name": user_name , "email" : email, "id" : Int(uid)!, "isLoggedIn" : 1, "udid" :uUID , "full_name" : fullname] as [String : Any]
        
        Database.database().reference().child("users").child(uid).updateChildValues(values)
        
        let imagesData = ["id" : 13, "one" : firstObject , "scaledImageURL" : thirdObject, "three" : thirdObject, "two": secondObject , "user_id" : Int(uid)!] as [String : Any]
        Database.database().reference().child("users").child(uid).child("images").updateChildValues(imagesData)
        
        print("")
        /* let storageRef = Storage.storage().reference().child("usersProfilePics").child(uid)
         let imageData = UIImageJPEGRepresentation(profilePic, 0.1)
         storageRef.putData(imageData!, metadata: nil, completion: { (metadata, err) in
         if err == nil {
         //let path = metadata?.downloadURL()?.absoluteString
         //                let values = ["first_name": withName, "full_name": email, "profilePicLink": path!, "uid":uid, "isDeleted":"false"]
         
         
         }
         })
         */
    }
    
//    // MARK: - Firebase create
//    func createObjectInFirebase(uid: String, profilePic: UIImage, withName: String, email: String, dictionary: NSDictionary)
//    {
//
//        let first_name = dictionary.object(forKey: "first_name") as! String
//        let last_name = dictionary.object(forKey: "last_name") as! String
//        let fullname = first_name + " " + last_name
//        let user_name = dictionary.object(forKey: "user_name") as! String
//
//        var uUID = UIDevice.current.identifierForVendor!.uuidString
//
//        if(getValueForKey(keyValue: kSaveUDID) == kEmptyString)
//        {
//            saveUsrDefault(keyValue: uUID, valueIs: kSaveUDID)
//        }
//        else
//        {
//            uUID = getValueForKey(keyValue: kSaveUDID)
//        }
//
//        let images = dictionary.object(forKey: "images") as! NSDictionary
//        var firstObject = ""
//        var secondObject = ""
//        var thirdObject = ""
//
//        if var firstObject = images.object(forKey: "1x") as? String
//        {
//            firstObject = images.object(forKey: "1x") as! String
//            secondObject = images.object(forKey: "2x") as! String
//            thirdObject = images.object(forKey: "3x") as! String
//        }
//        else if(self.socialSocialMediaLink != "")
//        {
//            firstObject = self.socialSocialMediaLink
//            secondObject = self.socialSocialMediaLink
//            thirdObject = self.socialSocialMediaLink
//        }
//
//        let values = ["first_name": first_name, "last_name": last_name, "user_name": user_name , "email" : email, "id" : Int(uid)!, "isLoggedIn" : 1, "udid" :uUID , "full_name" : fullname] as [String : Any]
//
//        Database.database().reference().child("users").child(uid).updateChildValues(values)
//
//        let imagesData = ["id" : 13, "one" : firstObject , "scaledImageURL" : thirdObject, "three" : thirdObject, "two": secondObject , "user_id" : Int(uid)!] as [String : Any]
//        Database.database().reference().child("users").child(uid).child("images").updateChildValues(imagesData)
//    }
    
}
extension SignInVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SignInVC:ValidationDelegate {
    
    func validationSuccessful() {
       hitLoginApi()
        print("Success")
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        validator.validateField(textField: textField){ error in
            if error == nil {
                // Field validation was successful
                print("Success")
            } else {
                print("error")
                // Validation error occurred
            }
        }
        return true
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in errors {
            if let field = field as? HoshiTextField {
               field.animateView(view: field)
               
                field.placeholderColor = UIColor.black
                signinButton.isUserInteractionEnabled = false
                
                error.errorLabel?.text = error.errorMessage // works if you added labels
                error.errorLabel?.isHidden = false
                let when2 = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when2){
                    error.errorLabel?.isHidden = true
                    field.placeholderColor = UIColor.white
                    self.signinButton.isUserInteractionEnabled = true
                }
            }
            
            
            
        }
    }
}
