//
//  ForgotPasswordVC.swift
//  2ULearn
//
//  Created by mac on 6/13/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import TextFieldEffects
import SwiftValidator

class ForgotPasswordVC : UIViewController ,ShowsAlert{
    
    @IBOutlet weak var emailTextField: HoshiTextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    
    @IBOutlet weak var forgotLine: UIView!
    
    @IBOutlet weak var backToLoginLbl: UILabel!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    @IBOutlet weak var welcomeToMandarinLanguageLabel: UILabel!
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Common.isTesting() == true{
            emailTextField.text = "abc@gmail.com" 
        }
       
        applicationFontsSetupAndRegisterValidation()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.forgotLine.alpha = 0.2
    }
    func applicationFontsSetupAndRegisterValidation(){
        
        let alertTitle = NSLocalizedString("Welcome To 2ULearn Language", comment: "")
        welcomeToMandarinLanguageLabel.text = alertTitle
        
        let string = Helper.attributedStringWithLineSpacing(string: welcomeToMandarinLanguageLabel.text ?? "", lineSpacing: 10, stralign: "center")
        welcomeToMandarinLanguageLabel.attributedText = string
        emailErrorLabel.font = UIFont.appMontFonts(size: 11) 
        welcomeToMandarinLanguageLabel.font = UIFont.appBoldFonts(size: 23)
        
        emailTextField.font = UIFont.appRegularFonts(size: 17)
        backToLoginLbl.font = UIFont.appMontFonts(size: 15)
        
        resetPasswordButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
        
         validator.registerField(textField: emailTextField, errorLabel: emailErrorLabel, rules: [RequiredRule(), MinLengthRule(length: 2)])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onPressed_SignIn(_ sender: UIButton) {
        
        validator.validate(delegate: self)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        UIView.animate(withDuration: 1.0, animations: {
            self.forgotLine.alpha = 1
        }) { (complete) in
            self.goBack()
        }
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Api Call's
    func hitForgotApi(){
        // Parametters for login api
        let params =   [
            "email":"\(self.emailTextField.text!)",
            ] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.postRequest(params, kPath: Users.forgotPassword.rawValue) { [weak self] (response, error) in
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                return
            }
            if  response == nil {
                
                let message:String = "Reset Password Email Sent"
                self?.showSuccessAlertMessage(title: "Empowered", message: message, duration: 2.5) { (result) in
                    self?.goBack()
                }
                //                self?.validationAlertView(title: "Alert", message: "Password Reset Email Sent", okButtonText: "OK")
            }
            
        }
        
    }
    
    
}
extension ForgotPasswordVC:ValidationDelegate {
    
    func validationSuccessful() {
        print("Success")
        hitForgotApi()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        validator.validateField(textField: textField){ error in
            if error == nil {
                // Field validation was successful
                print("Success")
            } else {
                print("error")
                // Validation error occurred
            }
        }
        return true
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in errors {
            if let field = field as? HoshiTextField {
                field.animateView(view: field)
                
                field.placeholderColor = UIColor.black
                resetPasswordButton.isUserInteractionEnabled = false
                
                error.errorLabel?.text = error.errorMessage // works if you added labels
                error.errorLabel?.isHidden = false
                let when2 = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when2){
                    error.errorLabel?.isHidden = true
                    field.placeholderColor = UIColor.white
                    self.resetPasswordButton.isUserInteractionEnabled = true
                }
            }
        }
    }
}
