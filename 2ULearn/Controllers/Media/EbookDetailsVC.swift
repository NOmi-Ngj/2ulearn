//
//  EbookDetailsVC.swift
//  2ULearn
//
//  Created by NoumanGul on 5/13/19.
//  Copyright © 2019 CMolds. All rights reserved.
//

import UIKit
import MarqueeLabel
import Alamofire

class EbookDetailsVC: UIViewController ,ShowsAlert {
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    let colors = [
        DotColors(first: color(0x7DC2F4), second: color(0xE2264D)),
        DotColors(first: color(0xF8CC61), second: color(0x9BDFBA)),
        DotColors(first: color(0xAF90F4), second: color(0x90D1F9)),
        DotColors(first: color(0xE9A966), second: color(0xF8C852)),
        DotColors(first: color(0xF68FA7), second: color(0xF6A2B8))
    ]
    @IBOutlet weak var titleTexts : UILabel!
    @IBOutlet weak var marqueeDetails : MarqueeLabel!
    @IBOutlet weak var heartButton: FaveButton!
    @IBOutlet weak var webView: UIWebView!
    
    var media:mMedia?
    var dataRequest : DataRequest?
    var delegatesForHeartButton : onHeartReloadViewControllerDelegates!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPlayerData()
        
        
        let url: URL! = URL(string:media?.media ?? "")
        webView.loadRequest(URLRequest(url: url))
        webView.scalesPageToFit = true
        titleTexts.text = media?.title ?? ""
        marqueeDetails.text = media?.description ?? ""
        // Do any additional setup after loading the view.
    }
    func loadPlayerData(){
        let isSelected = self.media?.is_favourite == 1 ? true:false
        self.heartButton?.setSelected(selected: isSelected, animated: false)
    }
 
    @IBAction func backTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func markOrRemoveFromFavorite(isSelected:Bool){
        //        dataRequest?.cancel()
        let requestPath = isSelected == true ?Media.addFav.rawValue:Media.removFav.rawValue
        let params = ["content_id":media?.id ?? ""] as [String : AnyObject]
        // Show loading on api call
//        self.hideLoaderView()
//        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        dataRequest = ApiHelper.postRequest(params, kPath: requestPath) { [weak self] (response, error) in
            //hide loader on response
//            self?.hideLoaderView()
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            
                self?.loadPlayerData()
                return
            }
            self?.media?.is_favourite = isSelected == true ? 1:0
            self?.delegatesForHeartButton.onHeardReloadViewData()
            self?.loadPlayerData()
        } 
    }
    
    @IBAction func heartbuttonTapped(){
        heartButton.isUserInteractionEnabled = false
        dataRequest?.cancel()
        
    }
}

extension EbookDetailsVC:FaveButtonDelegate{
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        heartButton.isUserInteractionEnabled = true
        markOrRemoveFromFavorite(isSelected: selected)
    }
    
    func faveButtonDotColors(_ faveButton: FaveButton) -> [DotColors]?{
        if( faveButton === heartButton){
            return colors
        }
        return nil
    }
    
}
