//
//  AudioPlayerVC.swift
//  2ULearn
//
//  Created by NoumanGul on 5/9/19.
//  Copyright © 2019 CMolds. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import Foundation
import MarqueeLabel
import IHEqualizerView
import Photos
import Alamofire



class AudioPlayerVC: UIViewController,AVAudioPlayerDelegate,ShowsAlert {
    let colors = [
        DotColors(first: color(0x7DC2F4), second: color(0xE2264D)),
        DotColors(first: color(0xF8CC61), second: color(0x9BDFBA)),
        DotColors(first: color(0xAF90F4), second: color(0x90D1F9)),
        DotColors(first: color(0xE9A966), second: color(0xF8C852)),
        DotColors(first: color(0xF68FA7), second: color(0xF6A2B8))
    ]
    @IBOutlet weak var audioVisualizationView: IHWaveFormView!
    @IBOutlet weak var heartButton: FaveButton!
    var audioPlayer: AVAudioPlayer?
    var updater : CADisplayLink! = nil
    var media:mMedia?
    @IBOutlet weak var titleText : UILabel!
    @IBOutlet weak var marqueeDetails : MarqueeLabel!
    @IBOutlet weak var sliderView : UISlider!
    @IBOutlet weak var startTime : UILabel!
    @IBOutlet weak var endTime : UILabel!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var playButton : UIButton!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var viewForTap : UIView!
    
    var dataRequest : DataRequest?
    var fileUrl = String()
    var isGoBack = false
    var isPlayingAudio = false
    var delegatesForHeartButton : onHeartReloadViewControllerDelegates!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isGoBack = false
        (UIApplication.shared.keyWindow?.rootViewController as! MainVC).isLeftViewSwipeGestureEnabled = false

        loadPlayerData()
        self.sliderView.minimumValue = 0
        let audioURL = URL.init(string: media?.media ?? "")!
        downloadFileFromURL(url: audioURL)
     
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.viewForTap.addGestureRecognizer(tap)
        tappedOnScreenHandling()
        titleText.text = media?.title ?? ""
        marqueeDetails.text = media?.description ?? ""
    }
    func loadPlayerData(){
        let isSelected = self.media?.is_favourite == 1 ? true:false
        self.heartButton?.setSelected(selected: isSelected, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if updater != nil{
            updater?.invalidate()
        }
        isGoBack = true
        self.audioVisualizationView.player?.stop()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if updater != nil{
            updater?.invalidate()
        }
        isGoBack = true
        self.audioVisualizationView.player?.stop()
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        tappedOnScreenHandling()
    }
    func tappedOnScreenHandling(){
        topTitle.isHidden = !topTitle.isHidden
        playButton.isHidden = !playButton.isHidden
        backButton.isHidden = !backButton.isHidden
        heartButton.isHidden = !heartButton.isHidden
        sliderView.isHidden = !sliderView.isHidden
        startTime.isHidden = !startTime.isHidden
      //  marqueeDetails.isHidden = !marqueeDetails.isHidden
        endTime.isHidden = !endTime.isHidden
       // titleText.isHidden = !titleText.isHidden
    }
    
    deinit {
        debugPrint("deinit Player VC")
    }
    
    @IBAction func backTapped(){
        (UIApplication.shared.keyWindow?.rootViewController as! MainVC).isLeftViewSwipeGestureEnabled = true
        if updater != nil{
            updater?.invalidate()
        }
        isGoBack = true
        self.audioVisualizationView.player?.stop()
        self.audioVisualizationView.player?.volume = 0.0
        self.navigationController?.popViewController(animated: true)

//
    }
    
    @IBAction func audioPlayerTapped(sender:UIButton){
        
        if self.audioVisualizationView.player?.isPlaying == true{
            
            isPlayingAudio = false
            self.audioVisualizationView.player?.pause()
            if updater != nil{
                updater.invalidate()
            }
            let image = #imageLiteral(resourceName: "icon_play")
            DispatchQueue.main.async {
                sender.setImage(image, for: .normal)
            }
            
        }else if self.audioVisualizationView.player != nil{
            isPlayingAudio = true
            let image = #imageLiteral(resourceName: "icon_pause")
            DispatchQueue.main.async {
                sender.setImage(image, for: .normal)
            }
            self.audioVisualizationView.player?.play()
            runTrackUpdater()
        }
    }
    
    func downloadFileFromURL(url:URL){
        self.downloadAudioAndAddInApp(showLoader: true)
        
        
//        let idPath2 = self.media?.id ?? "media"
//        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
//        let url = NSURL(fileURLWithPath: path)
//        if let pathComponent = url.appendingPathComponent("\(idPath2).mp3") {
//            let filePath = pathComponent.path
//            let fileManager = FileManager.default
//            if fileManager.fileExists(atPath: filePath) {
//                print("FILE AVAILABLE")
//                let filePath = "\(path)/\(idPath2).mp3"
//                self.fileUrl = filePath
//                self.downloadAudioAndAddInApp(showLoader: false)
//
//                DispatchQueue.main.async {
//                    if self.isGoBack == false{
//                        self.audioVisualizationView.delegate = self
//                        self.audioVisualizationView.dataSource = self
//                    }
//                }
//            } else {
//                print("FILE NOT AVAILABLE")
//                self.downloadAudioAndAddInApp(showLoader: true)
//            }
//        }
//        else {
//            print("FILE PATH NOT AVAILABLE")
//        }
        
    }
    
    func downloadAudioAndAddInApp(showLoader:Bool){
        if showLoader{
            showLoaderView(isCenter: true, UserInterfaceEnable: false)
        }
        
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: self.media?.media ?? ""),
                let data = NSData(contentsOf: url) {
                
                DispatchQueue.main.async {
                    self.hideLoaderView()
                }
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let idPath = self.media?.id ?? "media"
                let filePath = "\(documentsPath)/\(idPath).mp3"
                self.fileUrl = filePath
                
                DispatchQueue.main.async {
                    data.write(toFile: filePath, atomically: true)
                    if self.isGoBack == false && showLoader{
                        self.audioVisualizationView.delegate = self
                        self.audioVisualizationView.dataSource = self
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.hideLoaderView()
                }
            }
        }
        
    }
    func runTrackUpdater(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.updater = CADisplayLink(target: self, selector: #selector(self.trackAudio))
            self.updater.frameInterval = 1
            self.updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
        }
        
    }
    
    func play(url:URL) {
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: url as URL)
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
            DispatchQueue.main.async {
                self.sliderView.minimumValue = 0
                self.sliderView.maximumValue = Float(self.audioPlayer?.duration ?? 0.0)
            }
            
            runTrackUpdater()
            
        } catch let error as NSError {
            self.audioPlayer = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
    }
    
    
    @IBAction func scrubAudioChange(sender: UISlider) {
        self.audioVisualizationView.player?.currentTime = TimeInterval(sender.value)
        if self.audioVisualizationView.player?.isPlaying == true{
            self.audioVisualizationView.player?.prepareToPlay()
            self.audioVisualizationView.player?.play()
        }
    }
    @objc func trackAudio() {
        if self.audioVisualizationView.player != nil{

            
            if self.audioVisualizationView.xPoint > 410.0{
                DispatchQueue.main.async {
                    self.audioVisualizationView.eraseView()
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    // your code here
                    self.audioVisualizationView.xPoint = 0.0
                }
                
            }
            let currentTimeBtn = self.audioVisualizationView.player.currentTime
            
            let progress = ((self.audioVisualizationView.player?.currentTime ?? 0.0) * (100.0) / (self.audioVisualizationView.player?.duration ?? 0.0 ))
            self.sliderView.value = Float(progress)
            
            let currentTime = self.audioVisualizationView.player?.currentTime.rounded() ?? 0.0
            hmsFrom(seconds: Int(currentTime.rounded())) { (hours, minutes, seconds) in
                
                let hours = self.getStringFrom(seconds: hours)
                let minutes = self.getStringFrom(seconds: minutes)
                let seconds = self.getStringFrom(seconds: seconds)
                
                self.startTime.text = "\(hours):\(minutes):\(seconds)"
            }
            let endTimeCount = (self.audioVisualizationView.player?.duration.rounded() ?? 0.0) - (self.audioVisualizationView.player?.currentTime.rounded() ?? 0.0)
            hmsFrom(seconds: Int(endTimeCount.rounded())) {  (hours, minutes, seconds) in
                
                let hours = self.getStringFrom(seconds: hours)
                let minutes = self.getStringFrom(seconds: minutes)
                let seconds = self.getStringFrom(seconds: seconds)
                self.endTime.text = "\(hours):\(minutes):\(seconds)"
            }
        }
    }
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {

    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        
    }
    
    func markOrRemoveFromFavorite(isSelected:Bool){
        
        
        let requestPath = isSelected == true ?Media.addFav.rawValue:Media.removFav.rawValue
        let params = ["content_id":media?.id ?? ""] as [String : AnyObject]
        // Show loading on api call
//        self.hideLoaderView()
//        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        
        dataRequest = ApiHelper.postRequest(params, kPath: requestPath) { [weak self] (response, error) in
            //hide loader on response
//            self?.hideLoaderView()
            // handle error
            
            if  error != nil {
                let validate = self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK", okButtonDelegates: true)
                validate?.okDelegate = self
                
                for view in self?.view.subviews ?? []{
                    if view is ValidationAlertView{
                        view.removeFromSuperview()
                    }
                }
                validate?.show()
                self?.loadPlayerData()
                return
            }
            self?.media?.is_favourite = isSelected == true ? 1:0
            self?.delegatesForHeartButton.onHeardReloadViewData()
            self?.loadPlayerData()
            
        }
    }
    
    @IBAction func heartbuttonTapped(){
        heartButton.isUserInteractionEnabled = false
        dataRequest?.cancel()
        
    }
}

extension AudioPlayerVC:ButtonOKPressedDelegates{
    func okButtonPressed() {
        self.view.isUserInteractionEnabled = true
    }
}
extension AudioPlayerVC:FaveButtonDelegate{
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        
        heartButton.isUserInteractionEnabled = true
        markOrRemoveFromFavorite(isSelected: selected)
    }
    
    func faveButtonDotColors(_ faveButton: FaveButton) -> [DotColors]?{
        if( faveButton === heartButton){
            return colors
        }
        return nil
    }
}
extension AudioPlayerVC:IHWaveFormViewDelegate,IHWaveFormViewDataSource{
    
    func didFinishPlayBack() {
        if updater != nil{
            updater?.invalidate()
        }
        isPlayingAudio = false
        
        if self.heartButton.isHidden{
            tappedOnScreenHandling()
        }
        self.audioVisualizationView.eraseView()
        self.audioVisualizationView.redrawView()
        self.audioVisualizationView.xPoint = 0.0
        self.audioVisualizationView.player?.stop()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let image = #imageLiteral(resourceName: "icon_play")
            self.playButton.setImage(image, for: .normal)
        }
        
    }
    func didStartPlayingWithSuccess() {
        isPlayingAudio = true
        
        if self.heartButton.isHidden{
            self.tappedOnScreenHandling()
        }
        DispatchQueue.main.async {
            let image = #imageLiteral(resourceName: "icon_pause")
            self.playButton.setImage(image, for: .normal)
        }
    }
    
    func urlToPlay() -> URL {
        DispatchQueue.main.async {
            self.sliderView.minimumValue = 0
            self.sliderView.maximumValue = Float(self.audioVisualizationView.player?.duration ?? 0.0)
            self.runTrackUpdater()
        }
        debugPrint(fileUrl)
        if isGoBack == true{
//            fileUrl = ""
        }
        
        return URL(string: fileUrl)!
    }
    func lineWidth() -> CGFloat {
        return 2
    }
    func lineSeperation() -> CGFloat {
        return 1
    }
}
