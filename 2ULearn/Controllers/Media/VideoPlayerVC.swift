//
//  VideoPlayerVC.swift
//  2ULearn
//
//  Created by NoumanGul on 5/13/19.
//  Copyright © 2019 CMolds. All rights reserved.
//

import UIKit
import MarqueeLabel
import Player
import AVFoundation
import CoreMedia
import Alamofire

extension VideoPlayerVC: PlayerPlaybackDelegate, PlayerDelegate {
    func playerReady(_ player: Player) {
        debugPrint("player is ready")
        DispatchQueue.main.async {
            let image = #imageLiteral(resourceName: "icon_pause")
            self.playButton.setImage(image, for: .normal)

            self.sliderView.minimumValue = 0
            self.sliderView.maximumValue = Float(player.maximumDuration.rounded())
        }
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
        
        debugPrint("player state did change")
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        debugPrint("player buffering did change")
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        debugPrint("player buffer time did change")
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        self.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
        debugPrint("player error: \(error?.localizedDescription ?? "")")
    }
    
    
    public func playerPlaybackWillStartFromBeginning(_ player: Player) {
        debugPrint("player playbackWill Begin")
    }
    
    public func playerPlaybackDidEnd(_ player: Player) {
        debugPrint("player playBack end")
        DispatchQueue.main.async {
            self.playButton.setImage(#imageLiteral(resourceName: "icon_play"), for: .normal)
        }
        
    }
    
    public func playerCurrentTimeDidChange(_ player: Player) {

        self.sliderView.value = Float(player.currentTime.rounded())
        let currentTime = player.currentTime.rounded()
        
        isPlayerPlaying = true
        trackAudio()
        hmsFrom(seconds: Int(currentTime.rounded())) { (hours, minutes, seconds) in
            
            let hours = self.getStringFrom(seconds: hours)
            let minutes = self.getStringFrom(seconds: minutes)
            let seconds = self.getStringFrom(seconds: seconds)
            
            self.startTime.text = "\(hours):\(minutes):\(seconds)"
        }
        let endTimeCount = (self.player.maximumDuration.rounded()) - (self.player.currentTime.rounded())
        hmsFrom(seconds: Int(endTimeCount.rounded())) {  (hours, minutes, seconds) in
            
            let hours = self.getStringFrom(seconds: hours)
            let minutes = self.getStringFrom(seconds: minutes)
            let seconds = self.getStringFrom(seconds: seconds)
            self.endTime.text = "\(hours):\(minutes):\(seconds)"
        }
//        self._playbackViewController?.setProgress(progress: CGFloat(fraction), animated: true)
    }
    
    public func playerPlaybackWillLoop(_ player: Player) {
//        self. _playbackViewController?.reset()
    }
    
}
class VideoPlayerVC: UIViewController, ShowsAlert {

    let colors = [
        DotColors(first: color(0x7DC2F4), second: color(0xE2264D)),
        DotColors(first: color(0xF8CC61), second: color(0x9BDFBA)),
        DotColors(first: color(0xAF90F4), second: color(0x90D1F9)),
        DotColors(first: color(0xE9A966), second: color(0xF8C852)),
        DotColors(first: color(0xF68FA7), second: color(0xF6A2B8))
    ]
    @IBOutlet weak var heartButton: FaveButton!
    var media:mMedia?
    var player = Player()
    @IBOutlet weak var sliderView : UISlider!
    @IBOutlet weak var startTime : UILabel!
    @IBOutlet weak var titleText : UILabel!
    @IBOutlet weak var videoPlayer : UIView!
    
    @IBOutlet weak var marqueeDetails : MarqueeLabel!
    @IBOutlet weak var endTime : UILabel!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var playButton : UIButton!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var viewForTap : UIView!
    
    var dataRequest : DataRequest?
    var isPlayerPlaying = false
    var delegatesForHeartButton : onHeartReloadViewControllerDelegates!
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.keyWindow?.rootViewController as! MainVC).isLeftViewSwipeGestureEnabled = false
        
        loadPlayerData()
        setupForVideoPlayer()
        titleText.text = media?.title ?? ""
        marqueeDetails.text = media?.description ?? ""
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        viewForTap.addGestureRecognizer(tap)
        tappedOnScreenHandling()
    }
    func loadPlayerData(){
        
        let isSelected = self.media?.is_favourite == 1 ? true:false
        self.heartButton?.setSelected(selected: isSelected, animated: false)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        tappedOnScreenHandling()
    }
    func tappedOnScreenHandling(){
        topTitle.isHidden = !topTitle.isHidden
        playButton.isHidden = !playButton.isHidden
        backButton.isHidden = !backButton.isHidden
        heartButton.isHidden = !heartButton.isHidden
        sliderView.isHidden = !sliderView.isHidden
        startTime.isHidden = !startTime.isHidden
     //   marqueeDetails.isHidden = !marqueeDetails.isHidden
        endTime.isHidden = !endTime.isHidden
     //   titleText.isHidden = !titleText.isHidden
    }
    
    @IBAction func backTapped(){
        self.player.stop()
        isPlayerPlaying = false
        (UIApplication.shared.keyWindow?.rootViewController as! MainVC).isLeftViewSwipeGestureEnabled = true
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupForVideoPlayer(){
        
        self.player.view.frame = self.videoPlayer.bounds
        self.addChildViewController(self.player)
        self.videoPlayer.addSubview(self.player.view)
        self.player.didMove(toParentViewController: self)
        self.player.playerDelegate = self
        self.player.playbackDelegate = self
//        let audioURL = URL.init(string: "http://www.ebookfrenzy.com/ios_book/movie/movie.mov")!
        let audioURL = URL.init(string: media?.media ?? "")!
        self.player.url = audioURL
        self.player.playFromBeginning()
        self.player.fillMode = PlayerFillMode.resizeAspectFill.self

        
    }
 
    func markOrRemoveFromFavorite(isSelected:Bool){
        //        dataRequest?.cancel()
        let requestPath = isSelected == true ?Media.addFav.rawValue:Media.removFav.rawValue
        let params = ["content_id":media?.id ?? ""] as [String : AnyObject]
        // Show loading on api call
//        self.hideLoaderView()
//        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        dataRequest = ApiHelper.postRequest(params, kPath: requestPath) { [weak self] (response, error) in
            //hide loader on response
//            self?.hideLoaderView()
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                self?.loadPlayerData()
                return
            }
            self?.media?.is_favourite = isSelected == true ? 1:0
            self?.delegatesForHeartButton.onHeardReloadViewData()
            self?.loadPlayerData()
        }
    }
    @IBAction func scrubAudioChange(sender: UISlider) {
        self.player.pause()
        
        let maxDuration = CMTime(seconds: Double(sender.value), preferredTimescale: 1)
        
        self.player.seek(to: maxDuration) { (isDone) in
            if self.isPlayerPlaying{
                self.player.playFromCurrentTime()
            }
        }
        
    }
    
    @IBAction func audioPlayerTapped(sender:UIButton)
    {
        if self.player.isPlayerPlaying() == .playing{
            isPlayerPlaying = false
            self.player.pause()
            DispatchQueue.main.async {
                let image = #imageLiteral(resourceName: "icon_play")
                sender.setImage(image, for: .normal)
            }
        }else if self.player.isPlayerPlaying() == .paused{
         
            isPlayerPlaying = true
            self.player.playFromCurrentTime()
            DispatchQueue.main.async {
                let image = #imageLiteral(resourceName: "icon_pause")
                sender.setImage(image, for: .normal)
            }
        }
        
        
//        if isPlayerPlaying{
//
//            isPlayerPlaying = false
//            self.player.pause()
//            let image = #imageLiteral(resourceName: "icon_play")
//            sender.setImage(image, for: .normal)
//        }else if !isPlayerPlaying{
//            isPlayerPlaying = true
//            let image = #imageLiteral(resourceName: "icon_pause")
//            sender.setImage(image, for: .normal)
//            self.player.playFromCurrentTime()
//        }
    }
    
    @objc func trackAudio() {
        if isPlayerPlaying {
//            let image = #imageLiteral(resourceName: "icon_pause")
//            playButton.setImage(image, for: .normal)
        }
    }
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
    
    @IBAction func heartbuttonTapped(){
        heartButton.isUserInteractionEnabled = false
        dataRequest?.cancel()
        
    }
}

extension VideoPlayerVC:FaveButtonDelegate{
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        heartButton.isUserInteractionEnabled = true
        markOrRemoveFromFavorite(isSelected: selected)
    }
    
    func faveButtonDotColors(_ faveButton: FaveButton) -> [DotColors]?{
        if( faveButton === heartButton){
            return colors
        }
        return nil
    }
}
