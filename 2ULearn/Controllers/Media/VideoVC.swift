//
//  VideoVC.swift
//  2ULearn
//
//  Created by NoumanGul on 5/7/19.
//  Copyright © 2019 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper
import ESPullToRefresh
import DZNEmptyDataSet
import Alamofire

class VideoVC: UIViewController,ShowsAlert {
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var userPoints: UILabel!

    var dataRequest : DataRequest?
    var refreshControl = UIRefreshControl()
    var page:Int = 1
    var audioListing = [mMedia]()
    var initiallyViewLoaded = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: audioListingTVC.NibName, bundle: nil), forCellReuseIdentifier: audioListingTVC.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        
        addPullToRefreshHandler()
        addLoadMoreHandler()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DataHandler.sharedInstance.getPoints { (points) in
            self.userPoints.text = "\(points)"
        }
        if initiallyViewLoaded{
            initiallyViewLoaded = false
            ApiAudioListing(isRefreshing : true, isLoadingMore : false, page : 1, showLoaderView: !initiallyViewLoaded)
        }else{
            ApiAudioListing(isRefreshing : true, isLoadingMore : false, page : 1, showLoaderView: false)
        }
        
    }
    
    
    
    //MARK:- My Helper Methods
    func addPullToRefreshHandler() {
        self.tableView.es.addPullToRefresh {
            [weak self] in
            self?.tableView.es.resetNoMoreData()
            self?.ApiAudioListing(isRefreshing : true, isLoadingMore : false, page : 1, showLoaderView: false)
        }
    }
    
    func addLoadMoreHandler() {
        self.tableView.es.addInfiniteScrolling {
            [weak self] in
            self?.ApiAudioListing(isRefreshing : false, isLoadingMore : true, page : self?.page ?? 1, showLoaderView: false)
        }
    }
    
    func animateTable(tblviewMenuOfResturant: UITableView)
    {
        tblviewMenuOfResturant.reloadData()
        let cells = tblviewMenuOfResturant.visibleCells
        let tableHeight: CGFloat = tblviewMenuOfResturant.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.0, delay: 0.07 * Double(index), options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    func ApiAudioListing(isRefreshing: Bool, isLoadingMore: Bool, page: Int, showLoaderView:Bool){
        // Parametters for login api
        let params = ["page":page] as [String : AnyObject]
        // Show loading on api call
        if showLoaderView{
            self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        }
        let _ = ApiHelper.getRequest(params, kPath: Media.videoListing.rawValue) { [weak self] (response, error) in
            //hide loader on response
            self?.hideLoaderView()
            self?.tableView.emptyDataSetSource = self
            self?.tableView.emptyDataSetDelegate = self
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                self?.tableView.es.stopPullToRefresh(ignoreDate: true)
                self?.tableView.es.noticeNoMoreData()
//                self?.tableView.reloadData()
            }
            if  response != nil {
                debugPrint(response!)
                _ = isRefreshing ? self?.tableView.es.stopPullToRefresh(ignoreDate: true) : nil
                _ = isRefreshing ? self?.tableView.es.stopPullToRefresh(ignoreDate: true, ignoreFooter: false) : nil
                _ = isLoadingMore ? self?.tableView.es.stopLoadingMore() : nil
                
                let mdData = Mapper<MediaModel>().map(JSON: ((response as! [String:AnyObject])["data"] as! [String:AnyObject]))
                
                let pagination = mdData?.pagination
                self?.page = (pagination?.last ?? 1) == (pagination?.current ?? 1) ? 1 : (pagination?.last ?? 1)
                _ = self?.page == 1 ? self?.tableView.es.noticeNoMoreData() : nil
                
                
                
                if(mdData != nil && mdData?.data != nil)
                {
                    if !isRefreshing {
                        self?.audioListing += mdData?.data ?? []
                    }else{
                        self?.audioListing = mdData?.data ?? []
                    }
                }
                
                if showLoaderView{
                    self?.animateTable(tblviewMenuOfResturant:(self?.tableView)!)
                }else{
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func markOrRemoveFromFavorite(id:String, isSelected:Bool, index: Int){
        //        dataRequest?.cancel()
        let requestPath = isSelected == true ?Media.addFav.rawValue:Media.removFav.rawValue
        let params = ["content_id":id] as [String : AnyObject]
        // Show loading on api call
//        self.hideLoaderView()
//        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        dataRequest = ApiHelper.postRequest(params, kPath: requestPath) { [weak self] (response, error) in
            //hide loader on response
//            self?.hideLoaderView()
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                self?.tableView.reloadData()
                return
            }
            
            self?.audioListing[index].is_favourite = isSelected == true ? 1:0
            self?.tableView.reloadData()
//            if  response != nil {
////                self?.ApiAudioListing(isRefreshing : false, isLoadingMore : true, page : 1)
//            }
        }
        
    }
}


extension VideoVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.audioListing.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: audioListingTVC.reuseIdentifier, for: indexPath) as! audioListingTVC
        cell.setData(dataMedia: self.audioListing[indexPath.row])
        cell.heartButton.tag = indexPath.row
        cell.heartDelegate = self
        cell.setImageFromUrl()
        cell.play_icon.isHidden = false
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {   
        let vc = UIStoryboard.mediaStoryboard().instantiateViewController(withIdentifier:     MediaController.VideoPlayerVC.rawValue) as! VideoPlayerVC
        vc.media = self.audioListing[indexPath.row]
        vc.delegatesForHeartButton = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension VideoVC:onHeartReloadViewControllerDelegates{
    func onHeardReloadViewData() {
        debugPrint("ReloadViewNow")
        ApiAudioListing(isRefreshing : true, isLoadingMore : false, page : 1, showLoaderView: false)
    }
}

extension VideoVC:heartButtonTappedDelegates{
    func heartTappedWithId(MediaID: String, isSelected: Bool, index: Int) {
        self.markOrRemoveFromFavorite(id: MediaID, isSelected: isSelected, index: index)
    }
}

extension VideoVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!
    {
        let img = UIImage(named:"Placeholders_record")
        return img
    }
    
}
