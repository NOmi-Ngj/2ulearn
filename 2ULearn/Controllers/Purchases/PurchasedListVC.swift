//
//  PurchasedListVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class PurchasedListVC: UIViewController ,ShowsAlert{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var cartCountLable: UILabel!
    @IBOutlet weak var viewCartCount: UIView!

    //MARK:- Helper Variables
    var purchaseList = [PurchaseList]()
    override func viewDidLoad() {
        super.viewDidLoad()

        applicationFontsSetting()
        tableViewRegisterDelegateandDataSource()
        
        
        DispatchQueue.main.async {
            self.viewCartCount.cornerRadiuss = self.viewCartCount.frame.size.height/2
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.cartIconSetting()
        loadPurchasesListApi()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cartIconSetting(){
        self.viewCartCount.isHidden = DataHandler.sharedInstance.cartList.count > 0 ? false:true
        self.cartCountLable.text = "\(DataHandler.sharedInstance.cartList.count)"
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: BooksPurchaseDetailsTVC.NibName , bundle: nil), forCellReuseIdentifier: BooksPurchaseDetailsTVC.reuseIdentifier)
    }
    
    @IBAction func getMyCarts(_ sender:Any){
        
        let purchasedListVC = UIStoryboard.purchasesStoryboard().instantiateViewController(withIdentifier: PurchasesControllers.MyCartsVC.rawValue) as! MyCartsVC
        self.navigationController?.pushViewController(purchasedListVC, animated: true)
    }
    @IBAction func onPress_PurchasedList(segue:UIStoryboardSegue) {
        
    }
    func loadPurchasesListApi(){
        // Parametters for login api
        let params =   [String : AnyObject]()
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.allPurchases.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            if  response != nil {
                debugPrint(response!)
                if !((response as! [String:AnyObject])[ResponseKey.data.rawValue] is NSNull){
                self?.purchaseList = Mapper<PurchaseList>().mapArray(JSONArray: (response![ResponseKey.data.rawValue] as? [[String : AnyObject]])!) ?? []
                self?.tableView.reloadData()
                }
            }
        }
    }

}

extension PurchasedListVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return purchaseList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BooksPurchaseDetailsTVC.reuseIdentifier, for: indexPath) as! BooksPurchaseDetailsTVC
        cell.selectionStyle = .none
        let name = purchaseList[indexPath.row].book_details?.name ?? ""
        let lorem = purchaseList[indexPath.row].book_details?.descriptions ?? ""
        let price = purchaseList[indexPath.row].book_details?.price ?? 0
        let url = purchaseList[indexPath.row].book_details?.image_url?.scaledImageURL() ?? ""
        cell.setData(title: name, details: lorem, price: "$\(price)", imageUrl: url)

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let textBookDetailsVC = UIStoryboard.leftMenuItemsStoryboard().instantiateViewController(withIdentifier: LeftMenuControllers.TextBookDetailsVC.rawValue) as! TextBookDetailsVC
        textBookDetailsVC.str_titleText = purchaseList[indexPath.row].book_details?.name ?? ""
        textBookDetailsVC.bookDetail = purchaseList[indexPath.row].book_details
        textBookDetailsVC.isFromPurchases = true
        self.navigationController?.pushViewController(textBookDetailsVC, animated: true)
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension PurchasedListVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_purchase")
    }
}
