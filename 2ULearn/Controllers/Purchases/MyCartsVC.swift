//
//  MyCartsVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class MyCartsVC: UIViewController ,ShowsAlert{
    
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var totalCartAmout: UILabel!
     var createConfirmationPopup : ConfirmationAlertView?
    @IBOutlet weak var tableView: UITableView!
    var isFromPurchased:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        totalAmountUpdate()
        tableViewRegisterDelegateandDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: BooksPurchaseDetailsTVC.NibName , bundle: nil), forCellReuseIdentifier: BooksPurchaseDetailsTVC.reuseIdentifier)
    }
    
    func totalAmountUpdate(){
        let totalSum = (DataHandler.sharedInstance.cartList.map({$0.price})).reduce(0, {x,y in x + y!})
        totalCartAmout.text = "$\(totalSum)"
     
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    } 
    @IBAction func onPress_Proceed(_ sender: UIButton) {
         if DataHandler.sharedInstance.cartList.count > 0 {
            let checkOutVC = UIStoryboard.purchasesStoryboard().instantiateViewController(withIdentifier: PurchasesControllers.CheckOutVC.rawValue) as! CheckOutVC
            checkOutVC.isFromPurchased = isFromPurchased
            let totalSum = (DataHandler.sharedInstance.cartList.map({$0.price})).reduce(0, {x,y in x + y!})
            
            checkOutVC.price = totalSum 
            checkOutVC.booksId = []
            self.navigationController?.pushViewController(checkOutVC, animated: true)
         }else{
            validationAlertView(title: "Alert", message: "Add Cart to continue", okButtonText: "OK")
        }
    }
    
}
extension MyCartsVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataHandler.sharedInstance.cartList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BooksPurchaseDetailsTVC.reuseIdentifier, for: indexPath) as! BooksPurchaseDetailsTVC
        cell.selectionStyle = .none
        cell.setViewForCarts()
        let name = DataHandler.sharedInstance.cartList[indexPath.row].name ?? ""
        let lorem = DataHandler.sharedInstance.cartList[indexPath.row].descriptions ?? ""
        let price = DataHandler.sharedInstance.cartList[indexPath.row].price ?? 0
        let url = DataHandler.sharedInstance.cartList[indexPath.row].image_url?.scaledImageURL() ?? ""
        
        cell.setData(title: name, details: lorem, price: "$\(price)", imageUrl: url)
        
        cell.deleteImageView.tag = indexPath.row
        let tapGuestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(deleteTapped(tapGestureRecognizer:)))
        cell.deleteImageView.isUserInteractionEnabled = true
        cell.deleteImageView.addGestureRecognizer(tapGuestureRecognizer)
        
        return cell
    }
    
    @objc func deleteTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let imageView = tapGestureRecognizer.view as! UIImageView
        createConfirmationPopup = ConfirmationAlertView.instanceFromNib()
        createConfirmationPopup?.YesBtn.tag = imageView.tag
        createConfirmationPopup?.animator = FromTopTranslationAnimator()
        createConfirmationPopup?.delegate = self
        createConfirmationPopup?.show()
    }
}
extension MyCartsVC : customConfirmationAlertViewDelegate {
    func okButtonPressed(index: Int) {
        createConfirmationPopup?.dismiss(true)
        DataHandler.sharedInstance.cartList.remove(at: index)
        self.tableView.reloadData()
        totalAmountUpdate()
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension MyCartsVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
