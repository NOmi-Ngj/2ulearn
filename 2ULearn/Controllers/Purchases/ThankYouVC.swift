//
//  ThankYouVC.swift
//  2ULearn
//
//  Created by mac on 6/27/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class ThankYouVC: UIViewController {

    var isFromPurchased:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onPress_ThankYou(_ sender: Any) {
        if isFromPurchased{
            self.performSegue(withIdentifier: "purchasedHit", sender: self)
        }else{
            self.performSegue(withIdentifier: "textbookHit", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
