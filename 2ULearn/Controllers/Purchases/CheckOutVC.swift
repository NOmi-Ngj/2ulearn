//
//  CheckOutVC.swift
//  2ULearn
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import ObjectMapper

class CheckOutVC: UIViewController,ShowsAlert{

    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet weak var fullName: UITextField!
    
    @IBOutlet weak var PhoneNo: UITextField!
    
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var uAddress: UITextField!
    var isFromPurchased:Bool = true
    var price:Int = 0
    var booksId:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        applicationFontsSetting()
        totalPrice.text = "$\(price)"
        if Common.isTesting() {
            uAddress.text = "asda ad"
            PhoneNo.text = "065150150"
            fullName.text = "asdasd adasd"
            city.text = "adsada"
            country.text = "adada sa"
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
        
    }
    @IBAction func onPress_Checkout(_ sender: Any) {
        apiForCheckOut()

        
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    }
    
    func apiForCheckOut(){
        let booksId = DataHandler.sharedInstance.cartList.map({$0.id})
        
        // Parametters for login api
        let params = [ "items":booksId,
                       "full_name":fullName.text ?? "",
                       "address":uAddress.text ?? "",
                       "phone_no":PhoneNo.text ?? "",
                       "country":country.text ?? "",
                       "city":city.text ?? "" ] as! [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.postRequest(params, kPath: Users.placeOrder.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            // handle response
            if  response != nil {
                debugPrint(response!)
                let thankYouVC = UIStoryboard.purchasesStoryboard().instantiateViewController(withIdentifier: PurchasesControllers.ThankYouVC.rawValue) as! ThankYouVC
                thankYouVC.isFromPurchased = self?.isFromPurchased ?? false
                DataHandler.sharedInstance.cartList = []
                self?.navigationController?.pushViewController(thankYouVC, animated: true)
                
            }
        }
        }
}
extension CheckOutVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
