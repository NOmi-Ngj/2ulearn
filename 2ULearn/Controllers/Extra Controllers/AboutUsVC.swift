//
//  AboutUsVC.swift
//  2ULearn
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import WebKit

class AboutUsVC: UIViewController, ShowsAlert {
    
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var userPoints: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textView: UITextView!
    
    var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applicationFontsSetting()
        DataHandler.sharedInstance.getPoints { (points) in
            self.userPoints.text = "\(points)"
        }
        // Do any additional setup after loading the view.
        setupWebView()
        getHTMLString()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
    }
    
    func setupWebView() {
        DispatchQueue.main.async {
            self.webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.height))
            self.webView.scrollView.delegate = self
//            self.containerView.addSubview(self.webView)
        }
    }
    
    func getHTMLString() {
        showLoaderView(isCenter: true , UserInterfaceEnable: false)
        
        _ = ApiHelper.getRequest(nil, kPath: Users.aboutUs.rawValue, completion: {
            [weak self] (responseData, error) in
            
            self?.hideLoaderView()
            
            guard error == nil else {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
                return
            }
            
            let htmlContent = (((responseData as! [String : AnyObject])["data"] as! NSArray)[0] as! [String : AnyObject])["data"] as! String
            self?.textView.attributedText = htmlContent.htmlToAttributedString
//            self?.webView.loadHTMLString(htmlContent, baseURL: nil)
            
        })
    }


}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension AboutUsVC: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
        scrollView.panGestureRecognizer.isEnabled = false
    }
}
