//
//  LessonsResultsVC.swift
//  2ULearn
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import YYWebImage
class LessonsResultsVC: UIViewController,ShowsAlert {

    @IBOutlet weak var totalvocabulary: UILabel!
    @IBOutlet weak var totalgrammar: UILabel!
    @IBOutlet weak var totalword: UILabel!
    @IBOutlet weak var totalcharacters: UILabel!
    @IBOutlet weak var totalPoint: UILabel!
    @IBOutlet weak var lessonNo: UILabel!
    @IBOutlet weak var lessonTitle: UILabel!
    @IBOutlet weak var lessonDetails: UILabel!
    @IBOutlet weak var lessonImage: UIImageView!
    
    var isCorrect:[String]? = []
    var answerID:[String]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
    }
    
    func loadData(){
        DataHandler.sharedInstance.lessonsPagerItemsController.forEach { (isCorrect) in
            self.isCorrect?.append("\(isCorrect==true ? "1":"0")")
        }
        DataHandler.sharedInstance.lessonsPagerItemsId.forEach { (answerID) in
            self.answerID?.append(answerID)
        }
        //'picture','single','word','multiple'
        
//        "sentences": 0,"grammar": 0,"word": 1,"characters": 1,
        let sentences = DataHandler.sharedInstance.questionsList.filter({$0.nature == "sentences"})
//        map({$0.type == "picture"})
        let grammar = DataHandler.sharedInstance.questionsList.filter({$0.nature == "grammar"})
        let word = DataHandler.sharedInstance.questionsList.filter({$0.nature == "word"})
        let characters = DataHandler.sharedInstance.questionsList.filter({$0.nature == "characters"})
        
        let answerSentences = DataHandler.sharedInstance.lessonType.filter({$0.type == "sentences"})
        let answerGrammar = DataHandler.sharedInstance.lessonType.filter({$0.type == "grammar"})
        let answerWord = DataHandler.sharedInstance.lessonType.filter({$0.type == "word"})
        let answerCharacters = DataHandler.sharedInstance.lessonType.filter({$0.type == "characters"})
        
        let totalPoints = (DataHandler.sharedInstance.lessonType.map({$0.value})).reduce(0, +)
//        reduce(0, {x,y in x + y!})
        
        debugPrint(totalPoints)
//        totalPoint.text = "\(totalPoints)"
//        filter({$0.type == "multiple"})
        
        
        self.totalword.text = "\(answerWord.count)/\(word.count)"
        self.totalgrammar.text = "\(answerGrammar.count)/\(grammar.count)"
        self.totalcharacters.text = "\(answerCharacters.count)/\(characters.count)"
        self.totalvocabulary.text = "\(answerSentences.count)/\(sentences.count)"
        
        let currentIndex = DataHandler.sharedInstance.lessonsSelected
        let totalLessons = DataHandler.sharedInstance.lessonsTotal
        self.lessonNo.text = "Lesson \(currentIndex)/\(totalLessons)"
//        "Lesson 1/3"
        self.lessonTitle.text = DataHandler.sharedInstance.lessonsName
        self.lessonDetails.text = DataHandler.sharedInstance.lessonsDetails
        self.lessonImage.yy_setImage(with: URL(string: DataHandler.sharedInstance.lessonsImageUrl), placeholder:#imageLiteral(resourceName: "empty_Logo"))
        DispatchQueue.main.async {
            self.lessonImage.cornerRadiuss = self.lessonImage.frame.size.height/2
        }
 
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func onPress_XClose(){
        let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "closeX1":"closeX"
        self.performSegue(withIdentifier: identifier, sender: self)
    }

    @IBAction func onPress_Next(){
        apiForSubmittingUserAnswers()
    }

    @IBAction func onPress_Retry(){
        let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "Retry1":"Retry"
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    func apiForSubmittingUserAnswers(){

        // Parametters for login api
        let params = [ "user_answer":answerID, "is_correct":isCorrect] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.postRequest(params, kPath: Lessons.submitUserAnswers.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            // handle response
            if  response != nil {
                debugPrint(response!)
                let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "Next1":"Next"
                self?.performSegue(withIdentifier: identifier, sender: self)
                
            }
        }
    }


}
