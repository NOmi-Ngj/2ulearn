//
//  LessonWithSentancesVC.swift
//  2ULearn
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit

class LessonWithSentancesVC: UIViewController {

    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var questionTextLbl: UILabel!
    @IBOutlet weak var scoreButton: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    @IBOutlet weak var topCheckedImageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var collectionViewItemsController: UICollectionView!
    
    var nextLessonPopUp = WordsRightWrongPopUp()
    var indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let attributedString = NSMutableAttributedString(string: "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedStringKey, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 20)]
        attributedString.addAttributes(attributes, range: NSMakeRange(0, attributedString.length))
        questionTextLbl.attributedText = attributedString
        
//        questionTextLbl.text = "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")"
        titleText.text = DataHandler.sharedInstance.lessonsName
        textView.delegate = self
       
        let data = DataHandler.sharedInstance.lessonsPagerItemsController.filter { (isTrue) -> Bool in
            return isTrue == true
        }
        debugPrint(data)
        let points = data.count * DataHandler.sharedInstance.questionsList[indexQuestion].points
        btnPoints.setTitle("\(points) Score", for: .normal)
        
        applicationFontsSetting()
        setImageGuestures() 
        collectionViewItemsController.delegate = self
        collectionViewItemsController.dataSource = self
        collectionViewItemsController.register(UINib(nibName: ItemControllersCVC.NibName, bundle: nil), forCellWithReuseIdentifier: ItemControllersCVC.reuseIdentifier)
        if Common.isTesting() == true{
            textView.text = "一贯"
            topCheckedImageView.isHidden = false
            textView.textColor = UIColor.appRightBackgroundColor()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
        scoreButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
    }
    func showNextLessonImageView(){
        nextLessonPopUp = WordsRightWrongPopUp.instanceFromNib()!
        nextLessonPopUp.animator = FromTopTranslationAnimator()
        
        let rightAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers?.filter({ $0.is_correct == 1 })
        debugPrint(DataHandler.sharedInstance.questionsList[indexQuestion].answers ?? [])
        debugPrint(DataHandler.sharedInstance.questionsList[indexQuestion].answers?.filter({ $0.is_correct == 1 }))
        
        
        var answer:String?
        if rightAnswer?.count ?? 0 > 0 {
            answer = rightAnswer![0].answer ?? ""
        }
        let myAnswer = rightAnswer?.filter({$0.answer == self.textView.text ?? ""})
//        let isCorrect = myAnswer?.count ?? 0 > 0 ? true:false
        answer = myAnswer?.count ?? 0 > 0 ? myAnswer![0].answer:answer
        nextLessonPopUp.lessonLabel.text = "\(self.questionTextLbl.text ?? "") = \(self.textView.text ?? "")"
        nextLessonPopUp.wrongRightLabel.text = textView.text.lowercased() == answer?.lowercased() ? "Right":"Wrong"
        nextLessonPopUp.nextButton.backgroundColor = textView.text.lowercased() == answer?.lowercased() ? .appRightBackgroundColor(): .appWrongBackgroundColor()
        nextLessonPopUp.show()
        nextLessonPopUp.delegate = self
        
        let bool = textView.text.lowercased() == answer?.lowercased() ? true:false
        bool == true ?DataHandler.sharedInstance.lessonType.append((type: DataHandler.sharedInstance.questionsList[indexQuestion].nature ?? "", value: 1)):nil
        DataHandler.sharedInstance.lessonsPagerItemsController.append(bool)
        if rightAnswer?.count ?? 0 > 0 {
            DataHandler.sharedInstance.lessonsPagerItemsId.append(rightAnswer![0].id ?? "")
        }
        
    }
    
    //MARK:- Images Guesture On Tap
    func setImageGuestures(){
        
        topCheckedImageView.isHidden = true
        let selectedCheckImageViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showSelectionCheckImageTapped(tapGestureRecognizer:)))
        topCheckedImageView.isUserInteractionEnabled = true
        topCheckedImageView.addGestureRecognizer(selectedCheckImageViewGestureRecognizer)
        
    }
    //MARK:- Add Statement Tapped
    @objc func showSelectionCheckImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if textView.text == "Write Down The Translation" || textView.text == "" {
            return
        }
        showNextLessonImageView()
    }
    @IBAction func onPress_XClose(){
        let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "closeX1":"closeX"
        self.performSegue(withIdentifier: identifier, sender: self)
    }
}
extension LessonWithSentancesVC: LessonImagesSelectionCVCDelegates{
    func onPressNext() {
        DataHandler.sharedInstance.lessonsPagerIndex += 1
        let indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
        if indexQuestion < DataHandler.sharedInstance.lessonsPagerCount{
            let type = DataHandler.sharedInstance.questionsList[indexQuestion].type ?? ""
            Helper.showQuestionTypeViewController(type: type, navigation: self.navigationController!)
        }else{
            Helper.showQuestionTypeViewController(type: "result", navigation: self.navigationController!)
        }
        nextLessonPopUp.dismiss(true)
    }
}
//MARK:- Textview Delegates methods
extension LessonWithSentancesVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write Down The Translation" {
            textView.text = nil
            topCheckedImageView.isHidden = false
            textView.textColor = UIColor.appRightBackgroundColor()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write Down The Translation"
            textView.textColor = UIColor.black
            topCheckedImageView.isHidden = true
        }
    }
}

extension LessonWithSentancesVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataHandler.sharedInstance.lessonsPagerCount
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemControllersCVC.reuseIdentifier, for: indexPath) as! ItemControllersCVC
        cell.itemImage.image = #imageLiteral(resourceName: "checkunselected")
        if DataHandler.sharedInstance.lessonsPagerIndex > indexPath.row{
            cell.itemImage.image = DataHandler.sharedInstance.lessonsPagerItemsController[indexPath.row] == true ? #imageLiteral(resourceName: "checkselected"):#imageLiteral(resourceName: "Cross")
        }
        return cell
    }
}

extension LessonWithSentancesVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = (collectionView.frame.size.height)
        return CGSize(width:  height , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}
