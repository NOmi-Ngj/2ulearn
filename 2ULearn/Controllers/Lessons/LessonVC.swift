//
//  LessonVC.swift
//  2ULearn
//
//  Created by mac on 6/14/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import FSPagerView
import ObjectMapper
import DZNEmptyDataSet

//private var numberOfCards: Int = 5

class LessonVC: UIViewController ,ShowsAlert {

    @IBOutlet weak var lessonNumberLable: UILabel!
    @IBOutlet weak var categoryTitleLable: UILabel!
    @IBOutlet weak var categoryDetailsLable: UILabel!
    @IBOutlet weak var startLearningButton: UIButton!
    @IBOutlet weak var Placeholders_record: UIImageView!
    @IBOutlet weak var viewReview: UIView!
    @IBOutlet weak var reviewLable: UILabel!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.isInfinite = false
            self.pagerView.interitemSpacing = 1.0
            self.pagerView.transformer = FSPagerViewTransformer(type: .overlap)
            
        }
    }
    var selectedLearningIndex:Int = 0
    var catLessontitle = ""
    var catLessonDetails = ""
    var catID = ""
    var lessonsArr = [LessonsList]()
    var isNextQuestion = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
//        pagerView.emptyDataSetSource = self
//        pagerView.emptyDataSetDelegate = self
        pagerView.register(UINib(nibName: CandidateView.NibName, bundle: nil), forCellWithReuseIdentifier: CandidateView.reuseIdentifier)
        DispatchQueue.main.async {
            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: 0.75, y: 0.8))
        }
        applicationFontsSetting()
        lessonsArr.count > 0 ?nil:loadLessonsListApi()
    }
    
    @IBAction func onPress_NextLesson(segue:UIStoryboardSegue) {
        isNextQuestion = true
        loadLessonsListApi()
    }
    @IBAction func onPress_RetryLesson(segue:UIStoryboardSegue) {
 
    }
    @IBAction func onPress_Xclose(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func onPress_StartLearning(_ sender: Any) {
        let vc = UIStoryboard.lessonsStoryboard().instantiateViewController(withIdentifier: LessonsControllers.LessonWithImagesVC.rawValue) as! LessonWithImagesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onPress_Back(_ sender: Any) {
        goBack()
    }
    func goBack(){
        _ = navigationController?.popViewController(animated: true)
    } 
    func applicationFontsSetting(){
        lessonNumberLable.font = UIFont.appMontFonts(size: 17)
        categoryTitleLable.font = UIFont.appRegularFonts(size: 19)
        categoryDetailsLable.font = UIFont.appMontFonts(size: 15)
        startLearningButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
        reviewLable.font = UIFont.appMontFonts(size: 15)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadLessonsListApi(){
        // Parametters for login api
        let params = [ "category_id":catID ] as [String : AnyObject]
        
        // Show loading on api call
        self.showLoaderView(isCenter: true , UserInterfaceEnable: false)
        let _ = ApiHelper.getRequest(params, kPath: Lessons.allLessons.rawValue) { [weak self] (response, error) in
            
            //hide loader on response
            self?.hideLoaderView()
            
            // handle error
            if  error != nil {
                self?.validationAlertView(title: "Alert", message: (error?.localizedDescription)!, okButtonText: "OK")
            }
            // handle response
            if  response != nil {
                debugPrint(response!)
                self?.lessonsArr = Mapper<LessonsList>().mapArray(JSONArray: response![ResponseKey.data.rawValue] as! [[String : AnyObject]]) ?? []
                self?.pagerView.reloadData()
                if self?.isNextQuestion ?? false{
                    self?.isNextQuestion = false
                    let index = (self?.lessonsArr.count ?? 0) - 1 == self?.selectedLearningIndex ? self?.selectedLearningIndex:(self?.selectedLearningIndex)! + 1
                    self?.pagerView.scrollToItem(at: index!, animated: true)
                }
            }
        }
    }
    
    func showViewAccordingToIndex(index:Int){
        let question = index == 0 ? lessonsArr[index]:lessonsArr[index-1]
        if question.is_attempt == "true" || index == 0{
            if lessonsArr[index].questions?.count ?? 0 != 0 {
                DataHandler.sharedInstance.lessonsSelected = index+1
                DataHandler.sharedInstance.lessonsTotal = (lessonsArr.count ?? 0)
                DataHandler.sharedInstance.lessonsPagerItemsController = []
                DataHandler.sharedInstance.lessonsPagerItemsId = []
                DataHandler.sharedInstance.lessonsPagerIndex = 0
                DataHandler.sharedInstance.lessonsPagerCount = lessonsArr[index].questions?.count ?? 0
                selectedLearningIndex = index
                DataHandler.sharedInstance.lessonsName = lessonsArr[index].name ?? ""
                DataHandler.sharedInstance.lessonsImageUrl = lessonsArr[index].image_url?.scaledImageURL() ?? ""
                DataHandler.sharedInstance.lessonsDetails = lessonsArr[index].descriptions ?? ""
                DataHandler.sharedInstance.lessonType = []
                DataHandler.sharedInstance.lessonsSelectionType = 0
                DataHandler.sharedInstance.questionsList = lessonsArr[index].questions ?? []
                Helper.showQuestionTypeViewController(type: DataHandler.sharedInstance.questionsList[0].type ?? "", navigation: self.navigationController!)
            }
        }else{
            self.showAlertAndDisappear(title: "2ULearn", message: "Please complete previous lesson to start this lesson", completionHandler: { (isRemoved) in
                if isRemoved{
                    
                }
            })
            debugPrint("can't attem[ this question")
        }
    }
}

// MARK:- FSPagerViewDataSource and FSPagerViewDelegate Methods
extension LessonVC : FSPagerViewDataSource, FSPagerViewDelegate {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        Placeholders_record.isHidden = lessonsArr.count == 0 ?false:true
        return lessonsArr.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: CandidateView.reuseIdentifier, at: index) as! CandidateView
        cell.lessonNo.text = "Lesson \(index+1)/\(lessonsArr.count)"
        cell.lessonTitle.text = lessonsArr[index].name ?? ""
//            catLessontitle.capitalized
        cell.lessonDetail.text = lessonsArr[index].descriptions ?? ""
//            catLessonDetails.capitalized
        let imgUrl = lessonsArr[index].image_url?.scaledImageURL() ?? ""
        cell.lessonImage.yy_setImage(with: URL(string: imgUrl), placeholder:#imageLiteral(resourceName: "empty_Logo"))
        DispatchQueue.main.async {
            cell.lessonImage.cornerRadiuss = cell.lessonImage.frame.size.height/2
        }
        
//        cell.lessonImage.cornerRadius
//            UIImage.init(named: "icon_\(catLessontitle.lowercased())")
        cell.startLearning.tag = index
        cell.delegate = self
//        cell.delegate = self
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.showViewAccordingToIndex(index: index)
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
}

extension LessonVC : startLearningSelectionCVCDelegates{
    func onPressStartLearning(index: Int) {
        self.showViewAccordingToIndex(index: index)
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension LessonVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
