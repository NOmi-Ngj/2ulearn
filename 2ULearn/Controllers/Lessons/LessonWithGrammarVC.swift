//
//  LessonWithGrammarVC.swift
//  2ULearn
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class LessonWithGrammarVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var questionTextLbl: UILabel!
    @IBOutlet weak var btnPoints: UIButton!
    @IBOutlet weak var scoreButton: UIButton!
    @IBOutlet weak var topCheckedImageView: UIImageView!
  
    @IBOutlet weak var collectionViewItemsController: UICollectionView!
    var isSelectedImage = false
    var selectedIndex = 0 
    var nextLessonPopUp = WordsRightWrongPopUp()
    var indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let attributedString = NSMutableAttributedString(string: "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedStringKey, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 20)]
        attributedString.addAttributes(attributes, range: NSMakeRange(0, attributedString.length))
        questionTextLbl.attributedText = attributedString
        
//        questionTextLbl.text = "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")"
        titleText.text = DataHandler.sharedInstance.lessonsName
        
        let data = DataHandler.sharedInstance.lessonsPagerItemsController.filter { (isTrue) -> Bool in
            return isTrue == true
        }
        debugPrint(data)
        let points = data.count * DataHandler.sharedInstance.questionsList[indexQuestion].points
        btnPoints.setTitle("\(points) Score", for: .normal)
        applicationFontsSetting()
        setImageGuestures()
        tableViewRegisterDelegateandDataSource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func applicationFontsSetting(){
       titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
        scoreButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
    }
    func showNextLessonImageView(){
        nextLessonPopUp = WordsRightWrongPopUp.instanceFromNib()!
        nextLessonPopUp.animator = FromTopTranslationAnimator()
        nextLessonPopUp.show()
        let rightAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers?.filter({ $0.is_correct == 1 })
        let selectedAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers![selectedIndex]
        var answer:String?
        if rightAnswer?.count ?? 0 > 0 {
            answer = rightAnswer![0].answer ?? ""
        }
        nextLessonPopUp.lessonLabel.text = "\(self.questionTextLbl.text ?? "") = \(selectedAnswer.answer ?? "")"
        nextLessonPopUp.wrongRightLabel.text = selectedAnswer.is_correct == 1 ?"Right":"Wrong"
        nextLessonPopUp.nextButton.backgroundColor = selectedAnswer.is_correct == 1 ? .appRightBackgroundColor(): .appWrongBackgroundColor()
        nextLessonPopUp.delegate = self
        let bool = selectedAnswer.is_correct == 1 ?true:false
        bool == true ?DataHandler.sharedInstance.lessonType.append((type: DataHandler.sharedInstance.questionsList[indexQuestion].nature ?? "", value: 1)):nil
        DataHandler.sharedInstance.lessonsPagerItemsController.append(bool)
        DataHandler.sharedInstance.lessonsPagerItemsId.append(selectedAnswer.id ?? "")
        
    }
    
    //MARK:- Images Guesture On Tap
    func setImageGuestures(){
        
        topCheckedImageView.isHidden = true
        let selectedCheckImageViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showSelectionCheckImageTapped(tapGestureRecognizer:)))
        topCheckedImageView.isUserInteractionEnabled = true
        topCheckedImageView.addGestureRecognizer(selectedCheckImageViewGestureRecognizer)
        
    }
    //MARK:- Add Statement Tapped
    @objc func showSelectionCheckImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        showNextLessonImageView()
    }
    
    func tableViewRegisterDelegateandDataSource(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.register(UINib(nibName: grammarWordsTVC.NibName , bundle: nil), forCellReuseIdentifier: grammarWordsTVC.reuseIdentifier)
        
        collectionViewItemsController.delegate = self
        collectionViewItemsController.dataSource = self
        collectionViewItemsController.register(UINib(nibName: ItemControllersCVC.NibName, bundle: nil), forCellWithReuseIdentifier: ItemControllersCVC.reuseIdentifier)
    }
     
    @IBAction func onPress_XClose(){
        let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "closeX1":"closeX"
        self.performSegue(withIdentifier: identifier, sender: self)
    }
     
}
extension LessonWithGrammarVC: LessonImagesSelectionCVCDelegates{
    func onPressNext() {
        DataHandler.sharedInstance.lessonsPagerIndex += 1
        let indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
        if indexQuestion < DataHandler.sharedInstance.lessonsPagerCount{
            let type = DataHandler.sharedInstance.questionsList[indexQuestion].type ?? ""
            Helper.showQuestionTypeViewController(type: type, navigation: self.navigationController!)
        }else{
            Helper.showQuestionTypeViewController(type: "result", navigation: self.navigationController!)
        }
        
        nextLessonPopUp.dismiss(true)
    }
}

//MARK:- tableView Datasource and Delegate methods
extension LessonWithGrammarVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataHandler.sharedInstance.questionsList[indexQuestion].answers?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: grammarWordsTVC.reuseIdentifier, for: indexPath) as! grammarWordsTVC
        cell.selectionStyle = .none
        cell.itemTitle.text = DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? ""
        
        if selectedIndex == indexPath.row && isSelectedImage{
            cell.itemSelectionImage.image = #imageLiteral(resourceName: "checkselected")
        }else{
            cell.itemSelectionImage.image = #imageLiteral(resourceName: "checkunselected")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Calling helper method to play sound of answer via google translate
        Helper.playFileFromURL(url: "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=\(DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? "")&tl=zh", selfdelegate: self)
        
        selectedIndex = indexPath.row
        isSelectedImage = true
        topCheckedImageView.isHidden = false
        tableView.reloadData()
    }
    
}
extension LessonWithGrammarVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return DataHandler.sharedInstance.lessonsPagerCount
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemControllersCVC.reuseIdentifier, for: indexPath) as! ItemControllersCVC
            cell.itemImage.image = #imageLiteral(resourceName: "checkunselected")
            if DataHandler.sharedInstance.lessonsPagerIndex > indexPath.row{
                cell.itemImage.image = DataHandler.sharedInstance.lessonsPagerItemsController[indexPath.row] == true ? #imageLiteral(resourceName: "checkselected"):#imageLiteral(resourceName: "Cross")
            }
            return cell  
    }
}

extension LessonWithGrammarVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = (collectionView.frame.size.height)
        return CGSize(width:  height , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension LessonWithGrammarVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
