//
//  LessionWithImagesVC.swift
//  2ULearn
//
//  Created by mac on 6/14/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class LessonWithImagesVC: UIViewController, ShowsAlert {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var questionTextLbl: UILabel!
    @IBOutlet weak var btnPoints: UIButton!
    @IBOutlet weak var scoreButton: UIButton!
    @IBOutlet weak var topCheckedImageView: UIImageView!
    @IBOutlet weak var collectionViewItemsController: UICollectionView!
    
    var isSelectedImage = false
    var selectedIndex = 0
    var nextLessonPopUp = LessonImagesSelectionCVC()
    var indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString = NSMutableAttributedString(string: "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedStringKey, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 20)]
        attributedString.addAttributes(attributes, range: NSMakeRange(0, attributedString.length))
        questionTextLbl.attributedText = attributedString
        
//        questionTextLbl.text = "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")"
        titleText.text = DataHandler.sharedInstance.lessonsName
        let data = DataHandler.sharedInstance.lessonsPagerItemsController.filter { (isTrue) -> Bool in
            return isTrue == true
        }
        debugPrint(data)
        let points = data.count * DataHandler.sharedInstance.questionsList[indexQuestion].points
        btnPoints.setTitle("\(points) Score", for: .normal)
        applicationFontsSetting()
        setImageGuestures()
        collectionviewRegisterDelegateandDataSource()
        // Do any additional setup after loading the view.
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
        scoreButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
    }
    func showNextLessonImageView(){
        nextLessonPopUp = LessonImagesSelectionCVC.instanceFromNib()!
        nextLessonPopUp.animator = FromTopTranslationAnimator()
        nextLessonPopUp.show()
        let rightAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers?.filter({ $0.is_correct == 1 })
        let selectedAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers![selectedIndex]
        var answer:String?
        if rightAnswer?.count ?? 0 > 0 {
            answer = rightAnswer![0].answer ?? ""
        }
        nextLessonPopUp.topImageView.yy_setImage(with: URL(string: selectedAnswer.image_url?.scaledImageURL() ?? ""), placeholder: #imageLiteral(resourceName: "image4"))
        nextLessonPopUp.lessonLabel.text = "\(self.questionTextLbl.text ?? "") = \(selectedAnswer.answer ?? "")"
        nextLessonPopUp.wrongRightLabel.text = selectedAnswer.is_correct == 1 ?"Right":"Wrong"
        nextLessonPopUp.nextButton.backgroundColor = selectedAnswer.is_correct == 1 ? .appRightBackgroundColor(): .appWrongBackgroundColor()
        nextLessonPopUp.delegate = self
        let bool = selectedAnswer.is_correct == 1 ?true:false
        
        bool == true ?DataHandler.sharedInstance.lessonType.append((type: DataHandler.sharedInstance.questionsList[indexQuestion].nature ?? "", value: 1)):nil
        DataHandler.sharedInstance.lessonsPagerItemsController.append(bool)
        DataHandler.sharedInstance.lessonsPagerItemsId.append(selectedAnswer.id ?? "")
    }
    
    //MARK:- Images Guesture On Tap
    func setImageGuestures(){
        topCheckedImageView.isHidden = true
        let topCheckedImageViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showSelectionCheckImageTapped(tapGestureRecognizer:)))
        topCheckedImageView.isUserInteractionEnabled = true
        topCheckedImageView.addGestureRecognizer(topCheckedImageViewGestureRecognizer)

    }
    //MARK:- Add Statement Tapped
    @objc func showSelectionCheckImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        showNextLessonImageView()
    }
    
    func collectionviewRegisterDelegateandDataSource(){
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self
        collectionView.register(UINib(nibName: LessonsImagesView.NibName, bundle: nil), forCellWithReuseIdentifier: LessonsImagesView.reuseIdentifier)
        
        collectionViewItemsController.delegate = self
        collectionViewItemsController.dataSource = self
        collectionViewItemsController.register(UINib(nibName: ItemControllersCVC.NibName, bundle: nil), forCellWithReuseIdentifier: ItemControllersCVC.reuseIdentifier)
    }
    
    @IBAction func onPress_XClose(){
        let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "closeX1":"closeX"
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension LessonWithImagesVC: LessonImagesSelectionCVCDelegates{
    func onPressNext() {
        DataHandler.sharedInstance.lessonsPagerIndex += 1
        let indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
        if indexQuestion < DataHandler.sharedInstance.lessonsPagerCount{
            let type = DataHandler.sharedInstance.questionsList[indexQuestion].type ?? ""
            Helper.showQuestionTypeViewController(type: type, navigation: self.navigationController!)
        }else{
            Helper.showQuestionTypeViewController(type: "result", navigation: self.navigationController!)
        }
        
        nextLessonPopUp.dismiss(true)
    }
}
extension LessonWithImagesVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionViewItemsController == collectionView {
            return DataHandler.sharedInstance.lessonsPagerCount
        }
        return DataHandler.sharedInstance.questionsList[indexQuestion].answers?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionViewItemsController == collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemControllersCVC.reuseIdentifier, for: indexPath) as! ItemControllersCVC
            cell.itemImage.image = #imageLiteral(resourceName: "checkunselected")
            if DataHandler.sharedInstance.lessonsPagerIndex > indexPath.row{
                cell.itemImage.image = DataHandler.sharedInstance.lessonsPagerItemsController[indexPath.row] == true ? #imageLiteral(resourceName: "checkselected"):#imageLiteral(resourceName: "Cross")
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LessonsImagesView.reuseIdentifier, for: indexPath) as! LessonsImagesView
        
//        cell.setTitles(english: lessonImagesTupleSet[indexPath.row].textEnglish, Chineese: lessonImagesTupleSet[indexPath.row].textChineese)
//        cell.setImage(image: lessonImagesTupleSet[indexPath.row].image)
        let english = DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? ""
        let chinese = DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].other_language ?? ""
        
        cell.setTitles(english: english, Chineese: chinese)
        cell.image.yy_setImage(with: URL(string:DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].image_url?.scaledImageURL() ?? ""), placeholder: #imageLiteral(resourceName: "image4"))
//
//    cell.image.yy_setImage(with: URL(string:"https://www.abnamro.com/en/images/Images/Art/433x271-Recent_aquisitions.jpg"), placeholder: #imageLiteral(resourceName: "image4"))
        if selectedIndex == indexPath.row && isSelectedImage{
            cell.selectionIcon.isHidden = false
        }else{
            cell.selectionIcon.isHidden = true
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Calling helper method to play sound of answer via google translate
        if collectionView != collectionViewItemsController{
            Helper.playFileFromURL(url: "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=\(DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? "")&tl=zh", selfdelegate: self)
            selectedIndex = indexPath.row
            isSelectedImage = true
            topCheckedImageView.isHidden = false
            collectionView.reloadData()
        }
    }
}
extension LessonWithImagesVC
: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionViewItemsController == collectionView {
            let height = (collectionView.frame.size.height)
            return CGSize(width:  height , height: height)
        }
        let width = (collectionView.frame.size.width)/2-10
        let height = (collectionView.frame.size.height)/2-10
        return CGSize(width:  width , height: height)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    } 
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension LessonWithImagesVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}
