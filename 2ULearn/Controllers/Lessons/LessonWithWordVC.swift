//
//  LessonWithWordVC.swift
//  2ULearn
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 CMolds. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class LessonWithWordVC: UIViewController ,ShowsAlert{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var questionTextLbl: UILabel!
    @IBOutlet weak var btnPoints: UIButton!
    @IBOutlet weak var scoreButton: UIButton!
    @IBOutlet weak var topCheckedImageView: UIImageView!
    @IBOutlet weak var selectedLableText: UILabel!
    @IBOutlet weak var collectionViewItemsController: UICollectionView!
    
    var isSelectedImage = true
    var selectedIndex = 0 
    var nextLessonPopUp = WordsRightWrongPopUp()
    var indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let attributedString = NSMutableAttributedString(string: "\(DataHandler.sharedInstance.questionsList[indexQuestion].question ?? "")")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedStringKey, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 20)]
        attributedString.addAttributes(attributes, range: NSMakeRange(0, attributedString.length))
        questionTextLbl.attributedText = attributedString
        
        titleText.text = DataHandler.sharedInstance.lessonsName
        let data = DataHandler.sharedInstance.lessonsPagerItemsController.filter { (isTrue) -> Bool in
            return isTrue == true
        }
        debugPrint(data)
        let points = data.count * DataHandler.sharedInstance.questionsList[indexQuestion].points
        btnPoints.setTitle("\(points) Score", for: .normal)
        applicationFontsSetting()
        setImageGuestures()
        collectionviewRegisterDelegateandDataSource()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func applicationFontsSetting(){
        titleText.font = UIFont.appRegularFonts(size: Constants.App_HeaderTitleFontSize)
        scoreButton.titleLabel?.font = UIFont.appMontFonts(size: 17)
    }
    func showNextLessonImageView(){
        nextLessonPopUp = WordsRightWrongPopUp.instanceFromNib()!
        nextLessonPopUp.animator = FromTopTranslationAnimator()
        let myAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers![selectedIndex]
        let isCorrect = DataHandler.sharedInstance.questionsList[indexQuestion].answers![selectedIndex].is_correct
        let rightAnswer = DataHandler.sharedInstance.questionsList[indexQuestion].answers?.filter({ $0.is_correct == 1 })
        var answer:String?
        if rightAnswer?.count ?? 0 > 0 {
            answer = rightAnswer![0].answer ?? ""
        }
        nextLessonPopUp.lessonLabel.text = "\(self.questionTextLbl.text ?? "") = \(myAnswer.answer ?? "")"
        nextLessonPopUp.wrongRightLabel.text = isCorrect == 1 ? "Right":"Wrong"
        nextLessonPopUp.nextButton.backgroundColor = isCorrect == 1 ? .appRightBackgroundColor(): .appWrongBackgroundColor()
        nextLessonPopUp.show()
        nextLessonPopUp.delegate = self
        
        let bool = isCorrect == 1 ? true:false
        bool == true ?DataHandler.sharedInstance.lessonType.append((type: DataHandler.sharedInstance.questionsList[indexQuestion].nature ?? "", value: 1)):nil
        DataHandler.sharedInstance.lessonsPagerItemsController.append(bool)
        DataHandler.sharedInstance.lessonsPagerItemsId.append(myAnswer.id ?? "")
    }
    
    //MARK:- Images Guesture On Tap
    func setImageGuestures(){
        
        topCheckedImageView.isHidden = true
        let selectedCheckImageViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showSelectionCheckImageTapped(tapGestureRecognizer:)))
        topCheckedImageView.isUserInteractionEnabled = true
        topCheckedImageView.addGestureRecognizer(selectedCheckImageViewGestureRecognizer)
        
    }
    //MARK:- Add Statement Tapped
    @objc func showSelectionCheckImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        showNextLessonImageView()
    }
    
    func collectionviewRegisterDelegateandDataSource(){
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self
        collectionView.register(UINib(nibName: WordsSelectionViewCVC.NibName, bundle: nil), forCellWithReuseIdentifier: WordsSelectionViewCVC.reuseIdentifier)
        
        collectionViewItemsController.delegate = self
        collectionViewItemsController.dataSource = self
        collectionViewItemsController.register(UINib(nibName: ItemControllersCVC.NibName, bundle: nil), forCellWithReuseIdentifier: ItemControllersCVC.reuseIdentifier)
    }
    
    @IBAction func onPress_XClose(){
        let identifier = DataHandler.sharedInstance.lessonsSelectionType == 1 ? "closeX1":"closeX"
        self.performSegue(withIdentifier: identifier, sender: self)
    }
}

extension LessonWithWordVC: LessonImagesSelectionCVCDelegates{
    func onPressNext() {
        DataHandler.sharedInstance.lessonsPagerIndex += 1 
        indexQuestion = DataHandler.sharedInstance.lessonsPagerIndex
        if indexQuestion < DataHandler.sharedInstance.lessonsPagerCount{
            let type = DataHandler.sharedInstance.questionsList[indexQuestion].type ?? ""
            Helper.showQuestionTypeViewController(type: type, navigation: self.navigationController!)
        }else{
            Helper.showQuestionTypeViewController(type: "result", navigation: self.navigationController!)
        }
        nextLessonPopUp.dismiss(true)
    }
}
extension LessonWithWordVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionViewItemsController == collectionView {
            return DataHandler.sharedInstance.lessonsPagerCount
        }
        return DataHandler.sharedInstance.questionsList[indexQuestion].answers?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionViewItemsController == collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemControllersCVC.reuseIdentifier, for: indexPath) as! ItemControllersCVC
            cell.itemImage.image = #imageLiteral(resourceName: "checkunselected")
            if DataHandler.sharedInstance.lessonsPagerIndex > indexPath.row{
                cell.itemImage.image = DataHandler.sharedInstance.lessonsPagerItemsController[indexPath.row] == true ? #imageLiteral(resourceName: "checkselected"):#imageLiteral(resourceName: "Cross")
            }
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WordsSelectionViewCVC.reuseIdentifier, for: indexPath) as! WordsSelectionViewCVC
        cell.titleLable.text = DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? ""
 
        if selectedIndex == indexPath.row && isSelectedImage{
            cell.image.image = #imageLiteral(resourceName: "selectedShadowBox")
            selectedLableText.text = DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? ""
            cell.image.isHidden = false
        }else{
            cell.image.isHidden = true
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Calling helper method to play sound of answer via google translate
        if collectionView != collectionViewItemsController{
            Helper.playFileFromURL(url: "http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=\(DataHandler.sharedInstance.questionsList[indexQuestion].answers![indexPath.row].answer ?? "")&tl=zh", selfdelegate: self)
            selectedIndex = indexPath.row
            isSelectedImage = true
            topCheckedImageView.isHidden = false
            collectionView.reloadData()
        }
        
    }
}


extension LessonWithWordVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if collectionViewItemsController == collectionView {
            let height = (collectionView.frame.size.height)
            return CGSize(width:  height , height: height)
        }
        let width = (collectionView.frame.size.width)/2-10
        let height = (collectionView.frame.size.height)/3-10
        return CGSize(width:  width , height: height)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
}
//MARK:- Empty DataSet Delegates And Datasource
//tableView.emptyDataSetSource = self
//tableView.emptyDataSetDelegate = self
extension LessonWithWordVC : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
    //        let text = "Data Not Found."
    //        let attributes = [NSAttributedStringKey.font: UIFont.appRegularFonts(size: 16) , NSAttributedStringKey.foregroundColor: UIColor.darkGray]
    //        let attributedString = NSAttributedString.init(string: text, attributes: attributes)
    //        return attributedString
    //    }
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "Placeholders_record")
    }
}




