//
//  Protocol.swift
//  TheMall
//
//  Created by APP on 1/10/18.
//  Copyright © 2018 APP. All rights reserved.
//

import UIKit 
import TWMessageBarManager
//import DatePickerDialog
import YYImage
import NVActivityIndicatorView
import TTGSnackbar

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    
    static var NibName: String {
        return String(describing: self)
    }
}

var customAlert : CustomPopUpView?
var imageViewPopUp : fullImageView?
var validationAlert : ValidationAlertView?
var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 40 , y: 40, width: 60, height: 60))
var smallView : UIView!
var imageView = YYAnimatedImageView()

var birdLoadingAnimation: UIImageView?{
    didSet{
        var images: [UIImage] = []
        for i in 1...4 {
            images.append(UIImage(named: "img\(i)")!)
        }
        birdLoadingAnimation?.animationImages = images
        birdLoadingAnimation?.animationDuration = 0.6
    }
}

var backGroundView: UIView!

protocol ShowsAlert {}
extension ShowsAlert where Self: UIViewController {
    
    
    func showLoaderView(isCenter:Bool , UserInterfaceEnable:Bool) {
        
       
//        smallView.cor
        
        
//        if isCenter == true {
        
        let window = UIApplication.shared.keyWindow
        let x = window?.center.x
        let y = window?.center.y
        
        backGroundView = UIView.init(frame: window?.frame ?? CGRect())
        backGroundView.backgroundColor = .black
        backGroundView.alpha = 0
        
        
        
        birdLoadingAnimation = UIImageView.init(frame: CGRect(x: (x ?? 0) - 52, y: (y ?? 0) - 20, width: 104, height: 83))
        
//            let frame = CGRect(x: (x - 50), y: (y - 50), width: 100, height: 100)
//            activityIndicatorView = NVActivityIndicatorView(frame: frame)
//
//        activityIndicatorView.type = .ballSpinFadeLoader
//        activityIndicatorView.color = .appYellowBlueColor()
//        activityIndicatorView.startAnimating()
        birdLoadingAnimation?.startAnimating()
        birdLoadingAnimation?.isHidden = false
        
        window?.addSubview(backGroundView)
        window?.addSubview(birdLoadingAnimation!)
 
        UIView.animate(withDuration: 0.6, delay: 0, options: [.repeat, .autoreverse], animations: {() -> Void in
            backGroundView.alpha = 0.5
        })
        
        UIView.animate(withDuration: 0.6, delay: 0, options: [.repeat, .autoreverse], animations: {() -> Void in
            birdLoadingAnimation?.center = CGPoint.init(x: (birdLoadingAnimation?.center.x ?? 0) , y: (birdLoadingAnimation?.center.y  ?? 0) + 20)
        }, completion: nil)

        self.view.isUserInteractionEnabled = UserInterfaceEnable

        
    }
    func hideLoaderView() {
        for view in self.view.subviews{
            if view == birdLoadingAnimation  || view == backGroundView {
                view.removeFromSuperview()
                
            }
        }
        
        
        birdLoadingAnimation?.isHidden = true
        birdLoadingAnimation?.stopAnimating()
        backGroundView.removeFromSuperview()
        birdLoadingAnimation?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlertMessage(title: String , message: String) {

    }
    
    func showAlertAndDisappear(title:String, message:String,completionHandler:@escaping (Bool) -> ()) {
        
       let snackbar = TTGSnackbar(message: message,
                               duration: .short
        )
        snackbar.animationType = .slideFromBottomToTop
        snackbar.message = message
        snackbar.show()
    }
    
    func showSuccessAlertMessage(title:String, message:String, duration:CGFloat,completionHandler:@escaping (Bool) -> ()){
        TWMessageBarManager.sharedInstance().showMessage(withTitle: title, description: message, type: .success)
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            completionHandler(true)
        }
    }
    
    func showInfoAlertMessage(title:String, message:String){
        TWMessageBarManager.sharedInstance().showMessage(withTitle: title, description: message, type: .info, duration: 3.5)
    }
    
    func showErrorAlertMessage(title:String, message:String){
        TWMessageBarManager.sharedInstance().showMessage(withTitle: title, description: message, type: .error, duration: 3.5)
    }
    
    func validationAlertView(title:String, message:String, okButtonText:String){
        validationAlert = ValidationAlertView.instanceFromNib()
        validationAlert?.animator = FromTopTranslationAnimator()
        validationAlert?.messageLbl.text = message
        validationAlert?.alertTitleLbl.text = title
        validationAlert?.OkButton.setTitle(okButtonText, for: .normal)
         
        validationAlert?.show()
    }
    
    func validationAlertView(title:String, message:String, okButtonText:String,okButtonDelegates:Bool)-> ValidationAlertView {
        validationAlert = ValidationAlertView.instanceFromNib()
        validationAlert?.animator = FromTopTranslationAnimator()
        validationAlert?.messageLbl.text = message
        validationAlert?.alertTitleLbl.text = title
        validationAlert?.OkButton.setTitle(okButtonText, for: .normal)
        validationAlert?.okButtonDelegates = okButtonDelegates
        
        //        validationAlert?.show()
        return validationAlert!
    }
    
    
    
    func showCustomAlertView(messageText:String){
        customAlert = CustomPopUpView.instanceFromNib()
        customAlert?.animator = FromTopTranslationAnimator()
        customAlert?.messageLbl.text = messageText
        customAlert?.show()
    }
}

/*
protocol ShowDatePickerView {}
extension ShowDatePickerView where Self: UIViewController {
    
    func showDate(completion:@escaping  ( _ result: String) -> Void) {
        DatePickerDialog().show("Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
             //print(self.getValidWithDate(startingDate: date!))
            if let dt = date {
            completion(self.getValidWithDate(startingDate: dt))
            }

            
        }
    }
    
    func calcAge(birthday: String,completion:@escaping  ( _ result: Int) -> Void) {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MMMM-yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let gregorian = Calendar(identifier: .gregorian)
        let ageComponents = gregorian.dateComponents([.year], from: birthdayDate!, to: Date())
        let age = ageComponents.year!
        completion(age)
    }
    func getValidWithDate(startingDate: Date) ->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMMM-yyyy"
        return dateFormatter.string(from: startingDate)
    }
}

protocol ShowsAlert {}
var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 75, y: 3, width: 15, height: 15))

extension ShowsAlert where Self: UIViewController {
    
    func showLoaderView(isCenter:Bool , UserInterfaceEnable:Bool) {
        
        if isCenter == true {
            let x = self.view.center.x
            let y = self.view.center.y
            let frame = CGRect(x: (x - 50), y: (y - 50), width: 100, height: 100)
            activityIndicatorView = NVActivityIndicatorView(frame: frame)
            UIView.animate(withDuration: 1.5, animations: {
            })
            self.fadeOut()
        }else{
           activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 75, y: 3, width: 15, height: 15))
        }
        
        activityIndicatorView.type = .ballSpinFadeLoader
        activityIndicatorView.color = .appPurpleColor()
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        self.view.isUserInteractionEnabled = UserInterfaceEnable
        
    }
    func fadeOut() {
        UIView.animate(withDuration: 1.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.view.alpha = 0.9
        }, completion: nil)
    }
    func hideLoaderView() {
        self.fadeIn()
        activityIndicatorView.stopAnimating()
        self.view.isUserInteractionEnabled = true
    }
    func fadeIn() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.view.alpha = 1.0
            
        }, completion: nil)
    }
}
*/
