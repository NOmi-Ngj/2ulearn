//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import FirebaseAuth
import FirebaseCore
import FirebaseStorage
import FirebaseDatabase
import Firebase


class ChatUser: NSObject {
    
    //MARK: Properties
    let name: String
    let email: String
    let id: String
    var uid = ""
    var profilePic: UIImage
    var isLogin = ""
    var conversationId = ""
//    var lastConversationId = ""
    var userImgURL = ""
    var isFound = false
    var conversation = [MessagesObject]()
    //MARK: Methods
    class func registerUser(full_name: String,first_name:String, last_name:String,email: String, password: String, profilePicUrl: String,isLoggedIn:Int, udid:String, UserAppId:String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        
        /*
         let values = ["first_name": first_name, "last_name": last_name, "user_name": user_name , "email" : email, "id" : Int(uid)!, "isLoggedIn" : 1, "udid" :uUID , "full_name" : fullname] as [String : Any]
         */
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
//                let path = metadata?.path
                let values = ["first_name": first_name, "last_name": last_name, "email" : email, "id" : UserAppId, "isLoggedIn" : 1, "udid" :udid , "full_name" : full_name,"profilePicUrl":profilePicUrl] as [String : Any]
                Database.database().reference().child("users").child(UserAppId).updateChildValues(values, withCompletionBlock: { (errr, _) in
                    if errr == nil {
                        let userInfo = ["email" : email, "password" : password]
                        UserDefaults.standard.set(userInfo, forKey: "userInformation")
                        completion(user, nil)
                    }
                })

                
//                let storageRef = Storage.storage().reference().child("usersProfilePics").child(UserAppId)
//                let imageData = UIImageJPEGRepresentation(profilePic, 0.1)
//                storageRef.putData(imageData!, metadata: nil, completion: { (metadata, err) in
//                    if err == nil {
//                        let path = metadata?.path
//                        let values = ["name": withName, "email": email, "profilePicLink": path!, "uid":UserAppId, "isDeleted":"false"]
//                        Database.database().reference().child("users").child(UserAppId).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
//                            if errr == nil {
//                                let userInfo = ["email" : email, "password" : password]
//                                UserDefaults.standard.set(userInfo, forKey: "userInformation")
//                                completion(user, nil)
//                            }
//                        })
//                    }
//                })
            }
            else {
                completion(nil, error! as NSError)
            }
        })
    }
    
    class func loginUser(withEmail: String, password: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            if error == nil {
                let userInfo = ["email": withEmail, "password": password]
                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                completion(user,nil)
                //                completion(data, nil)
            }else{
                completion(nil,error as! NSError)
            }
        })
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        do {
            try Auth.auth().signOut()
            let data = ["isLoggedIn":0] as [String : Any]
            let usrId = DataHandler.sharedInstance.user?.id ?? ""
            
            Database.database().reference().child("users").child(usrId).updateChildValues(data)
//            UserDefaults.standard.removeObject(forKey: "userInformation")
            completion(true)
        } catch _ {
            completion(false)
        }
    }
    
    class func infoUserIdOnline(forUserID: String, completion: @escaping (Int ,String) -> Swift.Void) {
        Database.database().reference().child("users").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
            let data = snapshot.value as! NSMutableDictionary
//            let isLogin =  String(data.object(forKey: "isLoggedIn") as! Int)
            let isLogin = data.object(forKey: "isLoggedIn") as! Int
            let udid = data.object(forKey: "udid") as! String
            debugPrint(snapshot)
            completion(isLogin, udid)
            
        })
    }
    
    class func info(forUserID: String, completion: @escaping (ChatUser) -> Swift.Void) {
        Database.database().reference().child("users").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
            let data = snapshot.value as! NSMutableDictionary
            
            let email = data.object(forKey: "email") as! String //data!["email"]!
            let isLogin =  String(data.object(forKey: "isLoggedIn") as! Int)
            let fullName = data.object(forKey: "full_name") as! String
//            let id = String(data.object(forKey: "id") as! Int)
            let id = data.object(forKey: "id") as! String
            let udid = data.object(forKey: "udid") as! String
            
//            let imgDic = data.object(forKey: "images") as! NSDictionary
//            let userImgURL = imgDic.object(forKey: "three") as! String
            
            let objects = ChatUser.init(name: fullName, email: email, id: id, profilePic: UIImage(), uid: udid)
            objects.isLogin = isLogin
            objects.userImgURL = "userImgURL"
            completion(objects)
            
            print(snapshot)
        })
    }
    
    class func blockUser(user:ChatUser?, blockmessage:String, isBlocked:Bool){
        
        /*let message = Message.init(type: .text, content: blockmessage, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: true, isBlocked: isBlocked)
         Message.send(message: message, toID: (user?.id)!, isBlocked: isBlocked, completion: {(_) in
         })*/
    }
    
    
    class func updateUser(full_name: String,first_name:String, last_name:String,email: String, password: String, profilePicUrl: String,isLoggedIn:Int, udid:String, UserAppId:String, completion: @escaping (ChatUser) -> Swift.Void) {
        Database.database().reference().child("users").child(UserAppId).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let data = snapshot.value as? [String: String] {
                let uid = data["uid"]!
                let values = ["first_name": first_name, "last_name": last_name, "email" : email, "id" : UserAppId, "isLoggedIn" : 1, "udid" :udid , "full_name" : full_name,"profilePicUrl":profilePicUrl] as [String : Any]
                
                Database.database().reference().child("users").child((UserAppId)).updateChildValues(values, withCompletionBlock: { (error, _) in
                    if error == nil {
                        let user = ChatUser.init(name: full_name, email: email, id: UserAppId, profilePic: UIImage(), uid: uid)
                        completion(user)
                    }
                    
                })
//                else{
//                    let storageRef = Storage.storage().reference().child("usersProfilePics").child(forUserID)
//
//                    let imageData = UIImageJPEGRepresentation(profilePic, 0.1)
//                    storageRef.putData(imageData!, metadata: nil, completion: { (metadata, err) in
//                        if err == nil {
//                            let path = metadata?.path
////                            downloadURL()?.absoluteString
//                            let values = ["name": Name, "email": email, "profilePicLink": path!, "uid":UserAppId, "isDeleted":"false"]
//                            Database.database().reference().child("users").child((forUserID)).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
//                                let link = URL.init(string: path!)
//                                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
//                                    if error == nil {
//                                        let profilePic = UIImage.init(data: data!)
//                                        let user = ChatUser.init(name: Name, email: email, id: forUserID, profilePic: profilePic!, uid: UserAppId)
//                                        completion(user)
//                                    }
//                                }).resume()
//
//                            })
//                        }
//                    })
//                }
                
            }
        })
    }
    class func downloadAllUsers(exceptID: String, completion: @escaping (ChatUser?) -> Swift.Void) {
        
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            let id = snapshot.key
            let data = snapshot.value as! [String: Any]
            if id != exceptID {
                let name = data["full_name"] as! String
                debugPrint(name)
                let email = data["email"] as! String
                let uid = data["udid"] as! String
                
                
                 let user = ChatUser.init(name: name, email: email, id: id, profilePic: UIImage(), uid: uid)
                if user.id == DataHandler.sharedInstance.user?.id{
                     completion(nil)
                }else{
                    MessagesObject.checkIfConversationFound(currentUserId: id, { (isFound) in
                        if "yVQbkNLMYr" == id {
                            debugPrint("found")
                        }
                        
                        if isFound {
                            MessagesObject.reloadData(currentUserId: exceptID, isFromMessages: true, completion: { (messagesObjects) in
                                user.isFound = true
                                user.conversation = messagesObjects
                            })
                        }else{
                            user.isFound = false
                            completion(user)
                        }
                    })
                    
                }
               completion(user)
                
            }
        })
    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }
    
    init(name: String, email: String)
    {
        self.name = name
        self.email = email
        self.id = ""
        self.uid = ""
        self.profilePic = UIImage()
    }
    
    override init()
    {
        self.name = ""
        self.email = ""
        self.id = ""
        self.uid = ""
        self.profilePic = UIImage()
    }
    
    //MARK: Inits
    init(name: String, email: String, id: String, profilePic: UIImage, uid:String) {
        self.name = name
        self.email = email
        self.id = id
        self.uid = uid
        self.profilePic = profilePic
    }
}
