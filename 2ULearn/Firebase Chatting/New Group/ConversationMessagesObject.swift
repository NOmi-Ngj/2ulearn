//
//  ConversationMessagesObject.swift
//  Contrav
//
//  Created by MacBook on 10/25/17.
//  Copyright © 2017 DigitonicsLabs. All rights reserved.
//

import UIKit

enum conversationType
{
    case isSender
    case isReceiver
    case none
} 

class ConversationMessagesObject: NSObject {

    var imgUserPic: String
    var messageSendDate: String
    var chatTxt: String
    var conversationType: conversationType
    
    // MARK: Initialization
    
    init?(usrPic: String, dateLbl: String, chatTmp: String, tmpcon: conversationType)
    {
        // Initialize stored properties.
        self.imgUserPic =  usrPic
        self.messageSendDate = dateLbl
        self.chatTxt = chatTmp
        self.conversationType = tmpcon
    }
    
    override init() {
        
        self.imgUserPic =  ""
        self.messageSendDate = ""
        self.chatTxt = ""
        self.conversationType = .none
    }
}
