//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage


enum MessageType {
    case photo
    case text
    case location
}

enum MessageOwner {
    case sender
    case receiver
}


class Message: NSObject {
    
    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    
    var content: Any
    var content_ch: Any
    var timestamp: Int
    var isRead: Bool
    var isBlocked:Bool
    var image: UIImage?
    var toID = false
    var fromID = false
    var chatId = ""
    var timeAgo = ""
    var mediaUrl = ""
    var uid = ""
    var totalCount = 0
    var userid = ""
    var lastConversationId = ""
    
    class func deltePartiuclarChat(chatId: String, conversionId: String ,completion: @escaping (Int) -> Swift.Void) {
        Database.database().reference().child("chats").child(conversionId).child(chatId).removeValue()
        completion(1)
    }
    
//    class func deleteChatForUser(fromUserid: String , conversationID: String,completion: @escaping (Int) -> Swift.Void) {
//    let chat =  Database.database().reference().child("users").child(fromUserid).child("conversations").child(conversationID)
////    .updateChildValues(data)
//    completion(1)
//    }
    class func deleteAllChat(conversationId: String, curentUserId: String, fromUserid: String ,completion: @escaping (Int) -> Swift.Void) {
        
//        Database.database().reference().child("chats").child(conversationId).removeValue()
//        Database.database().reference().child("users").child(curentUserId).child("conversations").child(conversationId).removeValue()
//        Database.database().reference().child("users").child(fromUserid).child("conversations").child(conversationId).removeValue()
        
        completion(1)
        
    }
    
    //MARK: Methods
    class func downloadAllMessages(forUserID: String, currentUserId: String, conversionId: String,completion: @escaping ([Message]) -> Swift.Void) {

        Database.database().reference().child("chats").child(conversionId).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let data = snapshot.value as! NSDictionary
                var items = [Message]()
                
                print(conversionId)
                
                for i in 0 ..< data.allKeys.count
                {
                    let objects = data.allKeys[i] as! String
                    let valueObejct = data.value(forKey: objects)as! NSDictionary
                    
                    let messageType = valueObejct.object(forKey:"message_type") as! String
                    let type = MessageType.text

                    
                    let mediaUrl = valueObejct.object(forKey: "mediaUrl") as! String
                    let content = valueObejct.object(forKey: "mMessage") as! String
                    let content_ch = valueObejct.object(forKey: "mMessage_ch") as! String
                    var fromIDs = valueObejct.object(forKey: forUserID) as? Bool
                    if(fromIDs == nil)
                    {
                        fromIDs = false
                    }
                    
                    var toUserids = valueObejct.object(forKey: currentUserId) as? Bool
                    if(toUserids == nil)
                    {
                        toUserids = false
                    }
                    
                    let timestamp = valueObejct.object(forKey: "time") as! Int
                    let isDeleted = valueObejct.object(forKey: "isDeleted") as? String
                    let chatKey = valueObejct.object(forKey: "chatKey") as! String
                    let userid = valueObejct.object(forKey: "userid") as! String
                    let lastConversationId = valueObejct.object(forKey: "lastConversationId") as? String
//                    let userid = String(valueObejct.object(forKey: "userid") as! Int)
                    let unixTimestamp = Double(timestamp)
                    
                    let milisecond = unixTimestamp
                    let date = Date(timeIntervalSince1970: (milisecond / 1000.0))
                    
                    let horsAgo = Common.timeAgoSinceDate(date, currentDate: Date.init(), numericDates: true)
                    
                    var userChat = content.decodeUrl()
                    var userChatChine = content_ch.decodeUrl()
                    
                    userChat = userChat?.replacingOccurrences(of: "+", with: " ", options: .literal, range: nil)
                    
                    
                    userChatChine = userChatChine?.replacingOccurrences(of: "+", with: " ", options: .literal, range: nil)
                    if isDeleted == "deleteda" && userid == currentUserId
                    {
                        //return
                    }
                    else
                    {
    
                        if userid == currentUserId {
                            let message = Message.init(type: type, content: userChat!,contentChi: userChatChine, owner: .receiver, timestamp: timestamp, isRead: true, isBlocked: false)
                            message.chatId = chatKey
                            message.timeAgo = horsAgo
                            message.mediaUrl = mediaUrl
                            message.fromID = fromIDs!
                            message.toID = toUserids!
                            message.userid = userid
                            message.lastConversationId = lastConversationId ?? ""
                            if toUserids == false{
                                items.append(message)
                            }
                            // completion(message)
                        } else {
                            let message = Message.init(type: type, content: userChat!,contentChi: userChatChine, owner: .sender, timestamp: timestamp, isRead: true, isBlocked: false)
                            message.chatId = chatKey
                            message.timeAgo = horsAgo
                            message.mediaUrl = mediaUrl
                            message.fromID = fromIDs!
                            message.toID = toUserids!
                            message.userid = userid
                            message.lastConversationId = lastConversationId ?? ""
                            //completion(message)
                            if toUserids == false{
                                items.append(message)
                            }
//                            items.append(message)
                        }
                    }
                }
                
                /*  for i in 0 ..< data.allKeys.count
                 {
                 let objects = data.allKeys[i] as! String
                 let valueObejct = data.value(forKey: objects)as! NSDictionary
                 
                 let content = valueObejct.object(forKey: "mMessage") as! String
                 print(content)
                 }*/
                completion(items)
            }
            
        })
    }
    
    class func markMessagesRead(forUserID: String, currentUserID: String, childId: String)  {
        
        let data = ["isRead": true] as [String : Any]
        Database.database().reference().child("users").child(currentUserID).child("conversations").child(childId).updateChildValues(data)
        
        Database.database().reference().child("users").child(forUserID).child("conversations").child(childId).updateChildValues(data)
    }
    
    class func deleteUserFirebase(completion: @escaping (Bool) -> Swift.Void) {
        
        let user = Auth.auth().currentUser
        
        user?.delete(completion: { _ in
            
            UserDefaults.standard.removeObject(forKey: "userInformation")
            
            completion(true)
            
        })
    }
    
    
    func downloadImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
        if self.type == .photo {
            let imageLink = self.content as! String
            let imageURL = URL.init(string: imageLink)
            URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, response, error) in
                if error == nil {
                    self.image = UIImage.init(data: data!)
                    completion(true, indexpathRow)
                }
            }).resume()
        }
    }
    
    class func markMessagesRead(forUserID: String)  {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists(){
                    let data = snapshot.value as! [String: Any]
                    let location = data["location"] as! String
                    Database.database().reference().child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            for item in snap.children {
                                
                                if (snap ).key != "isBlock"{
                                    let receivedMessage = (item as! DataSnapshot).value as! [String: Any]
                                    let fromID = receivedMessage["fromID"] as! String
                                    if fromID != currentUserID {
                                        Database.database().reference().child("conversations").child(location).child((item as! DataSnapshot).key).child("isRead").setValue(true)
                                    }
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    class func deleteChat(toID:String,forLocation:String, completion: @escaping () -> Swift.Void) {
        
        let ref = Database.database().reference().child("conversations")
        if let currentUserID = Auth.auth().currentUser?.uid {
            ref.child(forLocation).observeSingleEvent(of:.value, with: { (snapshot) in
                
                let data = ["location": forLocation,"isDelete":true] as [String : Any]
                Database.database().reference().child("users").child(currentUserID).child("conversations").child(toID).updateChildValues(data)
                
                for snap in snapshot.children {
                    let receivedMessageValue = (snap as! DataSnapshot).value as! [String: Any]
                    
                    let isDeleted = receivedMessageValue[currentUserID] as? String
                    if isDeleted == nil{
                        let isotherDeleted = receivedMessageValue[toID] as? String
                        if isotherDeleted == nil{
                            let values = ["type": receivedMessageValue["type"], "content": receivedMessageValue["content"], "fromID": receivedMessageValue["fromID"], "toID": receivedMessageValue["toID"], "timestamp": receivedMessageValue["timestamp"], "isRead": receivedMessageValue["isRead"], "isBlocked": receivedMessageValue["isBlocked"],"\(currentUserID)":currentUserID]
                            Database.database().reference().child("conversations").child(forLocation).child(((snap) as AnyObject).key).setValue(values)
                        }else{
                            let values = ["type": receivedMessageValue["type"], "content": receivedMessageValue["content"], "fromID": receivedMessageValue["fromID"], "toID": receivedMessageValue["toID"], "timestamp": receivedMessageValue["timestamp"], "isRead": receivedMessageValue["isRead"], "isBlocked": receivedMessageValue["isBlocked"],"\(currentUserID)":currentUserID,"\(receivedMessageValue[toID]!)": receivedMessageValue[toID]!]
                            Database.database().reference().child("conversations").child(forLocation).child(((snap) as AnyObject).key).setValue(values)
                        }
                    }
                }
                
                completion()
            })
            
        }
        
    }
    
    func downloadLastMessage(forLocation: String, completion: @escaping () -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("conversations").child(forLocation).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    for snap in snapshot.children {
                        if (snap as! DataSnapshot).key != "isBlock"{
                            
                            // }else{
                            let receivedMessage = (snap as! DataSnapshot).value as! [String: Any]
                            
                            let isDeleted = receivedMessage[currentUserID] as? String
                            
                            
                            if isDeleted == nil{
                                self.content = receivedMessage["content"]!
                                self.timestamp = receivedMessage["timestamp"] as! Int
                                let messageType = receivedMessage["type"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                self.isRead = receivedMessage["isRead"] as! Bool
                                self.isBlocked = receivedMessage["isBlocked"] as? Bool ?? false
                                self.lastConversationId = receivedMessage["lastConversationId"] as! String
                                
                                var type = MessageType.text
                                switch messageType {
                                case "text":
                                    type = .text
                                case "photo":
                                    type = .photo
                                case "location":
                                    type = .location
                                default: break
                                }
                                self.type = type
                                if currentUserID == fromID {
                                    self.owner = .receiver
                                } else {
                                    self.owner = .sender
                                }
                                completion()
                            }
                            
                        }
                        
                    }
                }
            })
        }
    }
    
    class func send(isDataFound: Bool, message: Message, toID: String, currentUserID: String, conversationId: String,isBlocked: Bool, udi: String, chatConversationId: String, completion: @escaping (Int, String, UIImage , String) -> Swift.Void)  {
        
        let value =  Database.database().reference().childByAutoId()
        let messageEncode = (message.content as! String).encodeUrl()
        let messageEncodeChinease = (message.content_ch as! String).encodeUrl()
        
        let currentUsername = DataHandler.sharedInstance.user?.name ?? ""
        switch message.type {
        case .location:
            break
        case .photo:
            break
        case .text:
          
            if(isDataFound == true)
            {
                /*conversationId
                 mMessage
                 mMessage_ch
                 time
                 //fromName
                 //toName
                 //fromUid
                 //fromImages
                 //toUid
                 //toImages
                 //isBlock
                 //fromId
                 //toId
                 //searchName
                 //lastMsgCreatorId
                 //totalMessageCount
                 //isRead
                 //lastConversationId
                
                 result.put(""+fromId, false);                                    //18
                 result.put(""+toId,false);
                 */
                
                
                let values = ["message_type": "text", "mMessage": messageEncode ?? "","mMessage_ch": messageEncodeChinease ?? "", currentUserID: false, toID: false , "time": message.timestamp, "isDeleted" : "no_deleted", "mediaUrl" :"", "msgStatus" : "pending", "userName" : currentUsername,"fromName" : currentUsername, "userid" : currentUserID, "conversationId" :conversationId, "mUid": udi, "chatKey" : value.key, "lastConversationId": value.key ,"fromId":currentUserID,  "toId":toID] as [String : Any]
                
                Message.uploadMessage(path: kEmptyString ,type:"text" ,isDataFound: isDataFound,withValues: values, toID: currentUserID, currentUserID: toID ,conversationId: conversationId, chatConversationId: chatConversationId, content: message.content as! String,contentCh: message.content_ch as! String, timestamp:  message.timestamp, message: message, chatKey: value.key ?? "", lastConversationID: value.key ?? "", completion: { (status, chatID, imgData , userImage) in
                    completion(status, chatID, imgData , userImage)
                })
            }
            else
            {
                let values = ["message_type": "text", "mMessage": messageEncode,"mMessage_ch": messageEncodeChinease, currentUserID: false, toID: false , "time": message.timestamp, "isDeleted" : "no_deleted", "mediaUrl" :"", "msgStatus" : "pending", "userName" : currentUsername,"fromName" : currentUsername, "userid" : currentUserID, "conversationId" :conversationId, "mUid": udi, "chatKey" : value.key, "lastConversationId": value.key ,"fromId":currentUserID,  "toId":toID] as [String : Any]
//                let values = ["": ""]
                Message.uploadMessage(path: kEmptyString ,type:"text" , isDataFound: isDataFound,withValues: values, toID: currentUserID, currentUserID: toID ,conversationId: conversationId, chatConversationId: chatConversationId, content: message.content as! String,contentCh: message.content_ch as! String, timestamp:  message.timestamp, message: message, chatKey: value.key ?? "", lastConversationID: value.key ?? "", completion: { (status, chatID, imgData, userImage ) in
                    completion(status, chatID, imgData, userImage)
                })
            }
            
            break
        }
        
    
    }
    
    class func uploadMessage(path: String, type: String, isDataFound: Bool, withValues: [String: Any], toID: String, currentUserID: String,conversationId: String, chatConversationId: String, content: String, contentCh: String, timestamp: Int, message: Message, chatKey: String , lastConversationID:String,completion: @escaping (Int, String, UIImage, String) -> Swift.Void) {
        
        let value =  chatKey //Database.database().reference().childByAutoId()
        let messageEncode = content.encodeUrl()
        
        let messageEncodeChi  = contentCh.encodeUrl()
        
        let msgCon = (message.content as! String).encodeUrl()
        let msgConChi = (message.content_ch as! String).encodeUrl()
        
        
        if(isDataFound == false)
        {
            
            ChatUser.info(forUserID: currentUserID, completion: { (user) in
                let toName = user.name
                let imgDic = user.userImgURL
                
                var usrNem = DataHandler.sharedInstance.user?.name ?? ""
                var imgUrl = DataHandler.sharedInstance.user?.name ?? ""
//                var uUID = Common.getValueForKey(keyValue: kSaveUDID)
//                let uUID = UIDevice.current.identifierForVendor!.uuidString
                let uUID = Common.getValueForKey(keyValue: kdeviceToken)
                let profileimg = UIImage()
                let toUserId = currentUserID
                let fromUserId = toID// ->curent
                
                // 57 clark // toUserId
                // 50 tony // fromUserId
                let usrIds = DataHandler.sharedInstance.user?.id ?? ""
                let valueInner =  Database.database().reference().childByAutoId()
                
              
                
                
                var usrId = DataHandler.sharedInstance.user?.id ?? ""
                
                if Common.isTesting(){
                    let appUser = Helper.getUser()
                    usrId = appUser?.id ?? ""
                    usrNem = appUser?.name ?? ""
                    imgUrl = appUser?.image_url?.threeX ?? ""
                }

                
                
                
                let myObject = ["conversationId": chatKey, "fromId" : toUserId, "fromImages": imgDic,
                                "fromName": user.name, "fromUid" : user.uid, "isBlock": false, "isRead" : false,
                                "lastMsgCreatorId" : usrIds , "mMessage": messageEncode ,"mMessage_ch": messageEncodeChi , "searchName" : user.name.lowercased(), "time" : timestamp, "toId" : fromUserId,
                                "toImages" : imgUrl, "toName" : usrNem, "toUid" : uUID , "totalMessageCount": 1 , toID : false , currentUserID : false, "lastConversationId":valueInner.key] as [String : Any]
                
                
                let chatObject = ["message_type": "text", "mMessage": msgCon,"mMessage_ch": msgConChi, currentUserID: false, toID: false , "time": message.timestamp, "isDeleted" : "no_deleted", "mediaUrl" :"", "msgStatus" : "pending", "userName" : usrNem, "userid" :usrId, "conversationId" :chatKey, "mUid": uUID, "chatKey" : valueInner.key, "lastConversationId": valueInner.key ] as [String : Any]
                
                
                Database.database().reference().child("chats").child(value).child(valueInner.key ?? "").updateChildValues(chatObject)
                Database.database().reference().child("users").child(toID).child("conversations").child(value).updateChildValues(myObject)
                Database.database().reference().child("users").child(currentUserID).child("conversations").child(value).updateChildValues(myObject)
                
                completion(0, value, profileimg, imgDic)
                
            })
            
        }
        else
        {
            Database.database().reference().child("chats").child(conversationId).child(value).setValue(withValues, withCompletionBlock: { (error, reference) in
                print(currentUserID)
                print(toID)
                let data = ["chatKey": chatKey] as [String : Any]
                Database.database().reference().child("chats").child(conversationId).child(chatKey).updateChildValues(data)
                let totalCount = message.totalCount + 1
//               let data1 = ["message_type": "text", "mMessage": msgCon,"mMessage_ch": msgConChi, currentUserID: false, toID: false , "time": message.timestamp, "isDeleted" : "no_deleted", "mediaUrl" :"", "msgStatus" : "pending", "userName" : usrNem, "userid" :usrId, "conversationId" :chatKey, "mUid": uUID, "chatKey" : valueInner.key, "lastConversationId": valueInner.key ] as [String : Any]
                
                let data1 = ["mMessage": content, "mMessage_ch":contentCh, "isRead" : false, "time": timestamp, "totalMessageCount" : totalCount, "lastConversationId":lastConversationID, currentUserID: false, toID: false ] as [String : Any]
                Database.database().reference().child("users").child(toID).child("conversations").child(chatConversationId).updateChildValues(withValues)
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(chatConversationId).updateChildValues(withValues)
                
                let imge = UIImage()
                completion(totalCount, "-1", imge, "")
            })
        }
        
        
    }
    
    
    class func deleteParticularUserChat(forUserID: String, OtherUserId: String,conversationId: String, chatKey: String,completion: @escaping (Int) -> Swift.Void)
    {
        
        Database.database().reference().child("chats").child(conversationId).observeSingleEvent(of:.value, with: { (snapshot) in
            
            if snapshot.exists() {
                let childs = snapshot.childrenCount
                //                let lastChild = (snapshot.children.allObjects as! [DataSnapshot])[Int(childs-1)]
                //                let lastReceivedMessage = (lastChild).value as! [String: Any]
                
                var increaments = 1
                for snap in snapshot.children {
                    let receivedMessage = (snap as! DataSnapshot).value as! [String: Any]
                    let myUser = receivedMessage["\(forUserID)"] as? Bool
                    let otherUser = receivedMessage["\(OtherUserId)"] as? Bool
                    let chatKeyId = receivedMessage["chatKey"] as! String
                    let data = ["isDeleted": "deleted","\(forUserID)": true] as [String : Any]
                    Database.database().reference().child("chats").child(conversationId).child(chatKeyId).updateChildValues(data)
                    
                    if otherUser == true {
                        debugPrint("delete all chat")
                        Database.database().reference().child("chats").child(conversationId).child(chatKeyId).removeValue()
                    }
                    
                    
                    increaments == childs ? completion(1):nil
                    increaments += 1
                }
                
            }else{
                completion(0)
            }
        })
        
        let data = ["\(forUserID)": true] as [String : Any]
        
        Database.database().reference().child("users").child(forUserID).child("conversations").child(conversationId).updateChildValues(data)
        //        removeValue()
        //        updateChildValues(data)
        
    }
    class func deletePartiucalrChat(conversationId: String, chatKey: String)
    {
        let data = ["isDeleted": "deleted"] as [String : Any]
        Database.database().reference().child("chats").child(conversationId).child(chatKey).updateChildValues(data)
    }
    
    
    class func notifyOtherUser(uid:String,message:String)  {
        _ = ["id": uid, "message": message] as [String:AnyObject]
        //        let _ = ApiHelper.postRequest(params, kPath: Users.notificationOnSendMessage.rawValue) { (response, error) in
        //
        //        }
    }
    //MARK: Inits
    init(type: MessageType, content: Any,contentChi: Any, owner: MessageOwner, timestamp: Int, isRead: Bool, isBlocked:Bool) {
        self.type = type
        self.content = content
        self.content_ch  = contentChi
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
        self.isBlocked = isBlocked
    }
    
    init(type: MessageType, content: Any,contentChi: Any, owner: MessageOwner, timestamp: Int, isRead: Bool, isBlocked:Bool,toID:Bool,fromID:Bool,chatId:String,timeAgo:String,uid:String,totalCount:Int,userid:String,lastConversationId:String) {
      
         self.toID = toID
        
        self.fromID = fromID
        
        self.chatId = chatId
        
        self.timeAgo = timeAgo
        
        self.uid = uid
        
        self.totalCount = totalCount
        
        self.userid = userid
        
        self.lastConversationId = lastConversationId
        self.type = type
        self.content = content
        self.content_ch  = contentChi
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
        self.isBlocked = isBlocked
    }
    
    override init() {
        self.type = .location
        self.content = ""
        self.content_ch  = ""
        self.owner = .receiver
        self.timestamp = 12312321
        self.isRead = false
        self.isBlocked = false
    }
    
    func getDictionary() -> String {
        var data = String()
        data.append(lastConversationId)
        data.append(content as? String ?? "")
        data.append(content_ch as? String ?? "")
        data.append("\(timestamp)")
        //data.append(Helper.getUser()?.name)
        data.append("toName")
        data.append("fromUid")
        data.append("fromImages")
        data.append("toUid")
        data.append("toImages")
        data.append("isBlock")
        data.append("fromId")
        data.append("toId");
        data.append("searchName");
        data.append("lastMsgCreatorId")
        data.append("totalMessageCount");
        data.append("isRead");
        data.append("lastConversationId");
        return data
    }
}

extension String
{
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
}


