//
//  MessagesObject.swift
//  Contrav
//
//  Created by MacBook on 10/20/17.
//  Copyright © 2017 DigitonicsLabs. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class MessagesObject: NSObject {
    
    //MARK: Properties
    let user: ChatUser
    var lastMessage: Message
    var location:String
    var totalCount = 0
    var conversationId = ""
    var lastConversationId = ""
    
    class func checkIfConversationFound(currentUserId: String , _ completion: @escaping (Bool) -> Void) {
        // ...
        
        let db = Database.database().reference()
        db.child("users").child(currentUserId).child("conversations").observe(.value, with: { (snapshot) in
            if snapshot.value is NSNull
            {
                completion(false)
            }
            else
            {
                completion(true)
            }
        })
    }
    
    
    //MARK: Methods
    class func showConversations(currentUserId: String , completion: @escaping ([MessagesObject], Bool) -> Swift.Void) {
        //if let currentUserID = Auth.auth().currentUser?.uid {
        
        checkIfConversationFound(currentUserId: currentUserId) { (hasSucceeded: Bool) -> Void in
            if hasSucceeded {
                // Do what you want
                reloadData(currentUserId: currentUserId, isFromMessages: false, completion: { (messagesObjects) in
                    completion(messagesObjects, true)
                })
            } else {
                // Failed
                print("failes")
                
                var conversations = [MessagesObject]()
                let conversationObj = MessagesObject.init()
                conversationObj.conversationId = "-1"
                conversations.append(conversationObj)
                completion(conversations, false)
            }
        }
        
        
    }
    
    class func reloadData(currentUserId: String , isFromMessages:Bool, completion: @escaping ([MessagesObject])  -> Swift.Void)
    {
        var conversations = [MessagesObject]()
        
        Database.database().reference().removeAllObservers();
        Database.database().reference().child("users").child(currentUserId).child("conversations").observe(.childAdded , with: { (snapshot) in
            
            if snapshot.exists() {
                print(currentUserId)
                //let fromID = snapshot.key

                let values = snapshot.value as! [String: Any]
                
                let isDeleted = values["isBlock"] as? Bool
                if(isDeleted == nil)
                {
                    return
                }
                let isRead = values["isRead"] as? Bool
                let isUserDeleted = values["\(currentUserId)"] as? Bool
                let totalMsgCount = values["totalMessageCount"]  as! Int
                let currentUsername = DataHandler.sharedInstance.user?.name  ?? ""
                let firebasUr = String(describing: values["fromName"]!)
                
                var UserId = ""
                if(firebasUr == currentUsername)
                {
                    let toId = String(describing: values["toId"]!)
                    UserId = toId
                }
                else
                {
                    let formUdid = String(describing: values["fromId"]!)
                    UserId = formUdid
                }
                
                let messageTxt = values["mMessage"] as! String
                let messageTxtChines = values["mMessage_ch"] as! String
                
                let mesageString = messageTxt.decodeUrl()
                
                let timeStampe = values["time"] as? Int
                let conversationId = values["conversationId"] as? String
                let lastConversationId = values["lastConversationId"] as? String ?? ""
                
                
                if(isDeleted == false)
                {
                    ChatUser.info(forUserID: UserId, completion: { (user) in
                        let toName = user.name
                        
                        let imgDic = user.userImgURL
                        let profilePic = UIImage()
                        
                        let user1 = ChatUser.init(name: toName , email: user.email, id: UserId, profilePic: profilePic, uid: user.uid)
                        user1.isLogin = user.isLogin
                        user1.conversationId = conversationId!
        
                        user1.uid = user.uid
                        user1.userImgURL = imgDic
                        
                        let emptyMessage = Message.init(type: .text, content: mesageString,contentChi: messageTxtChines, owner: .sender, timestamp: timeStampe!, isRead: isRead!, isBlocked: false)
                        emptyMessage.uid = user.uid //UserId
                        emptyMessage.totalCount = totalMsgCount
                        emptyMessage.chatId = conversationId!
            
                        emptyMessage.uid = String(describing: values["toId"]!)
                        emptyMessage.userid = String(describing: values["fromId"]!)
                        let unixTimestamp = Double(timeStampe!)
                        
                        let milisecond = unixTimestamp
                        let date = Date(timeIntervalSince1970: (milisecond / 1000.0))
                        
                        let time420 = Date(milliseconds: Int(milisecond))
                        print(time420)
                        
                        let horsAgo = Common.timeAgoSinceDate(date, currentDate: Date.init(), numericDates: true)
                        emptyMessage.timeAgo = horsAgo
                        
                        let conversation = MessagesObject.init(user: user1, lastMessage: emptyMessage,location: "123")
                        conversation.conversationId = conversationId!
                        conversation.totalCount = totalMsgCount
                        conversation.lastConversationId = lastConversationId
                        if isUserDeleted ?? false == false {
                            conversations.append(conversation)
                        }
                        if isFromMessages{
                            conversations.append(conversation)
                        }
                        
                        completion(conversations)
                        
                        /* let link = URL.init(string: imgDic)
                         URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                         if error == nil {
                         let profilePic = UIImage.init(data: data!)
                         
                         }
                         }).resume()
                         */
                    })
                }
            }
        })
    }
    
    //MARK: Inits
    init(user: ChatUser, lastMessage: Message,location:String) {
        self.user = user
        self.lastMessage = lastMessage
        self.location = location
    }
    
    override init() {
        self.user = ChatUser()
        self.lastMessage = Message(type: .location, content: kEmptyString,contentChi: kEmptyString, owner: .receiver, timestamp: 445554, isRead: false, isBlocked: false)
        self.location = kEmptyString
    }
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
