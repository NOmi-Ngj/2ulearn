//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import UIKit
import FirebaseAuth
import FirebaseCore
import FirebaseStorage
import FirebaseDatabase
import Firebase

class Conversation {
    
    //MARK: Properties
    let user: ChatUser
    var lastMessage: Message
    var location:String
    
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            var conversations = [Conversation]()
            
            Database.database().reference().removeAllObservers(); Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded , with: { (snapshot) in
                if snapshot.exists() {
                    let fromID = snapshot.key
                    let values = snapshot.value as! [String: Any]
                    let location = values["location"] as? String
                    let isDeleted = values["isDelete"] as? Bool
                    
                    if !isDeleted!{
                        ChatUser.info(forUserID: fromID, completion: { (user) in
                            let emptyMessage = Message.init(type: .text, content: "loading",contentChi: "loading", owner: .sender, timestamp: 0, isRead: true, isBlocked: false)
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage,location: location!)
                            conversations.append(conversation)
                            conversation.lastMessage.downloadLastMessage(forLocation: location!, completion: {
                                
                                completion(conversations)
                            })
                        })
                    }else{
                       completion(conversations)
                    }
                }
            })
        }
    }
    
    //MARK: Inits
    init(user: ChatUser, lastMessage: Message,location:String) {
        self.user = user
        self.lastMessage = lastMessage
        self.location = location
    }
 

}
