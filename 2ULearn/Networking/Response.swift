//
//  Response.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper


class Response : NSObject, Mappable {
    
    var code : Int?
    var data : AnyObject?
    var message : [String]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        data <- map[ResponseKey.data.rawValue]
        message <- (map["messages"], ErrorMessagesTransform())
    }
}

// This class convert string into array if error message is string
class ErrorMessagesTransform: TransformType {
    
    func transformFromJSON(_ value: Any?) -> Array<String>? {
        if let value = value as? String {
            return [value]
        }
        if let value = value as? [String] {
            return value
        }
        return []
    }
    
    func transformToJSON(_ value: Array<String>?) -> String? {
        // TODO: If needed
        return nil
    }
}

