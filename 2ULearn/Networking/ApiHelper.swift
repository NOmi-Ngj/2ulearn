//
//  ApiHelper.swift
//  ittendance
//
//  Created by Cygnis on 5/3/17.
//  Copyright © 2017 Cygnis. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
//import CRNotifications

class ApiHelper {

}

enum ResponseKey: String {
    case error = "error",success = "response", data = "data"
    
}

private let unableToConnect = "Oops! something went wrong. Please try again."

extension ApiHelper {
    
    // MARK: POST REQUEST
    static func postRequest(_ params: [String : AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) -> DataRequest {
        let url = baseURL + kPath
        
        debugPrint("Web URL == \(url)")
        debugPrint("Param == \(String(describing: params))")
        
        
        let request = Alamofire.request(url, method: .post,
                                        parameters: params,
                                        encoding: JSONEncoding.default,
                                        headers: DataHandler.sharedInstance.getHeader(contentType: true)
                                        ).responseJSON { (response: DataResponse<Any>) in
                                            
                                          
                                            debugPrint("response APIPATH \(kPath): ===error=== \(String(describing: response.result.error))")
                                            debugPrint("response APIPATH \(kPath): ===value=== \(String(describing: response.result.value))")
                                            
                                            
                                            guard response.result.error == nil else { // error for eg : network unavailable
                                                
                                                let err = response.result.error as NSError?
                                                if err?.code != NSURLErrorCancelled {
                                                    
                                                    let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                                    let error = NSError(domain: "",
                                                                        code: (err?.code)!,
                                                                        userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                                                    completion(nil, error)
                                                    
                                                }
                                                return
                                            }
                                            
                                            let reponseObject = response.result.value as? [String:AnyObject?]
                                            
                                            guard reponseObject != nil else { // reponse is nil
                                                
                                                let error = NSError(domain: "",
                                                                    code: 404,
                                                                    userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                                completion(nil, error)
                                                
                                                return;
                                            }
                                            
                                            if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                                
                                                debugPrint("Response object: \(String(describing: reponseObject))")
                                                
                                                
                                                completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                                
                                                
                                            } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                                
                                                
                                                let responseData = Mapper<Response>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                               
                                                    
                                                    debugPrint("responseData postRequest ==\(String(describing: responseData))")
                                                    let error = NSError(domain: "",
                                                                        code: (responseData?.code)!,
                                                                        userInfo: [NSLocalizedDescriptionKey : String(describing: (responseData!.message![0])) as String])
                                                    
                                                    completion(nil, error)
                                                
                                            }
        }
        
        return request
    }
    
    static func postMultipartRequestWithNSMutableDictionary(_ params: NSMutableDictionary?,multiPartParams:[String:AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseURL + kPath
        
        
        debugPrint("API URL:\n \(String(describing: url)) ===params===\n\(String(describing: params)) ===multipart params===\n\(String(describing: multiPartParams)) ===headers===\n\(String(describing: DataHandler.sharedInstance.getHeader(contentType: false)))")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if let imageData = UIImageJPEGRepresentation(multiPartParams?["image"] as! UIImage , 0.5) {
                debugPrint(imageData.count/1024)
                multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "image.jpg", mimeType:  "image/jpeg")
            }
            
            if params != nil {
                
                for (key, value) in params! {
                    
                    let stringValue = "\(value)"
                    multipartFormData.append(((stringValue  as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key as! String)
                    
                }}}, to: url, method: .post, headers: DataHandler.sharedInstance.getHeader(contentType: false),
                     encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                guard response.result.error == nil else { // error for eg : network unavailable
                                    
                                    let err = response.result.error as NSError?
                                    if err?.code != NSURLErrorCancelled {
                                        
                                        
                                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                        let error = NSError(domain: "",
                                                            code: (err?.code)!,
                                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                                        completion(nil, error)
                                        
                                    }
                                    debugPrint("%@",err?.localizedDescription )
                                    
                                    return;
                                }
                                
                                let reponseObject = response.result.value as? [String:AnyObject?]
                                
                                guard reponseObject != nil else { // reponse is nil
                                    
                                    
                                    let error = NSError(domain: "",
                                                        code: 404,
                                                        userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                    completion(nil, error)
                                    debugPrint("%@",error.localizedDescription )
                                    
                                    return;
                                }
                                
                                
                                if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                    
                                    debugPrint("Response object: \(String(describing: reponseObject))")
                                    
                                    completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                    
                                } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                    
                                    let responseData = Mapper<Response>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                    
                                    
                                    let error = NSError(domain: "",
                                                        code: (responseData?.code)!,
                                                        userInfo: [NSLocalizedDescriptionKey : (responseData?.message?[0])! as String])
                                    
                                    completion(reponseObject as AnyObject?, error)
                                    
                                }
                            }
                        case .failure(let encodingError):
                            completion(nil, encodingError as NSError?)
                            
                            debugPrint("error:\(encodingError)")
                        }
        })
        
    }
    
    
    // MARK: POST MULTIPART IMAGE
    
    static func postMultipartRequest(_ params: [String : AnyObject]?,multiPartParams:[String:AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseURL + kPath
        
        
        debugPrint("API URL:\n \(String(describing: url)) ===params===\n\(String(describing: params)) ===multipart params===\n\(String(describing: multiPartParams)) ===headers===\n\(String(describing: DataHandler.sharedInstance.getHeader(contentType: false)))")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
                        if let imageData = UIImageJPEGRepresentation(multiPartParams?["data"] as! UIImage , 0.5) {
                            debugPrint(imageData.count/1024)
                            multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "image.jpg", mimeType:  "image/jpeg")
             
                        }
            
            
            if params != nil {
                
                for (key, value) in params! {
                    
                    let stringValue = "\(value)"
                    multipartFormData.append(((stringValue  as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key)
                    
                }}}, to: url, method: .post, headers: DataHandler.sharedInstance.getHeader(contentType: false),
                     encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                guard response.result.error == nil else { // error for eg : network unavailable
                                    
                                    let err = response.result.error as NSError?
                                    if err?.code != NSURLErrorCancelled {
                                        
                                        
                                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                        let error = NSError(domain: "",
                                                            code: (err?.code)!,
                                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                                        completion(nil, error)
                                        
                                    }
                                    debugPrint("%@",err?.localizedDescription )
                                    
                                    return;
                                }
                                
                                let reponseObject = response.result.value as? [String:AnyObject?]
                                
                                guard reponseObject != nil else { // reponse is nil
                                    
                                    
                                    let error = NSError(domain: "",
                                                        code: 404,
                                                        userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                    completion(nil, error)
                                    debugPrint("%@",error.localizedDescription )
                                    
                                    return;
                                }
                                
                                
                                if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                    
                                    debugPrint("Response object: \(String(describing: reponseObject))")
                                    
                                    completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                    
                                } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                    
                                    let responseData = Mapper<Response>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                    
                                    
                                    let error = NSError(domain: "",
                                                        code: (responseData?.code)!,
                                                        userInfo: [NSLocalizedDescriptionKey : (responseData?.message?[0])! as String])
                                    
                                    completion(reponseObject as AnyObject?, error)
                                    
                                }
                            }
                        case .failure(let encodingError):
                            completion(nil, encodingError as NSError?)
                            
                            debugPrint("error:\(encodingError)")
                        }
        })
        
    }
    
    static func postMultipartRequestAudioFile(_ params: [String : AnyObject]?,multiPartParams:[String:AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseURL + kPath
        
        
        debugPrint("API URL:\n \(String(describing: url)) ===params===\n\(String(describing: params)) ===multipart params===\n\(String(describing: multiPartParams)) ===headers===\n\(String(describing: DataHandler.sharedInstance.getHeader(contentType: false)))")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(multiPartParams?["data"] as! Data, withName: multiPartParams?["parameter"] as! String, fileName: "audio.3gp", mimeType:  "video/3gpp")
            
            if params != nil {
                
                for (key, value) in params! {
                    
                    let stringValue = "\(value)"
                    multipartFormData.append(((stringValue  as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key)
                    
                }}}, to: url, method: .post, headers: DataHandler.sharedInstance.getHeader(contentType: false),
                     encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                guard response.result.error == nil else { // error for eg : network unavailable
                                    
                                    let err = response.result.error as NSError?
                                    if err?.code != NSURLErrorCancelled {
                                        
                                        
                                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                        let error = NSError(domain: "",
                                                            code: (err?.code)!,
                                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                                        completion(nil, error)
                                        
                                    }
                                    debugPrint("%@",err?.localizedDescription )
                                    
                                    return;
                                }
                                
                                let reponseObject = response.result.value as? [String:AnyObject?]
                                
                                guard reponseObject != nil else { // reponse is nil
                                    
                                    
                                    let error = NSError(domain: "",
                                                        code: 404,
                                                        userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                    completion(nil, error)
                                    debugPrint("%@",error.localizedDescription )
                                    
                                    return;
                                }
                                
                                
                                if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                    
                                    debugPrint("Response object: \(String(describing: reponseObject))")
                                    
                                    completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                    
                                } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                    
                                    let responseData = Mapper<Response>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                    
                                    
                                    let error = NSError(domain: "",
                                                        code: (responseData?.code)!,
                                                        userInfo: [NSLocalizedDescriptionKey : (responseData?.message?[0])! as String])
                                    
                                    completion(reponseObject as AnyObject?, error)
                                    
                                }
                            }
                        case .failure(let encodingError):
                            completion(nil, encodingError as NSError?)
                            
                            debugPrint("error:\(encodingError)")
                        }
        })
        
    }
    
    /*
    // MARK: PUT REQUEST
    
    
    static func putRequest(_ params: [String : AnyObject], kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseURL + kPath
        
        debugPrint("put request api url: \(String(describing: url)) ===params===\(String(describing: params)) ===headers===: \(String(describing: DataHandler.sharedInstance.getHeader(contentType: true)))")
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: DataHandler.sharedInstance.getHeader(contentType: true)).responseJSON { (response: DataResponse<Any>) in
            //Error handling
            
            guard response.result.error == nil else {
                
                let err = response.result.error as NSError?
                if err?.code != NSURLErrorCancelled {
                    if err?.code == errorCode.AuthorizationFailed.rawValue {
                        Helper.popToLogin()
                    }else{
                        
                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                        let error = NSError(domain: "",
                                            code: (err?.code)!,
                                            userInfo: [NSLocalizedDescriptionKey : message])
                        completion(nil, error)
                    }
                }
                return;
            }
            
            let responseObject = response.result.value as? [String : AnyObject]
            // if response contains no data
            guard responseObject != nil else {
                let error = NSError(domain: "", code: 404, userInfo: [NSLocalizedDescriptionKey : unableToConnect])
                completion(nil, error)
                return;
            }
            
            if (responseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                debugPrint("response object: \(String(describing: responseObject))")
                
                completion(responseObject![ResponseKey.success.rawValue]! as AnyObject?, nil)
                
            } else if (responseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                
                let responseData = Mapper<Response>().map(JSON: responseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                
                if responseData?.code == errorCode.AuthorizationFailed.rawValue {
                    Helper.popToLogin()
                } else{
                    
                    
                    let error = NSError(domain: "",
                                        code: (responseData?.code)!,
                                        userInfo: [NSLocalizedDescriptionKey : (responseData?.message?[0])! as String])
                    
                    completion(nil, error)
                }
                
            }
            
            // code commented because for dynamic use of this method
            /*let requestResult = Mapper<Response>().map(JSON: responseObject![ResponseKey.success.rawValue] as! [String : Any])
             if let requestResult = requestResult {
             completion(requestResult, nil)
             } else {
             let error = NSError(domain: "Failed", code: 0 , userInfo: [NSLocalizedDescriptionKey : "Unknown error"])
             completion(nil, error)
             }*/
        }
        
    }*/
    
    // MARK: GET REQUEST
    
    static func getRequest(_ params: [String : AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) -> DataRequest {
        var url : String
        
     
        if kPath.contains("https"){
            
            url = kPath
            debugPrint("webservice path: \(kPath): \(url)")
            debugPrint(params ?? "")
        }else{
            url = baseURL + kPath
            debugPrint("webservice path: \(kPath): \(url)")
            debugPrint(params ?? "")
        }
    
        
        let request = Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: kPath.contains("https") ? DataHandler.sharedInstance.getHeader(contentType: false) : DataHandler.sharedInstance.getHeader(contentType: true)).responseJSON { (response: DataResponse<Any>) in
            
            
            guard response.result.error == nil else {
                
                let err = response.result.error as NSError?
                if err?.code != NSURLErrorCancelled {
                    
//                    if err?.code == errorCode.AuthorizationFailed.rawValue {
//                        Helper.popToLogin()
//                    }else{
                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                        let error = NSError(domain: "",
                                            code: (err?.code)!,
                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                        completion(nil, error)
                   // }
                }
                return;
            }
            
            let responseObject = response.result.value as? [String:AnyObject?]
            guard responseObject != nil else {
                let error = NSError(domain: "No Data", code: 404, userInfo: [NSLocalizedDescriptionKey : unableToConnect])
                completion(nil, error)
                return;
            }
            if (responseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                completion(responseObject![ResponseKey.success.rawValue] as AnyObject?, nil)
                
            }else  if (responseObject![ResponseKey.data.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                completion(responseObject![ResponseKey.data.rawValue] as AnyObject?, nil)
                
            } else if (responseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                
                let responseData = Mapper<Response>().map(JSON: responseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                
                //                if responseData?.code == errorCode.AuthorizationFailed.rawValue {
                //                    Helper.popToLogin()
                //                } else{
                
                let error =  NSError(domain: "",
                                     code: (responseData?.code)!,
                                     userInfo: [NSLocalizedDescriptionKey : (responseData?.message?[0])! as String])
                
                completion(nil, error)
                // }
            }
        }
        
        return request
    }
    
    // MARK: DELETE REQUEST
    
    static func deleteRequest(_ params: [String : AnyObject], kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseURL + kPath
        debugPrint("webservice path: \(kPath): \(url)")
        debugPrint(params)
        
        Alamofire.request(url, method: .delete, parameters: params, encoding: URLEncoding.queryString, headers: DataHandler.sharedInstance.getHeader(contentType: true)).responseJSON { (response: DataResponse<Any>) in
            
            
            guard response.result.error == nil else {
                
                let err = response.result.error as NSError?
                if err?.code != NSURLErrorCancelled {
//                    if err?.code == errorCode.AuthorizationFailed.rawValue {
//                        Helper.popToLogin()
//                    }else{
                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                        let error = NSError(domain: "",
                                            code: (err?.code)!,
                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                        completion(nil, error)
                 //   }
                }
                return
            }
            
            let responseObject = response.result.value as? [String:AnyObject?]
            guard responseObject != nil else {
                
                let error = NSError(domain: "", code: 404, userInfo: [NSLocalizedDescriptionKey : unableToConnect])
                
                completion(nil, error)
                return;
            }
            
            if (responseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                completion(responseObject![ResponseKey.success.rawValue] as AnyObject?, nil)
                
            } else if (responseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                
                let responseData = Mapper<Response>().map(JSON: responseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                
//                if responseData?.code == errorCode.AuthorizationFailed.rawValue {
//                    Helper.popToLogin()
//                } else{
                
                    let error =  NSError(domain: "",
                                         code: (responseData?.code)!,
                                         userInfo: [NSLocalizedDescriptionKey : (responseData?.message?[0])! as String])
                    
                    completion(nil, error)
              //  }
                
            }
        }
    }
    
    
//    static func accessToken() {
//        
//        let loginUser = DataHandler.sharedInstance.getUser(userId: DataHandler.sharedInstance.loginId)
//        let accessToken = loginUser?.accessToken
//        
//        do {
//            let jwt = try decode(jwt: accessToken!)
//            let expireDate = Calendar.current.date(byAdding: .minute, value: -1, to: jwt.expiresAt!)
//            let localUTCDate = Helper.utcConversion(date: Date())
//            
//            if expireDate?.compare(localUTCDate) != ComparisonResult.orderedDescending {
//                let _ = ApiHelper.getRequest([:], kPath:GeneralPaths.accessToken.rawValue , completion: { (data, error) in
//                    
//                    if error != nil {
//                        
//                        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
//                        let viewControllers: [UIViewController] = navigationController.viewControllers
//                        
//                        for aViewController in viewControllers {
//                            if aViewController is StartScreen {
//                                navigationController.popToViewController(aViewController, animated: true)
//                            }
//                            else if aViewController == viewControllers.last {
//                                let storyboard: UIStoryboard = UIStoryboard.newsFeedsStoryboard()
//                                let vc = storyboard.instantiateViewController(withIdentifier: START_SCREEN)
//                                navigationController.pushViewController(vc, animated: false)
//                            }
//                        }
//                        
//                    } else {
//                        
//                        let loginUser = DataHandler.sharedInstance.getUser(userId:DataHandler.sharedInstance.loginId )
//                        let realm = try! Realm()
//                        try! realm.write {
//                            loginUser?.accessToken = data?[ResponseKey.data.rawValue] as! String
//                        }
//                    }
//                    
//                })
//            }
//        }
//            
//            
//        catch {
//            print("Failed to decode JWT: \(error)")
//        }
//    }*/
    
    // MARK: - Internet connected check
//    func isConnectedToNetwork() -> Bool {
//
//
//        if Reachability.isConnectedToNetwork() == true
//        {
//            return true
//        }
//        else
//        {
//            return false
//        }
//    }
    
    static func sendPush(fcmToken: String, params:[String: AnyObject]?,converstaionObject:[String: AnyObject]?,isLogin:Int ,completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let fcmKey = kFCM_Message_token
        let authValue = "key="+fcmKey
        let headers = [
            "Authorization": authValue,
            "Content-Type": "application/json",
            ]
        
        let sendPushForChatUrl  = "https://fcm.googleapis.com/fcm/send"
       
        var param = [String : Any]()
        if isLogin == 2 {
               param = ["to":fcmToken, "priority":"high", "content_available": true, "data": converstaionObject] as [String : Any]
            
        }else{
               param = ["to":fcmToken, "priority":"high", "content_available": true, "notification":params, "data": converstaionObject] as [String : Any]
            
        }
      
        
        
        Alamofire.request(sendPushForChatUrl, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    let jsonDic = response.result.value as! NSDictionary
                    completion(jsonDic, nil)
                    
                } else {
                    let httpError: NSError = response.result.error! as NSError
                    let statusCode = httpError.code
//                    let error:NSDictionary = ["error" : httpError,"statusCode" : statusCode]
                    let error = NSError(domain: "",
                                        code: statusCode,
                                        userInfo: [NSLocalizedDescriptionKey : httpError ?? ""])
                    completion(nil, error)
                }
        }
        
    }

 }
