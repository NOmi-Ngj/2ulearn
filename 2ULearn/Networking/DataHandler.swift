//
//  DataHandler.swift
//  ittendance
//
//  Created by cygnismedia on 05/05/2017.
//  Copyright © 2017 Cygnis. All rights reserved.
//

import Foundation
import ObjectMapper
import AVFoundation
import MediaPlayer

class DataHandler {
    
    // MARK: - Shared Instance
    static let sharedInstance = DataHandler()
    var player: LGAudioPlayer? = nil
    lazy var playlist: [LGPlaybackItem] = { return [] }()
    var accessToken : String = ""
    var image : UIImage?
    var user : User?
    var lessonsPagerItemsController = [Bool]()
    var lessonsPagerItemsId = [String]()
    var lessonsPagerCount = 0
    var lessonsPagerIndex = 0
    var lessonsName = ""
    var lessonsImageUrl = ""
    var lessonsDetails = ""
    var lessonType:[(type: String, value: Int)] = []
    var lessonsSelectionType = 0
    var lessonsSelected = 0
    var lessonsTotal = 0
    var questionsList = [QuestionsList]()
    var cartList = [BooksList]()
    var lessonsList = [LessonsList]()
    
    func getHeader(contentType:Bool)->[String:String]? {
        if DataHandler.sharedInstance.accessToken != "" {
            let headers = contentType == true ? [
                "Authorization": "Bearer  \((DataHandler.sharedInstance.accessToken) as String)",
                "Accept": "application/json"
                ]:["Authorization": "\((DataHandler.sharedInstance.accessToken) as String)"]
            debugPrint("\(headers)")
            return headers
        }
        return nil
    }

    func mapUserResponse(response: AnyObject?) -> User? {
        let user = Mapper<User>().map(JSON: response as! [String : AnyObject])
        DataHandler.sharedInstance.user = user
        DataHandler.sharedInstance.accessToken  = user?.accessToken ?? ""
        return user
    }
    
    
    func removeUser(){
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "vertified")
    }
    func playerDependencies(){
        let audioSession = AVAudioSession.sharedInstance()
        let userDefaults = UserDefaults.standard
        if let isPlayback = userDefaults.value(forKey: "isPlayback") as? Bool {
            
            if isPlayback {
                //duckOthers
                //mixWithOthers
                try! audioSession.setCategory(AVAudioSessionCategoryPlayback, with: .duckOthers)
 
            }else{
                try! audioSession.setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions(rawValue: 0))
            }
        }
        
        let commandCenter = MPRemoteCommandCenter.shared()
        let nowPlayingInfoCenter = MPNowPlayingInfoCenter.default()
        let notificationCenter = NotificationCenter.default
        self.player = LGAudioPlayer(dependencies: (audioSession, commandCenter, nowPlayingInfoCenter, notificationCenter))
    }
    
    func changeAudioSessionForPlayer(){
        let userDefaults = UserDefaults.standard
        if let isPlayback = userDefaults.value(forKey: "isPlayback") as? Bool {
            let audioSession = AVAudioSession.sharedInstance()
            if isPlayback {
                try! audioSession.setCategory(AVAudioSessionCategoryPlayback, with: .duckOthers)
            }else{
                try! audioSession.setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions(rawValue: 0))
            }
            self.player?.audioSession = audioSession
        }
    }
    
    
    func playMusicPlayer(playlist : [LGPlaybackItem]){
        //self.setVolume()
        if playlist.count > 0 {
            DataHandler.sharedInstance.player?.audioPlayer?.isPlaying == true ? DataHandler.sharedInstance.player?.pause():DataHandler.sharedInstance.player?.playItems(playlist, firstItem: playlist[0])
        }
    }
    
    func refreshFcmToken(){
 
    }
    func convertToStringFromDate(date:Date) -> String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "yyyy-MM-dd"
        // again convert your date to string
        return formatter.string(from: yourDate!)
    }
 
    
    func updateUserObject(){
 
    }
    
    func logoutUser(completion: @escaping (_ logout:Bool) -> Void){
        
        let params =   ["device_type":"ios"] as [String : AnyObject]
        
        let _ =   ApiHelper.getRequest(params, kPath: Users.logout.rawValue , completion:{
            response,error in
            if response != nil {
                debugPrint(response!)
                completion(true)
            }else{
                completion(false)
            }
        })
    }
    
    func getPoints(completion: @escaping (_ Points:String) -> Void){
        
        let params = [:] as [String : AnyObject]
        
        let _ =   ApiHelper.getRequest(params, kPath: Users.Points.rawValue , completion:{
            response,error in
            
            if response != nil {
                debugPrint(response!)
                
                let data = (((response as! [String : AnyObject])[ResponseKey.data.rawValue] as? [[String : AnyObject]]))
                if data != nil{
                    let totalScore  =  data?.first?["total_score"] as? String ?? "0"
                    completion(totalScore)
                }else{
                    completion("0")
                }
            }else{
                completion("0")
            }
        })
    }
    //MARK:-  removeUserAndLogout
    func removeUserAndLogout(){
        UIApplication.shared.applicationIconBadgeNumber = 0
        ChatUser.logOutUser { (compeletion) in
            
        }
         player?.audioPlayer?.stop()
         removeUser()
        let vc = UIStoryboard.registrationStoryboard().instantiateViewController(withIdentifier: RegistrationControllers.InitialLoginNavigationController.rawValue) as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
}
