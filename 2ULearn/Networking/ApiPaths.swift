//
//  ApiPaths.swift
//  ittendance
//
//  Created by Cygnis on 5/3/17.
//  Copyright © 2017 Cygnis. All rights reserved.
//

import UIKit

class ApiPaths {
}


//let baseURL = "http://demo-appocta.com/appocta/empowered/public/api/"
let baseURL = "http://2uln.com/api"

enum Users : String{
    case register = "register.json",
    login = "/login",
    socialSignIn = "social.json",
    forgotPassword = "/forgotPassword",
    placeOrder = "/ordercreate",
    logout = "/logout",
    Points = "/indiviualtotalscores",
    aboutUs = "/page?page=about",
    help = "/page?page=help",
    terms = "/page?page=termsandcondition"
    
    
}

enum Media : String {
    case favoriteListing = "/favourite/list",
    audioListing = "/audio/files",
    videoListing = "/video/files",
    eBookListing = "/ebook/files",
    addFav = "/add/favourite",
    removFav = "/remove/favourite"
}

enum Lessons : String {
    case home = "/allcategories?type=home",
    goals = "/allcategories?type=goal",
    allLessons = "/alllessons",
    submitUserAnswers = "/useranswercreate",
    allResults = "/userresult",
    topScorers = "/toptotalscores",
    allBooks = "/bookall",
    allPurchases = "/purchaselist"
}


