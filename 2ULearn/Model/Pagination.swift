//
//  Pagination.swift
//  Konnect
//
//  Created by APP on 12/26/17.
//  Copyright © 2017 APP. All rights reserved.
//

import Foundation
import ObjectMapper

class Pagination : Mappable{
    
    var total : Int = 0
    var current : Int = 1
    var first : Int = 1
    var last : Int = 0
    var previous : Int = 0
    var next : Int = 0
    
    required convenience init?( map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        first <- map["first"]
        total <- map["total"]
        current <- map["current"]
        last <- map["last"]
        previous <- map["previous"]
        next <- map["next"]
    }
}

