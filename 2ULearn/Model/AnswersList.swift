//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class AnswersList:RMMapper, Mappable {
    
    var id : String?
    var question_id : String?
    var answer : String?
    var other_language : String?
    var image_url : ImageUrl?
    var is_correct : Int?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    
    private override init(){}
    
    func mapping(map: Map) {
        
        id <- map["id"]
        question_id <- map["question_id"]
        answer <- map["answer"]
        image_url <- map["image_url"]
        is_correct <- map["is_correct"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
        other_language <- map["other_language"]
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



