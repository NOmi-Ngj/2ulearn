//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class LessonsList:RMMapper, Mappable {
    
    var id : String?
    var image_url : ImageUrl?
    var name : String?
    var created_at : String?
    var descriptions : String?
    var updated_at : String?
    var category_id : String?
    var deleted_at : String?
    var questions : [QuestionsList]?
    var is_attempt : String?
    private override init(){}
    
    func mapping(map: Map) {
        
        created_at <- map["created_at"]
        descriptions <- map["description"]
        id <- map["id"]
        image_url <- map["image_url"]
        name <- map["name"]
        updated_at <- map["updated_at"]
        category_id <- map["category_id"]
        deleted_at <- map["deleted_at"]
        questions <- map["questions"]
        is_attempt <- map["is_attempt"]
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



