//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class Results:RMMapper, Mappable {
 
    var vocabulary : Int?
    var totalvocabulary : Int?
    var grammar : Int?
    var totalgrammar : Int?
    var word : Int?
    var totalword : Int?
    var characters : Int?
    var totalcharacters : Int?
     
    private override init(){}
    
    func mapping(map: Map) {
        
        vocabulary <- map["sentences"]
        totalvocabulary <- map["totalsentences"]
        grammar <- map["grammar"]
        totalgrammar <- map["totalgrammar"]
        word <- map["word"]
        totalword <- map["totalword"]
        characters <- map["characters"]
        totalcharacters <- map["totalcharacters"] 
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



