//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class QuestionsList:RMMapper, Mappable {
    
    
    var id : String?
    var type : String?
    var question : String?
    var audio_file : String?
    var lesson_id : String?
    var nature : String?
    var points = 0
    var language : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    var answers : [AnswersList]?
    
    private override init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        question <- map["question"]
        audio_file <- map["audio_file"]
        lesson_id <- map["lesson_id"]
        nature <- map["nature"]
        language <- map["language"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
        answers <- map["answers"]
        points <- map["point"]
        
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



