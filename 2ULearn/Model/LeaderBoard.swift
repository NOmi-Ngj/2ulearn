//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class LeaderBoard:RMMapper, Mappable {
 
    var id : String?
    var name : String?
    var email : String?
    var remember_token : String?
    var created_at : String?
    var updated_at : String?
    var is_active : String?
    var login_at : String?
    var access_token : String?
    var image_url : ImageUrl?
    var social_id : String?
    var join_date : String?
    var total_score : String?
    var totalscore : String?
    private override init(){}
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        remember_token <- map["remember_token"]
        created_at <- map["created_at"]
        is_active <- map["is_active"]
        login_at <- map["login_at"]
        access_token <- map["access_token"]
        image_url <- map["image_url"]
        social_id <- map["social_id"]
        join_date <- map["join_date"]
        total_score <- map["total_score"]
        
        totalscore <- map["totalscore"]
        
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



