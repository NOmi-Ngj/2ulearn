//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class User:RMMapper, Mappable {
    
    var id : String?
    var name : String?
    var email : String?
    var remember_token : String?
    var is_active : String?
    var login_at : String?
    var accessToken : String?
    var Points : Int?
    var image_url : ImageUrl?
    var social_id : String?
    var join_date : String?
    
    private override init(){}
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        remember_token <- map["remember_token"]
        is_active <- map["is_active"]
        login_at <- map["login_at"]
        accessToken <- map["access_token"]
        image_url <- map["image_url"]
        social_id <- map["social_id"]
        join_date <- map["join_date "]
        Points <- map["total_score "]
      
        
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



