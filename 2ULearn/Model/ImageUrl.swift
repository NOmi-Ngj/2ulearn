//
//  ImageUrl.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//


import UIKit
import ObjectMapper
import RMMapper

class ImageUrl: RMMapper, Mappable {
    
    var oneX: String = ""
    var twoX: String = ""
    var threeX: String = ""
    var thumb : String = ""
    
    private override init(){}
    
    required convenience init?( map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        oneX <- map["1x"]
        twoX <- map["2x"]
        threeX <- map["3x"]
        thumb <- map["thumb"]
    }
    
    func scaledImageURL() -> String {
        let device = Helper.getDeviceType()
//        if (device == .iPhone6Plus || device == .Unknown) {
            return threeX
//        } else {
//            return twoX
//        }
    }
}
