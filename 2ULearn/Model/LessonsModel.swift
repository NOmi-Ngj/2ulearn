//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class LessonsModel:RMMapper, Mappable {
    
    var created_at : String?
    var descriptions : String?
    var id : String?
    var image_url : ImageUrl?
    var name : String?
    var type : String?
    var updated_at : String?
    var lessons :[LessonsList]?
    var books: [BooksList]? 
    
    private override init(){}
    
    func mapping(map: Map) {
        
        created_at <- map["created_at"]
        descriptions <- map["description"]
        id <- map["id"]
        image_url <- map["image_url"]
        name <- map["name"]
        type <- map["type"]
        updated_at <- map["updated_at"]
        lessons <- map["lessons"]
        books <- map["book"] 
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



