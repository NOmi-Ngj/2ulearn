//
//  User.swift
//  NavigationApp
//
//  Created by APP on 12/19/17.
//  Copyright © 2017 APP. All rights reserved.
//

import UIKit
import ObjectMapper
import RMMapper

class PurchaseList:RMMapper, Mappable {
     
    var id : Int?
    var book_id : Int?
    var order_id : Int?
    var price : Int?
    var created_at : String?
    var updated_at : String?
    var category_id : String?
    var deleted_at : String?
    var book_details : BooksList?
    
    private override init(){}
    
    func mapping(map: Map) {
        
        id <- map["id"]
        book_id <- map["book_id"]
        order_id <- map["order_id"]
        price <- map["price"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        category_id <- map["category_id"]
        deleted_at <- map["deleted_at"]
        book_details <- map["book_details"]
       
    }
    
    required convenience init?( map: Map) {
        self.init()
    }
    
}



