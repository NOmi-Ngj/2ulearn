//
//  UIStoryBoard+Extension.swift
//  Free Agent
//
//  Created by APP on 1/22/18.
//  Copyright © 2018 APP. All rights reserved.
//

import UIKit

enum RegistrationControllers : String {
    case InitialLoginNavigationController = "InitialLoginNavigationController",
    SignInVC = "SignInVC",
    ForgotPasswordVC = "ForgotPasswordVC"
    
}

enum HomeControllers : String {
    case HomeVC = "HomeVC",
    NaviHome = "NavigationForHome",
    MainVC  = "MainVC",
    LeftMenuVC = "LeftMenuVC"
}

enum LeftMenuControllers : String {
    case HomeVC = "HomeVC",
    GoalsVC = "GoalsVC",
    ResultsVC = "ResultsVC",
    TextBooksVC = "TextBooksVC",
    TextBookListVC = "TextBookListVC",
    TextBookDetailsVC = "TextBookDetailsVC"
    
}
enum MediaController :String{
    case AudioVC = "AudioVC",
    VideoVC = "VideoVC",
    EbookVC = "EbookVC",
    FavoriteVC = "FavoriteVC",
    AudioPlayerVC = "AudioPlayerVC",
    EbookDetailsVC = "EbookDetailsVC",
    VideoPlayerVC = "VideoPlayerVC"
}


enum LessonsControllers : String {
    case LessonsResultsVC = "LessonsResultsVC",
     LessonVC = "LessonVC",
     LessonWithImagesVC = "LessonWithImagesVC",
     LessonWithWordVC = "LessonWithWordVC",
     LessonWithGrammarVC = "LessonWithGrammarVC",
     LessonWithSentancesVC = "LessonWithSentancesVC"
}

enum PurchasesControllers : String {
    case PurchasedListVC = "PurchasedListVC",
    MyCartsVC = "MyCartsVC",
    CheckOutVC = "CheckOutVC",
    ThankYouVC = "ThankYouVC"
    
}
enum ExtraControllers : String {
    case AboutUsVC = "AboutUsVC",
    HelpVC = "HelpVC"
}

enum ChatControllers : String {
    case TalksListVC = "TalksListVC",
    LeadershipBoardVC = "LeadershipBoardVC",
    ChatConversationVC = "ChatConversationVC",
    UsersListVC = "UsersListVC"
}
 

extension UIStoryboard {
    static func registrationStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Registration", bundle: nil)
    }
    
    static func mediaStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Media", bundle: nil)
    }
    static func chatStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Chat", bundle: nil)
    }
    
    static func homeStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    static func leftMenuItemsStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "LeftMenuItems", bundle: nil)
    }
    static func lessonsStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Lessons", bundle: nil)
    }
    static func purchasesStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Purchases", bundle: nil)
    }
    static func extraStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "ExtraViewControllers", bundle: nil)
    }
    
    
    
}

