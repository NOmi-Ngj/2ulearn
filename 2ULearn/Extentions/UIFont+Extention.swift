//
//  UIFont+Extention.swift
//  Free Agents
//
//  Created by Nouman Gul Junejo on 4/25/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com. All rights reserved.
//

import UIKit

extension UIFont {
    
//    for family: String in UIFont.familyNames
//    {
//    print("\(family)")
//    for names: String in UIFont.fontNames(forFamilyName: family)
//    {
//    print("== \(names)")
//    }
//    }
    
    static func scaledFonts(size:CGFloat) -> CGFloat {
        let device = Helper.getDeviceType()
        if (device == .iPhone4 || device == .iPhone5) {
            return size - 5
        } else if (device == .iPhone6) {
            return size - 2
        }else{
            return size
        }
    }
    
    static func appRegularFonts(size: CGFloat) -> UIFont {
        let fontSize = scaledFonts(size: size)
        return UIFont(name: "TrajanPro-Regular", size: fontSize)!
    }
    static func appBoldFonts(size: CGFloat) -> UIFont {
        let fontSize = scaledFonts(size: size)
        return UIFont(name: "TrajanPro-Bold", size: fontSize)!
    }
    static func appHeadingFonts(size: CGFloat) -> UIFont {
        let fontSize = scaledFonts(size: size)
        return UIFont(name: "Exo-SemiBold", size: fontSize)!
    }
    
    static func appMontFonts(size: CGFloat) -> UIFont {
        let fontSize = scaledFonts(size: size)
        return UIFont(name: "Montserrat-Regular", size: fontSize)!
    }
    static func appSystemFonts(size:CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    static func appSystemRegularFonts(size:CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    static func appSystemBoldFonts(size:CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    static func appSystemHeadingFonts(size:CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
}
