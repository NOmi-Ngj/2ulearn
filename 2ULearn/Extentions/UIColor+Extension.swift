//
//  UIColor+Extension.swift
//  TheMall
//
//  Created by APP on 1/10/18.
//  Copyright © 2018 APP. All rights reserved.
//

import UIKit

extension UIColor {
 
    //return UIColor.init(red: 137.0/255.0, green: 88.0/255.0, blue: 177.0/255.0, alpha: 1.0)
    static func appOrangeColor() -> UIColor {
        // 
        return hexStringToUIColor(hex: "#FA7057")
    }
    static func appYellowBlueColor() -> UIColor {
        //
        return hexStringToUIColor(hex: "#00C3FF")
    }
    
    static func appLightGrayColor() -> UIColor {
        //
        return hexStringToUIColor(hex: "#f2f2f2")
        
    }
    static func appBackgroundColor() -> UIColor {
        //
        return hexStringToUIColor(hex: "#2c2c2c")
    }
    
    static func appWrongBackgroundColor() -> UIColor {
        //
        return hexStringToUIColor(hex: "#FA7057")
    }
    static func appRightBackgroundColor() -> UIColor {
        //
        return hexStringToUIColor(hex: "#00A8CF")
    }
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
